<?php

namespace commonprj\components\catalog\entities\materialCollection;

use commonprj\components\core\entities\element\ElementDBRepository;
use commonprj\components\crm\entities\manufacturer\Manufacturer;
use commonprj\extendedStdComponents\BaseServiceRepository;
use Yii;

/**
 * Class MaterialCollectionServiceRepository
 * @package commonprj\components\catalog\entities\materialCollection
 */
class MaterialCollectionServiceRepository extends BaseServiceRepository
{
    /**
     * @var string
     */
    protected $baseUri = 'material-collection/';

    /**
     * PropertyServiceRepository constructor.
     */
    public function __construct()
    {
        $this->restServer = Yii::$app->catalogManager;
    }

    /**
     * @param MaterialCollection $materialCollection
     * @param int $manufacturerId
     * @return bool
     */
    public function bindManufacturer(MaterialCollection $materialCollection, int $manufacturerId): bool
    {
        $uri = $this->getBaseUri($materialCollection->id . '/manufacturer/' . $manufacturerId);
        $response = $this->restServer->post($uri)->send();
        return $response->getIsOk();
    }

    /**
     * @param MaterialCollection $materialCollection
     * @param int $manufacturerId
     * @return bool
     */
    public function unbindManufacturer(MaterialCollection $materialCollection, int $manufacturerId): bool
    {
        $uri = $this->getBaseUri($materialCollection->id . '/manufacturer/' . $manufacturerId);
        $response = $this->restServer->delete($uri)->send();
        return $response->getIsOk();
    }

    /**
     * @param MaterialCollection $materialCollection
     * @return \commonprj\extendedStdComponents\BaseCrudModel
     * @throws \yii\web\HttpException
     */
    public function getManufacturer(MaterialCollection $materialCollection)
    {
        $this->requestUri = $this->getBaseUri($materialCollection->id . '/manufacturer');
        $arModel = $this->getApiData();
        return $this->getOneModel($arModel, Manufacturer::className());
    }

}