<?php

namespace commonprj\components\catalog\entities\priceCategory;

use commonprj\components\crm\entities\manufacturer\Manufacturer;

/**
 * Interface IPriceCategoryRepository
 * @package commonprj\components\catalog\entities\priceCategory
 */
interface IPriceCategoryRepository
{

    /**
     * @param PriceCategory $priceCategoryId
     * @param int $manufacturerId
     * @return boolean
     */
    public function bindManufacturer(PriceCategory $priceCategoryId, int $manufacturerId): bool;

    /**
     * @param PriceCategory $priceCategoryId
     * @param int $manufacturerId
     * @return boolean
     */
    public function unbindManufacturer(PriceCategory $priceCategoryId, int $manufacturerId): bool;

    /**
     * @param \commonprj\components\catalog\entities\priceCategory\PriceCategory $priceCategoryId
     * @return Manufacturer|null
     */
    public function getManufacturer(PriceCategory $priceCategoryId): ?Manufacturer;
}
