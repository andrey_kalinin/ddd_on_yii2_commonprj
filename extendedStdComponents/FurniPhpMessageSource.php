<?php

namespace commonprj\extendedStdComponents;

use yii\i18n\PhpMessageSource;

class FurniPhpMessageSource extends PhpMessageSource
{

    /**
     * Translates a message to the specified language.
     *
     * Note that unless [[forceTranslation]] is true, if the target language
     * is the same as the [[sourceLanguage|source language]], the message
     * will NOT be translated.
     *
     * If a translation is not found, a [[EVENT_MISSING_TRANSLATION|missingTranslation]] event will be triggered.
     *
     * @param string $category the message category
     * @param string $message the message to be translated
     * @param string $language the target language
     * @return string|bool the translated message or false if translation wasn't found or isn't required
     */
    public function translate($category, $message, $language)
    {
        if ($this->forceTranslation || $language !== $this->sourceLanguage) {
            $translation = $this->translateMessage($category, $message, $language);
            if ($translation === false) {
                $translation = $this->translateMessage($category, $message, '');
            }
            if ($translation === false) {
                $translation = $this->translateMessage($category, $message, 'default');
            }
        } else {
            $translation = $this->translateMessage($category, $message, 'default');
        }
        return $translation;
    }

}