<?php
/**
 * @package         FurniPrice
 * @subpackage      CRM
 * @category        Entity
 * @created         2018.01.22
 * @author          Vasiliy Konakov
 * @updated         2018.01.23
 * @updatedBy       Vasiliy Konakov
 */

namespace commonprj\components\crm\entities\employee;

use commonprj\components\crm\entities\company\Company;
use Yii;
use yii\db\Exception;
use yii\web\HttpException;
use commonprj\components\crm\entities\user\User;
use commonprj\components\crm\entities\ProductOffer\ProductOffer;

class Employee extends User
{

    /**
     * @var EmployeeDBRepository $repository
     */
    public $repository;

    /**
     * @var int $positionId
     */
    public $positionId;

    /**
     * Присвоение свойству доменного слоя $repository - соответствующего компонента
     * @inheritdoc
     */

    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->repository = Yii::$app->employeeRepository;
    }

    /**
     * bindCompany
     * @param int $companyId
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException|Exception
     */
    public function bindCompany(int $companyId): bool
    {
        return $this->repository->bindCompany($this, $companyId);
    }

    /**
     * unbindCompany
     * @param int $companyId
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException|Exception
     */
    public function unbindCompany(int $companyId): bool
    {
        return $this->repository->unbindCompany($this, $companyId);
    }

    /**
     * getCompany
     * @return null|AbstractCompany
     * @throws HttpException|Exception
     */
    public function getCompany(): ?Company
    {
        return $this->repository->getCompany($this);
    }

    /**
     * getProductOffers
     * @return ProductOffer[]
     * @throws HttpException|Exception
     */
    public function getProductOffers(): array
    {
        return $this->repository->getProductOffers($this);
    }

}
