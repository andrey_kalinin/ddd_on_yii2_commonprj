<?php
/**
 * @package         FurniPrice
 * @subpackage      Core
 * @category        DBRepository
 * @created         17.06.2016
 * @author          daSilva.Rodrigues
 * @refactoredBy    Vasiliy Konakov
 * @updated         13.12.2017
 */

namespace commonprj\components\core\entities\propertyValueBigint;

/**
 * Interface IpropertyValueBigintRepository
 * @package commonprj\components\core\entities\propertyValueBigint
 */
interface IPropertyValueBigintRepository
{

}
