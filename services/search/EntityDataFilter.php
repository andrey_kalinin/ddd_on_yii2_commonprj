<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/27/2018
 * Time: 11:07 AM
 */

namespace commonprj\services\search;

use commonprj\components\core\entities\element\Element;
use yii\data\ActiveDataFilter;
use yii\helpers\Inflector;

class EntityDataFilter extends ActiveDataFilter
{

    public $operatorTypes = [
        '<'       => [self::TYPE_INTEGER, self::TYPE_FLOAT],
        '>'       => [self::TYPE_INTEGER, self::TYPE_FLOAT],
        '<='      => [self::TYPE_INTEGER, self::TYPE_FLOAT],
        '>='      => [self::TYPE_INTEGER, self::TYPE_FLOAT],
        '='       => '*',
        '!='      => '*',
        'IN'      => '*',
        'NOT IN'  => '*',
        'LIKE'    => [self::TYPE_STRING],
        'BETWEEN' => [self::TYPE_INTEGER, self::TYPE_FLOAT],
    ];

    public $filterControls = [
        'and'   => 'AND',
        'or'    => 'OR',
        'not'   => 'NOT',
        'lt'    => '<',
        'gt'    => '>',
        'lte'   => '<=',
        'gte'   => '>=',
        'eq'    => '=',
        'neq'   => '!=',
        'in'    => 'IN',
        'nin'   => 'NOT IN',
        'like'  => 'LIKE',
        'range' => 'BETWEEN',
    ];

    /**
     * @var array maps filtering condition keywords to build methods.
     * These methods are used by [[buildCondition()]] to build the actual filtering conditions.
     * Particular condition builder can be specified using a PHP callback. For example:
     *
     * ```php
     * [
     *     'XOR' => function (string $operator, mixed $condition) {
     *         //return array;
     *     },
     *     'LIKE' => function (string $operator, mixed $condition, string $attribute) {
     *         //return array;
     *     },
     * ]
     * ```
     */
    public $conditionBuilders = [
        'AND'     => 'buildConjunctionCondition',
        'OR'      => 'buildConjunctionCondition',
        'NOT'     => 'buildBlockCondition',
        '<'       => 'buildOperatorCondition',
        '>'       => 'buildOperatorCondition',
        '<='      => 'buildOperatorCondition',
        '>='      => 'buildOperatorCondition',
        '='       => 'buildOperatorCondition',
        '!='      => 'buildOperatorCondition',
        'IN'      => 'buildOperatorArrayCondition',
        'NOT IN'  => 'buildOperatorArrayCondition',
        'LIKE'    => 'buildOperatorCondition',
        'BETWEEN' => 'buildOperatorRangeCondition',
    ];

    /**
     * Sets the attribute values in a massive way.
     * @param array $values attribute values (name => value) to be assigned to the model.
     * @param bool $safeOnly whether the assignments should only be done to the safe attributes.
     * A safe attribute is one that is associated with a validation rule in the current [[scenario]].
     * @see safeAttributes()
     * @see attributes()
     */
    public function setAttributes($values, $safeOnly = true)
    {
        $result = [];

        foreach ($values as $key => $value) {
            try {
                list($field, $operator) = explode('__', $key);
                $field = lcfirst(Inflector::camelize($field));

                $result['filter'][$field][$operator] = $value;

            } catch (\Exception $e) {

            }
        }

        parent::setAttributes($result, $safeOnly);
    }

    /**
     * Builds an operator condition.
     * @param string $operator operator keyword.
     * @param mixed $condition attribute condition.
     * @param string $attribute attribute name.
     * @return array actual condition.
     */
    protected function buildOperatorArrayCondition($operator, $condition, $attribute)
    {
        $attribute = $this->transformCamel2id($attribute);
        $condition = explode(',', $condition);

        return parent::buildOperatorCondition($operator, $condition, $attribute);
    }

    /**
     * Builds an operator condition.
     * @param string $operator operator keyword.
     * @param mixed $condition attribute condition.
     * @param string $attribute attribute name.
     * @return array actual condition.
     */
    protected function buildOperatorRangeCondition($operator, $condition, $attribute)
    {
        $attribute = $this->transformCamel2id($attribute);
        $condition = explode('..', $condition);

        if (empty($condition[0])) {
            $operator = '<=';
            $condition = $condition[1];
        } else if (empty($condition[1])) {
            $operator = '>=';
            $condition = $condition[0];
        }

        return parent::buildOperatorCondition($operator, $condition, $attribute);
    }

    /**
     * Builds an operator condition.
     * @param string $operator operator keyword.
     * @param mixed $condition attribute condition.
     * @param string $attribute attribute name.
     * @return array actual condition.
     */
    protected function buildOperatorCondition($operator, $condition, $attribute)
    {
        $attribute = $this->transformCamel2id($attribute);

        if (isset($this->queryOperatorMap[$operator])) {
            $operator = $this->queryOperatorMap[$operator];
        }
        return [$operator, $attribute, $this->filterAttributeValue($attribute, $condition)];
    }

    /**
     * @param string $attribute
     * @return string
     */
    private function transformCamel2id(string $attribute): string
    {
        if (!$this->searchModel instanceof Element) {
            return Inflector::camel2id($attribute, '_');
        }

        return $attribute;
    }
}