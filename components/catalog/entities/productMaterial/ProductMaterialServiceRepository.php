<?php

namespace commonprj\components\catalog\entities\ProductMaterial;

use commonprj\components\catalog\entities\material\Material;
use commonprj\components\catalog\entities\materialCollection\MaterialCollection;
use commonprj\components\catalog\entities\materialGroup\MaterialGroup;
use commonprj\components\catalog\entities\priceCategory\PriceCategory;
use commonprj\components\crm\entities\manufacturer\Manufacturer;
use commonprj\extendedStdComponents\BaseServiceRepository;
use Yii;
use yii\httpclient\Client;

/**
 * Class ProductMaterialServiceRepository
 * @package commonprj\components\catalog\entities\ProductMaterial
 */
class ProductMaterialServiceRepository extends BaseServiceRepository
{
    /**
     * @var Client
     */
    protected $restServer;

    /**
     * @var string
     */
    protected $baseUri = 'product-material/';

    /**
     * ProductMaterialServiceRepository constructor.
     */
    public function __construct()
    {
        $this->restServer = Yii::$app->catalogManager;
    }

    /**
     * @param ProductMaterial $productMaterial
     * @param int $manufacturerId
     * @return bool
     */
    public function bindManufacturer(ProductMaterial $productMaterial, int $manufacturerId)
    {
        $uri = $this->getBaseUri($productMaterial->id . '/manufacturer/' . $manufacturerId);
        $response = $this->restServer->post($uri)->send();
        return $response->getIsOk();
    }

    /**
     * @param ProductMaterial $productMaterial
     * @param int $manufacturerId
     * @return bool
     */
    public function unbindManufacturer(ProductMaterial $productMaterial, int $manufacturerId)
    {
        $uri = $this->getBaseUri($productMaterial->id . '/manufacturer/' . $manufacturerId);
        $response = $this->restServer->delete($uri)->send();
        return $response->getIsOk();
    }

    /**
     * @param ProductMaterial $productMaterial
     * @return \commonprj\extendedStdComponents\BaseCrudModel|null
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\HttpException
     */
    public function getManufacturer(ProductMaterial $productMaterial)
    {
        $this->requestUri = $this->getBaseUri($productMaterial->id . '/manufacturer');
        $this->requestParams = [];
        $content = $this->getApiData();

        if (!empty($content)) {
            $result = $this->getOneModel($content, Manufacturer::className());
        }

        return $result ?? null;
    }

    /**
     * @param ProductMaterial $productMaterial
     * @param int $materialId
     * @return bool
     */
    public function bindMaterial(ProductMaterial $productMaterial, int $materialId)
    {
        $uri = $this->getBaseUri($productMaterial->id . '/material/' . $materialId);
        $response = $this->restServer->post($uri)->send();
        return $response->getIsOk();
    }

    /**
     * @param ProductMaterial $productMaterial
     * @param int $materialId
     * @return bool
     */
    public function unbindMaterial(ProductMaterial $productMaterial, int $materialId)
    {
        $uri = $this->getBaseUri($productMaterial->id . '/material/' . $materialId);
        $response = $this->restServer->delete($uri)->send();
        return $response->getIsOk();
    }

    /**
     * @param ProductMaterial $productMaterial
     * @return array|\commonprj\extendedStdComponents\BaseCrudModel[]|null
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\HttpException
     */
    public function getMaterial(ProductMaterial $productMaterial)
    {
        $this->requestUri = $this->getBaseUri($productMaterial->id . '/material');
        $this->requestParams = [];
        $content = $this->getApiData();

        if (!empty($content)) {
            $result = $this->getOneModel($content, Material::className());
        }

        return $result ?? null;
    }

    /**
     * @param ProductMaterial $productMaterial
     * @param int $materialCollectionId
     * @return bool
     */
    public function bindMaterialCollection(ProductMaterial $productMaterial, int $materialCollectionId)
    {
        $uri = $this->getBaseUri($productMaterial->id . '/material-collection/' . $materialCollectionId);
        $response = $this->restServer->post($uri)->send();
        return $response->getIsOk();
    }

    /**
     * @param ProductMaterial $productMaterial
     * @param int $materialCollectionId
     * @return bool
     */
    public function unbindMaterialCollection(ProductMaterial $productMaterial, int $materialCollectionId)
    {
        $uri = $this->getBaseUri($productMaterial->id . '/material-collection/' . $materialCollectionId);
        $response = $this->restServer->delete($uri)->send();
        return $response->getIsOk();
    }

    /**
     * @param ProductMaterial $productMaterial
     * @return \commonprj\extendedStdComponents\BaseCrudModel|null
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\HttpException
     */
    public function getMaterialCollection(ProductMaterial $productMaterial)
    {
        $this->requestUri = $this->getBaseUri($productMaterial->id . '/material-collection');
        $this->requestParams = [];
        $content = $this->getApiData();

        if (!empty($content)) {
            $result = $this->getOneModel($content, MaterialCollection::className());
        }

        return $result ?? null;
    }

    /**
     * @param ProductMaterial $productMaterial
     * @param int $priceCategoryId
     * @return bool
     */
    public function bindPriceCategory(ProductMaterial $productMaterial, int $priceCategoryId)
    {
        $uri = $this->getBaseUri($productMaterial->id . '/price-category/' . $priceCategoryId);
        $response = $this->restServer->post($uri)->send();
        return $response->getIsOk();
    }

    /**
     * @param ProductMaterial $productMaterial
     * @param int $priceCategoryId
     * @return bool
     */
    public function unbindPriceCategory(ProductMaterial $productMaterial, int $priceCategoryId)
    {
        $uri = $this->getBaseUri($productMaterial->id . '/price-category/' . $priceCategoryId);
        $response = $this->restServer->delete($uri)->send();
        return $response->getIsOk();
    }

    /**
     * @param ProductMaterial $productMaterial
     * @return \commonprj\extendedStdComponents\BaseCrudModel|null
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\HttpException
     */
    public function getPriceCategory(ProductMaterial $productMaterial)
    {
        $this->requestUri = $this->getBaseUri($productMaterial->id . '/price-category');
        $this->requestParams = [];
        $content = $this->getApiData();

        if (!empty($content)) {
            $result = $this->getOneModel($content, PriceCategory::className());
        }

        return $result ?? null;
    }

    /**
     * @param ProductMaterial $productMaterial
     * @param int $materialGroupId
     * @return bool
     */
    public function bindMaterialGroup(ProductMaterial $productMaterial, int $materialGroupId)
    {
        $uri = $this->getBaseUri($productMaterial->id . '/material-group/' . $materialGroupId);
        $response = $this->restServer->post($uri)->send();
        return $response->getIsOk();
    }

    /**
     * @param ProductMaterial $productMaterial
     * @param int $materialGroupId
     * @return bool
     */
    public function unbindMaterialGroup(ProductMaterial $productMaterial, int $materialGroupId)
    {
        $uri = $this->getBaseUri($productMaterial->id . '/material-group/' . $materialGroupId);
        $response = $this->restServer->delete($uri)->send();
        return $response->getIsOk();
    }

    /**
     * @param ProductMaterial $productMaterial
     * @return \commonprj\extendedStdComponents\BaseCrudModel|null
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\HttpException
     */
    public function getMaterialGroup(ProductMaterial $productMaterial)
    {
        $this->requestUri = $this->getBaseUri($productMaterial->id . '/material-group');
        $this->requestParams = [];
        $content = $this->getApiData();

        if (!empty($content)) {
            $result = $this->getOneModel($content, MaterialGroup::className());
        }

        return $result ?? null;
    }

}