<?php

namespace commonprj\components\core\entities\elementCategory;

use commonprj\components\core\entities\elementClass\ElementClass;
use commonprj\components\core\entities\propertyVariant\PropertyVariant;
use Yii;
use yii\web\HttpException;
use commonprj\extendedStdComponents\BaseCrudModel;

/**
 * Class elementCategory
 * @property IElementCategoryRepository $repository
 * @package commonprj\components\core\entities\elementCategory
 */
class ElementCategory extends BaseCrudModel
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $sysname;

    /**
     * @var string
     */
    public $description;

    /**
     * @var int
     */
    public $parentId;

    /**
     * @var int
     */
    public $menuId;

    /**
     * @var bool
     */
    public $isActive = 1;

    /**
     * @var int
     */
    public $propertyVariantId;

    /**
     * @var PropertyVariant
     */
    public $propertyVariant;

    /**
     * @var int
     */
    public $sortOrder = 1000;

    /**
     * @var int
     */
    public $level = 0;

    /**
     * @var int
     */
    public $imageId;

    /**
     * @var int
     */
    public $propertyId;

    /**
     * @var array
     */
    public $propertyValueIds;

    /**
     * @var array
     */
    public $children;

    /**
     * Присвоение свойству доменного слоя $repository - соответствующего компонента
     * @inheritdoc
     */
    public function __construct($config = [])
    {
        parent::__construct($config);
        $this->repository = Yii::$app->elementCategoryRepository;
    }

    /**
     * @return mixed
     */
    public function getChildren()
    {
        return $this->repository->getChildren($this);
    }

    /**
     * @return null|\yii\db\ActiveRecord
     * @throws HttpException
     */
    public function getParent()
    {
        return $this->repository->getParent($this);
    }

    /**
     * @return ElementClass
     */
    public function getElementClass(): ElementClass
    {
        return $this->repository->getElementClass($this);
    }

    /**
     * @return PropertyVariant
     */
    public function getPropertyVariant(): PropertyVariant
    {
        return $this->repository->getPropertyVariant($this);
    }

}
