<?php

namespace commonprj\components\core\entities\elementCategory;

use commonprj\components\core\entities\elementClass\ElementClass;
use commonprj\components\core\entities\propertyVariant\PropertyVariant;
use commonprj\extendedStdComponents\IBaseCrudModel;
use commonprj\extendedStdComponents\IBaseRepository;
use yii\db\ActiveRecord;
use yii\web\HttpException;

/**
 * Class ElementCategoryRepository
 * @package commonprj\components\core\entities\elementCategory
 */
interface IElementCategoryRepository extends IBaseRepository
{
    /**
     * @param ElementCategory $elementCategory
     * @return ElementCategory[]
     */
    public function getChildren(ElementCategory $elementCategory);

    /**
     * @param ElementCategory $elementCategory
     * @return null|ElementCategory
      */
    public function getParent(ElementCategory $elementCategory);

    /**
     * @param ElementCategory $elementCategory
     * @return ElementClass
     */
    public function getElementClass(ElementCategory $elementCategory): IBaseCrudModel;

    /**
     * @param ElementCategory $elementCategory
     * @return PropertyVariant
     */
    public function getPropertyVariant(ElementCategory $elementCategory): IBaseCrudModel;

}