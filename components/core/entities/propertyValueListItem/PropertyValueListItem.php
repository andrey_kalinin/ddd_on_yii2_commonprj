<?php
/**
 * Created by daSilva.Rodrigues
 * Date: 22.08.2016
 */

namespace commonprj\components\core\entities\propertyValueListItem;

use commonprj\components\core\entities\abstractPropertyValue\AbstractPropertyValue;
use commonprj\components\core\models\ElementRecord;
use commonprj\components\core\models\PropertyListItemRecord;
use commonprj\components\core\models\PropertyRecord;
use yii\db\ActiveRecord;

/**
 * Class ListItemProperty
 * @package commonprj\components\core\entities\listItemProperty
 */
class PropertyValueListItem extends AbstractPropertyValue
{
    public $label;
    public $value;

    public function __construct(array $config = [], ActiveRecord $activeRecord = null)
    {
        parent::__construct($config);
        $this->repository = \Yii::$app->propertyValueListRepository;
    }

    /**
     * Active record style validation for not-active record Model
     * @return array
     */
    public function rules(): array
    {
        return [
            [['id', 'element_id', 'property_id', 'value'], 'required'],
            [['id', 'element_id', 'property_id', 'value'], 'integer'],
            [['label'], 'string', 'max' => 255],
            [['element_id'], 'exist', 'skipOnError' => true, 'targetClass' => ElementRecord::className(), 'targetAttribute' => ['element_id' => 'id']],
            [['property_id'], 'exist', 'skipOnError' => true, 'targetClass' => PropertyRecord::className(), 'targetAttribute' => ['property_id' => 'id']],
            [['value'], 'exist', 'skipOnError' => true, 'targetClass' => PropertyListItemRecord::className(), 'targetAttribute' => ['property_list_item_id' => 'id']],
        ];
    }
}