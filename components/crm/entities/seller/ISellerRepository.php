<?php

namespace commonprj\components\crm\entities\seller;

use commonprj\components\crm\entities\manufacturer\Manufacturer;

interface ISellerRepository
{

    /**
     * @param Seller $seller
     * @param int $manufacturerId
     * @return bool
     */
    public function bindManufacturer(Seller $seller, int $manufacturerId): bool;

    /**
     * @param Seller $seller
     * @param int $manufacturerId
     * @return bool
     */
    public function unbindManufacturer(Seller $seller, int $manufacturerId): bool;

    /**
     * @param Seller $seller
     * @return Manufacturer[]
     */
    public function getManufacturers(Seller $seller): array;

    /**
     * @param Seller $seller
     * @return array
     */
    public function findProductRequests(Seller $seller): array;

}
