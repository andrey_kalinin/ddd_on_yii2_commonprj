<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 15.12.2017
 * Time: 12:08
 */

namespace commonprj\components\core\entities\propertyArrayValue;

use commonprj\components\core\entities\abstractPropertyValue\AbstractPropertyValue;
use commonprj\components\core\entities\abstractPropertyValue\AbstractPropertyValueDBRepository;
use commonprj\components\core\entities\propertyValueListItem\PropertyValueListItem;
use commonprj\components\core\entities\propertyValueTreeItem\PropertyValueTreeItem;
use commonprj\components\core\helpers\PostgreSqlArrayHelper;
use commonprj\components\core\models\PropertyArrayRecord;
use commonprj\extendedStdComponents\IBaseCrudModel;
use commonprj\services\SearchDBSynchService;
use Yii;
use yii\db\ActiveRecord;
use yii\web\HttpException;

class PropertyArrayValueDBRepository extends AbstractPropertyValueDBRepository
{
    public $activeRecordClass = PropertyArrayRecord::class;

    /**
     * @param array $valueIds
     * @param $propertyValueClass
     * @return array
     */
    public function getValuesArray(array $valueIds, $propertyValueClass): array
    {
        $result = [];

        foreach ($valueIds as $valueId) {
            $elementPropertyValue = new $propertyValueClass;
            $propertyValue = $elementPropertyValue->findOne([
                'condition' => ['id' => $valueId],
            ]);
            $result[] = $propertyValue['value'];
        }

        return $result;
    }

    /**
     * @param IBaseCrudModel $entity
     * @param array|int|string $condition
     * @return \commonprj\extendedStdComponents\BaseCrudModel|\yii\db\ActiveRecord
     * @throws HttpException
     */
    public function findOne(IBaseCrudModel $entity, $condition)
    {
        $propertyArrayValueRecord = parent::findOne($condition);

        $propertyRecord = $propertyArrayValueRecord->getProperty()->one();

        $propertyValueClass = self::getPropertyValueClassByTypeId($propertyRecord->getAttribute('property_type_id'));
        $propertyArrayValue = parent::instantiateByARAndClassName($propertyArrayValueRecord, PropertyArrayValue::class);
        $valuesIds = PostgreSqlArrayHelper::pg2array($propertyArrayValue->valueIds);
        $propertyArrayValue->values = $this->getValuesArray($valuesIds, $propertyValueClass);

        return $propertyArrayValue;
    }

    /**
     * @param IBaseCrudModel $propertyValue
     * @return bool
     * @throws HttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function delete(IBaseCrudModel $propertyValue): bool
    {
        /**
         * @var ActiveRecord|null $record
         */
        $record = $propertyValue->repository->activeRecordClass::findOne($propertyValue->id);

        if (!$record) {
            throw new HttpException(404, basename(__FILE__, '.php') . __LINE__);
        }

        return $record->delete();
    }

    /**
     * @param AbstractPropertyValue $arrayPropertyValue
     * @return bool|mixed|\yii\db\ActiveRecord
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function save(IBaseCrudModel $arrayPropertyValue, $runValidation = true, $attributes = null): bool
    {

        $transaction = Yii::$app->getDb()->beginTransaction();
        $valueIds = [];

        // save scalar values
        $concretePropertyClass = self::getConcretePropertyValueClassByPropertyId($arrayPropertyValue->propertyId);

        $existingValues = $arrayPropertyValue->getOldAttributes('values') ?? [];
        $requestsValues = $arrayPropertyValue->values;

        $deletedValues = array_diff($existingValues, $requestsValues);

        $attributes = [
            'propertyId'     => $arrayPropertyValue->propertyId,
            'elementId'      => $arrayPropertyValue->elementId,
            'multiplicityId' => AbstractPropertyValue::MULTIPLICITY_ARRAY,
        ];

        foreach ((array)$deletedValues as $deletedValue) {

            if (!$this->isList($concretePropertyClass)) {
                $searchParams = [
                    'property_id' => $arrayPropertyValue->propertyId,
                    'value'       => $deletedValue,
                    'element_id'  => $arrayPropertyValue->elementId,
                ];
                $deletedPropertyValue = $concretePropertyClass::find()->where($searchParams)->one(Yii::$app->getDb());
            }

            $attributes['value'] = $deletedValue;
            $deletedPropertyValue = $deletedPropertyValue ?? new $concretePropertyClass($attributes);

            $deletedPropertyValue->delete();
            $deletedPropertyValue = null;
        }

        foreach ($requestsValues as $value) {

            if (!$this->isList($concretePropertyClass)) {
                $searchParams = [
                    'property_id' => $arrayPropertyValue->propertyId,
                    'value'       => $value,
                    'element_id'  => $arrayPropertyValue->elementId,
                ];
                $propertyValue = $concretePropertyClass::find()->where($searchParams)->one(Yii::$app->getDb());
            }

            $attributes['value'] = $value;
            $propertyValue = $propertyValue ?? new $concretePropertyClass($attributes);

            $result = $propertyValue->save();

            if (!$result) {
                $transaction->rollBack();
                return false;
            }

            if ($propertyValue instanceof PropertyValueListItem) {
                $valueIds[] = $propertyValue->value;
            } else {
                $valueIds[] = $propertyValue->id;
            }

            $propertyValue = null;
        }

        // save array value
        if ($arrayPropertyValue->isNew()) {
            $propertyArrayRecord = new $this->activeRecordClass();
            $propertyArrayRecord->setAttributes(self::arrayKeysCamelCase2Underscore($arrayPropertyValue->getAttributes()));
        } else {
            $propertyArrayRecord = $this->activeRecordClass::find()->where(['id' => $arrayPropertyValue->id])->one();
        }

        $propertyArrayRecord->value_ids = PostgreSqlArrayHelper::array2pg($valueIds);

        $result = $propertyArrayRecord->save();

        if (!$result) {
            $transaction->rollBack();
            return false;
        }

        $searchDbSynch = new SearchDBSynchService($propertyArrayRecord, $arrayPropertyValue->multiplicityId);

        if (!$this->isList($concretePropertyClass)) {
            $result = $searchDbSynch->saveMultiplePropertyValue((new $concretePropertyClass)->repository->activeRecordClass, $arrayPropertyValue->values);
        } else {
            $result = $searchDbSynch->saveMultiplePropertyValue((new $concretePropertyClass)->repository->activeRecordClass, $valueIds);
        }

        $transaction->commit();
        return $result;
    }

    private function isList($propertyClass)
    {
        return in_array($propertyClass, [PropertyValueListItem::class, PropertyValueTreeItem::class]);
    }
}