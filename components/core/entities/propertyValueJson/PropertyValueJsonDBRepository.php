<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 20.03.2018
 * Time: 16:55
 */

namespace commonprj\components\core\entities\propertyValueJson;

use commonprj\components\core\entities\abstractPropertyValue\AbstractPropertyValueDBRepository;
use commonprj\components\core\models\PropertyValueJsonRecord;
use commonprj\extendedStdComponents\IBaseCrudModel;

/**
 * Class PropertyValueJsonDBRepository
 * @package commonprj\components\core\entities\propertyValueJson
 */
class PropertyValueJsonDBRepository extends AbstractPropertyValueDBRepository
{
    /**
     * @var string
     */
    public $activeRecordClass = PropertyValueJsonRecord::class;

    /**
     * @param IBaseCrudModel $entity
     * @param bool $runValidation
     * @param null $attributes
     * @return bool
     */
    public function save(IBaseCrudModel $entity, $runValidation = true, $attributes = null): bool
    {
        $entity->value = json_encode($entity->value);
        return parent::save($entity, $runValidation, $attributes);
    }
}