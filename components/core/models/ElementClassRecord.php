<?php

namespace commonprj\components\core\models;

use Yii;

/**
 * This is the model class for table "element_class".
 *
 * @property integer $id
 * @property integer $context_id
 * @property string $name
 * @property string $description
 * @property integer $parent_id
 *
 * @property Element2elementClassRecord[] $element2elementClasses
 * @property ElementRecord[] $elements
 * @property ElementCategoryRecord[] $elementCategories
 * @property ContextRecord $context
 * @property Property2elementClassRecord[] $property2elementClasses
 * @property PropertyRecord[] $properties
 * @property PropertyTypeRecord[] $propertyTypes
 */
class ElementClassRecord extends AbstractBasicRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'element_class';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['context_id', 'name'], 'required'],
            [['context_id', 'parent_id'], 'integer'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['context_id'], 'exist', 'skipOnError' => true, 'targetClass' => ContextRecord::className(), 'targetAttribute' => ['context_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'context_id' => 'Context ID',
            'name' => 'Name',
            'description' => 'Description',
            'parent_id' => 'Parent ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getElement2elementClasses()
    {
        return $this->hasMany(Element2elementClassRecord::className(), ['element_class_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getElements()
    {
        return $this->hasMany(ElementRecord::className(), ['id' => 'element_id'])->viaTable('element2element_class', ['element_class_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getElementCategories()
    {
        return $this->hasMany(ElementCategoryRecord::className(), ['element_class_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContext()
    {
        return $this->hasOne(ContextRecord::className(), ['id' => 'context_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty2elementClasses()
    {
        return $this->hasMany(Property2elementClassRecord::className(), ['element_class_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperties()
    {
        return $this->hasMany(PropertyRecord::className(), ['id' => 'property_id'])->viaTable('property2element_class', ['element_class_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertyTypes()
    {
        return $this->hasMany(PropertyTypeRecord::className(), ['element_class_id' => 'id']);
    }
}
