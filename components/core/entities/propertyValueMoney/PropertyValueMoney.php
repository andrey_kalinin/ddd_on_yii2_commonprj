<?php

namespace commonprj\components\core\entities\propertyValueMoney;

use commonprj\components\core\entities\abstractPropertyValue\AbstractPropertyValue;
use yii\db\ActiveRecord;

/**
 * Class IntProperty
 * @package commonprj\components\core\entities\propertyValueInt
 */
class PropertyValueMoney extends AbstractPropertyValue
{
    public $currencyListItemId;

    public function __construct(array $config = [], ActiveRecord $activeRecord = null)
    {
        parent::__construct($config);
        $this->repository = \Yii::$app->propertyValueMoneyRepository;
    }
}