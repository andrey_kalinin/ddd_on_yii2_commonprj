<?php
/**
 * Created by daSilva.Rodrigues
 * Date: 17.06.2016
 */

namespace commonprj\components\core\entities\elementClass;

use commonprj\components\core\entities\property\Property;
use commonprj\components\core\entities\relationClass\RelationClass;
use commonprj\components\core\helpers\ClassAndContextHelper;
use commonprj\components\core\models\ElementClassRecord;
use commonprj\components\core\models\Property2elementClassRecord;
use commonprj\components\core\models\RelationClassRecord;
use commonprj\extendedStdComponents\BaseCrudModel;
use commonprj\extendedStdComponents\BaseDBRepository;
use commonprj\extendedStdComponents\IBaseCrudModel;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\web\HttpException;
use yii\web\ServerErrorHttpException;

/**
 * Class ElementClassRepository
 * @package commonprj\components\core\entities\elementClass
 */
class ElementClassDBRepository extends BaseDBRepository implements IElementClassRepository
{
    public $activeRecordClass = ElementClassRecord::class;


    /**
     * Сохранение переданного объекта посредством active record
     * @param ElementClass $elementClass
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function save(IBaseCrudModel $elementClass, $runValidation = true, $attributes = null): bool
    {
        $translations = $this->generateTranslationList($elementClass->description);

        $elementClass->description = $translations['eng'] ?? $translations['transliterate'] ?? $elementClass->label;

        $result = parent::save($elementClass);

        //получаем ID записи соответствующее этому значению в searchDB
        $idInSearchDB = $elementClass->id;

        // Если была запись и у нас есть переводы, то добавляем их в базу переводов
        if (is_array($translations) && $idInSearchDB) {
            $this->addTranslations('search.' . $this->activeRecordClass::tableName() . '.description',
                $translations,
                null,
                $idInSearchDB
            );
        }

        return $result;
    }

    /**
     * Метод возвращает объект контекста к которому принадлежит текущий класс.
     * @param int $elementClassId - Класс чей контекст нужно вернуть.
     * @return ActiveRecord
     */
    public function getContext(int $elementClassId)
    {
        $elementClassRecord = ElementClassRecord::findOne($elementClassId);

        return $elementClassRecord->getContext()->one();
    }

    /**
     * @param $condition
     * @param bool $isRoot
     * @return RelationClass[]
     * @throws HttpException
     */
    public function getRelationClassesById($condition, bool $isRoot)
    {
        if (!$elementClassRecord = ElementClassRecord::findOne($condition)) {
            throw new HttpException(404, basename(__FILE__, '.php') . __LINE__);
        }

        $elementClass2relationClasses = $elementClassRecord->getElementClass2relationClasses()
            ->select(['relation_class_id'])
            ->where(['is_root' => $isRoot])
            ->all();

        $relationClassIds = [];
        foreach ($elementClass2relationClasses as $elementClass2relationClass) {
            $relationClassIds[] = $elementClass2relationClass->getAttribute('relation_class_id');
        }

        if (!$relationClassIds) {
            return [];
        }

        $result = [];
        $relationClassRecords = RelationClassRecord::find()->where(['id' => $relationClassIds])->all();

        foreach ($relationClassRecords as $relationClassRecord) {
            $result[$relationClassRecord['id']] = self::instantiateByARAndClassName($relationClassRecord, 'commonprj\components\core\entities\relationClass\RelationClass');
        }

        return $result;
    }

    /**
     * @param $condition
     * @return array|\yii\db\ActiveRecord[]
     * @throws HttpException
     */
    public function getPropertiesById($condition)
    {
        if (!$elementClassRecord = ElementClassRecord::findOne($condition)) {
            throw new HttpException(404, basename(__FILE__, '.php') . __LINE__);
        }

        $properties = $elementClassRecord->getProperties()->all();
        $result = [];

        foreach ($properties as $property) {
            $result[$property['id']] = $property;
        }

        return $result;
    }

    /**
     * @param $contextNameAndClassName
     * @return BaseCrudModel|ActiveRecord
     * @throws HttpException
     */
    public function getElementClassByName($contextNameAndClassName)
    {
        $className = Yii::$app->route->classNameUrlDecode($contextNameAndClassName);
        $elementClassId = ClassAndContextHelper::getElementClassIdByClassAndContextName($className);
        $elementClassRecord = ElementClassRecord::findOne($elementClassId);

        return self::instantiateByARAndClassName($elementClassRecord, get_called_class());
    }

    /**
     * @param $id
     * @throws HttpException
     * @throws \yii\db\Exception
     */
    public function deleteElementClassById($id)
    {
        $model = ElementClassRecord::findOne($id);

        if (empty($model)) {
            throw new HttpException(404, basename(__FILE__, '.php') . __LINE__);
        }

        Yii::$app->getDb()->beginTransaction();

        try {
            BaseCrudModel::deleteRows($model->getElementTypes()->all());
            BaseCrudModel::deleteRows($model->getProperty2elementClasses()->all());
            BaseCrudModel::deleteRows($model->getElementClass2relationClasses()->all());
            BaseCrudModel::deleteRows($model->getElement2elementClasses()->all());
            BaseCrudModel::deleteRows($model::find()->where(['id' => $id])->all());
        } catch (ServerErrorHttpException $e) {
            Yii::$app->getDb()->getTransaction()->rollBack();
            throw new HttpException(500, 'Failed to delete the object for unknown reason. ' . basename(__FILE__, '.php') . __LINE__);
        }

        Yii::$app->getDb()->getTransaction()->commit();
    }

    /**
     * @return string[]
     */
    public function primaryKey()
    {
        return ElementClassRecord::primaryKey();
    }

    /**
     * @param $elementClassId
     * @param $propertyId
     * @return Property2elementClassRecord
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function createProperty2ElementClass($elementClassId, $propertyId)
    {

        $property2ElementClassRecord = new Property2elementClassRecord();
        $property2ElementClassRecord->setAttributes([
            'element_class_id' => $elementClassId,
            'property_id'      => $propertyId,
        ], false);
        $property2ElementClassRecord->save();

        return $property2ElementClassRecord;
    }

    /**
     * @param ElementClass $elementClass
     * @param bool $propertyTypeRelation
     * @return Property[]
     * @throws HttpException
     */
    public function getProperties(ElementClass $elementClass, $propertyTypeRelation = false): array
    {

        $elementClassesQuery = $this->getElementClassesQuery($elementClass->id);

        $elementClassesQuery = $elementClassesQuery
            ->with('properties')
            ->with('properties.propertyType');
        $elementClassRecords = $elementClassesQuery->all();

        $properties = [];

        $propertyTypeParentId = $propertyTypeRelation ? Property::PROPERTY_TYPE_RELATION_ID : Property::PROPERTY_TYPE_PROPERTY_ID;

        foreach ($elementClassRecords as $elementClassRecord) {
            foreach ($elementClassRecord->properties as $propertyRecord) {
                if ($propertyRecord->propertyType->parent_id == $propertyTypeParentId) {
                    unset($propertyRecord->propertyType);

                    $property = Property::instantiate();
                    Property::populateRecord($property, $propertyRecord);
                    Property::translateRecord($property);

                    $properties[$property->id] = $property;
                }
            }
        }

        return $properties;
    }


    /**
     * @param int $elementClassId
     * @return ActiveQuery
     */
    private function getElementClassesQuery(int $elementClassId)
    {
        // Данный запрос рекурсивно вытягивает все дерево классов текущего элемента
        $query = sprintf("WITH RECURSIVE r AS(
        select ec.* from element_class as ec where ec.id = %s

          UNION

          SELECT element_class.*
          FROM element_class
            JOIN r
              ON element_class.id = r.parent_id
        )

        SELECT * from r;", $elementClassId);

        return ElementClassRecord::findBySql($query);
    }

    /**
     * @param ElementClass $elementClass
     * @param int $propertyId
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function bindProperty(ElementClass $elementClass, int $propertyId): bool
    {
        $existedProperty2elementClassRecord = Property2elementClassRecord::find()->where([
            'element_class_id' => $elementClass->id,
            'property_id'      => $propertyId,
        ])->one();

        if ($existedProperty2elementClassRecord) {
            throw new HttpException(409, 'Это Property уже привязано к этому ElementClass');
        }

        $property2elementClassRecord = new Property2elementClassRecord();
        $property2elementClassRecord->element_class_id = $elementClass->id;
        $property2elementClassRecord->property_id = $propertyId;
        $property2elementClassRecord->is_parent = false;

        return $property2elementClassRecord->save();
    }

    /**
     * @param ElementClass $elementClass
     * @param int $propertyId
     * @return bool
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function unbindProperty(ElementClass $elementClass, int $propertyId): bool
    {
        $property2elementClassRecord = Property2elementClassRecord::find()->where([
            'element_class_id' => $elementClass->id,
            'property_id'      => $propertyId,
        ])->one();

        if ($property2elementClassRecord) {
            return $property2elementClassRecord->delete();
        }

        return true;
    }

}