<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 23.01.2018
 * Time: 11:09
 */

namespace commonprj\services\search;

use commonprj\components\core\entities\property\Property;
use commonprj\components\core\entities\propertyType\PropertyType;
use commonprj\components\core\models\search\PropertyRecord;
use commonprj\services\search\exception\SearchServiceBadRequestException;
use commonprj\services\search\filters\AbstractFilter;
use commonprj\services\search\filters\ClassIdFilter;
use commonprj\services\search\filters\IdFilter;
use commonprj\services\search\filters\PropertyListFilter;
use commonprj\services\search\filters\PropertyPriceListFilter;
use commonprj\services\search\filters\PropertyStringFilter;
use commonprj\services\search\filters\PropertyTextFilter;
use commonprj\services\search\filters\PropertyTreeFilter;
use yii\db\Query;
use yii\helpers\Inflector;

/**
 * Class SearchFilter
 * @package commonprj\services\search
 */
class SearchFilter
{
    const MULTIPLE_OPERATIONS = ['range', 'in'];
    const OPERATIONS_BY_TYPE = [
        'boolean'     => ['eq', 'ne', 'null', 'notnull'],
        'number'      => ['eq', 'ne', 'gt', 'ge', 'lt', 'le', 'null', 'notnull', 'range', 'in'],
        'string'      => ['eq', 'ne', 'null', 'notnull', 'like'],
        'geolocation' => ['eq', 'ne', 'null', 'notnull', 'in'],
    ];
    const PROPERTY_TYPE_TO_OPERATION_TYPE = [
        'boolean'     => ['boolean'],
        'number'      => ['int', 'bigint', 'float', 'date', 'timestamp', 'list'],
        'string'      => ['string', 'text'],
        'geolocation' => ['geolocation'],
    ];

    /**
     * @var string
     */
    private $operation;

    /**
     * @var string
     */
    private $value;

    /**
     * @var string
     */
    private $propertyType;

    /**
     * @var PropertyRecord
     */
    private $property;

    protected $filters = [];
    protected $classId = null;

    /**
     * SearchFilter constructor.
     * @param array $filters
     * @param int $classId
     */
    public function __construct(array $filters = [], int $classId)
    {
        $this->filters = $filters;
        $this->classId = $classId;

    }

    /**
     * Строит запрос с пересечением всех фильтров
     * @return Query
     */
    public function buildFilters()
    {
        $filters = $this->filters;

        $filters = $this->normalizeFilters($filters);

        /** @var Query $resultQuery */
        $resultQuery = null;

        // Собираем все фильтры и пересекаем их
        foreach ($filters as $key => $filter) {
            if (!is_array($filter)) {
                continue;
            }

            $query = $this->renderCondition($filter);

            if ($resultQuery) {
                $prevKey = $key - 1;
                $resultQuery
                    ->innerJoin(["t_$key" => $query], "t_$key.element_id = t_$prevKey.element_id");
            } else {
                $resultQuery = (new Query())
                    ->select("t_$key.element_id")
                    ->from(["t_$key" => $query]);
            }
        }

        return $resultQuery;
    }

    /**
     * Фозвращаем фильтр по названию свойства
     * @param string $orderByField
     * @return AbstractFilter
     */
    public static function getFilterBySysname(string $orderByField)
    {
        @list($orderByField, $sysname) = explode('.', $orderByField);

        if ($sysname) {
            $sysname = lcfirst(Inflector::camelize($sysname));
            $property = Property::findOne(['sysname' => $sysname]);
            $propertyType = PropertyType::findOne($property->propertyTypeId);

            return self::getFilter($propertyType->name, $property->id, $sysname);
        } else {
            return self::getFilter(null, null, $orderByField);
        }
    }


    /**
     * Приводит массив фильтров к нужному виду: массив состоящий из одельного фильтра
     * @param $filters
     * @return array
     */
    protected function normalizeFilters($filters)
    {
        $result = [];

        $simpleFilter = true;
        foreach ($filters as $key => $value) {
            if (is_string($key)) {
                $operation = is_array($value) ? 'IN' : '=';
                $result[] = [$operation, $key, $value];
                $simpleFilter = false;
            } else {
                if (is_array($value) && count($value) == 3) {
                    $result[] = $value;
                    $simpleFilter = false;
                }
            }

        }

        if ($simpleFilter) {
            $result[] = $filters;
        }

        $result[] = [
            '=',
            null,
            $this->classId,
        ];

        return $result;
    }

    /**
     * фабрика по созданию фильтров на основе свойства и его типа
     * @param string|null $type
     * @param int|null $propertyId
     * @param string|null $sysname
     * @return AbstractFilter
     */
    protected static function getFilter(string $type = null, int $propertyId = null, string $sysname = null)
    {
        switch ($sysname) {
            case 'byClassId':
                return new ClassIdFilter($propertyId);
            case 'id':
                return new IdFilter();
            case 'price':
                return new PropertyPriceListFilter($propertyId);
        }

        switch ($type) {
            case 'list':
                return new PropertyListFilter($propertyId);
            case 'tree':
                return new PropertyTreeFilter($propertyId);
            case 'string':
                return new PropertyStringFilter($propertyId);
            case 'text':
                return new PropertyTextFilter($propertyId);
        }

    }


    public static function getFilterData($propertyId, $propertyTypeId, $elementIds)
    {
        $property = Property::findOne($propertyId);
        $propertyType = $property->getType();

        $filter = self::getFilter($propertyType['name'], $propertyId, $property->sysname);

        $result = $filter->getDataForFilter($propertyId, $propertyTypeId, $elementIds);

        return $result;
    }

    /**
     * @param array $filterData
     * @return Query
     */
    protected function renderCondition(array $filterData)
    {
        @list($operation, $sysname, $values, $extendData) = $filterData;

        if ($sysname == null) {
            $filter = self::getFilter(null, null, 'byClassId');
        } else if (strtolower($sysname) == 'id') {
            $filter = self::getFilter(null, null, 'id');
        } else {
            $property = Property::find()
                ->where(['sysname' => $sysname])
                ->one();

            $propertyType = $property->getType();

            $filter = self::getFilter($propertyType['name'], $property->id, $sysname);
        }


        $query = $filter->getQuery($operation, $values, $this->classId, $extendData);

        return $query;
    }

}
