<?php

namespace commonprj\components\crm\entities\productRequest;

use Yii;
use commonprj\components\core\entities\element\Element;

class ProductRequest extends Element
{
    /**
     * Присвоение свойству доменного слоя $repository - соответствующего компонента
     * @inheritdoc
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->repository = Yii::$app->productRequestRepository;
    }
}
