<?php

namespace commonprj\components\crm\entities\permission;

use commonprj\components\crm\entities\permissionGroup\PermissionGroup;

interface IPermissionRepository
{
    /**
     * @param Permission $permission
     * @param int $permissionGroupId
     * @return bool
     */
    public function bindPermissionGroup(Permission $permission, int $permissionGroupId): bool;

    /**
     * @param Permission $permission
     * @param int $permissionGroupId
     * @return bool
     */
    public function unbindPermissionGroup(Permission $permission, int $permissionGroupId): bool;

    /**
     * @param Permission $permission
     * @return PermissionGroup
     */
    public function getPermissionGroup(Permission $permission): PermissionGroup;

}