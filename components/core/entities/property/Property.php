<?php
/**
 * Created by daSilva.Rodrigues
 * Date: 17.06.2016
 */

namespace commonprj\components\core\entities\property;

use commonprj\components\core\entities\propertyVariant\PropertyVariant;
use commonprj\components\core\models\PropertyRecord;
use commonprj\extendedStdComponents\BaseCrudModel;
use commonprj\extendedStdComponents\IBaseCrudModel;
use Yii;
use yii\db\ActiveRecord;
use yii\web\HttpException;

/**
 * Class Property
 * @param PropertyDBRepository $repository
 * @property IPropertyRepository $repository
 * @package commonprj\components\core\entities\property
 */
class Property extends BaseCrudModel implements IBaseCrudModel
{
    const PROPERTY_TYPE_PROPERTY_ID = 1;    //Значение ID обозначающего обычное свойство
    const PROPERTY_TYPE_RELATION_ID = 2;    //Значение ID обозначающего отношение
    const PROPERTY_TYPE_RELATION_ASSOCIATION_ID = 3;       //Значение ID обозначающего отношение
    const PROPERTY_TYPE_RELATION_AGGREGATION_ID = 4;       //Значение ID обозначающего отношение
    const PROPERTY_TYPE_RELATION_HIERARCHY_ID = 5;       //Значение ID обозначающего отношение
    const PROPERTY_TYPE_LIST = 15;       //Значение ID обозначающего список
    const PROPERTY_TYPE_TREE = 20;       //Значение ID обозначающего список-дерево
    const PROPERTY_TYPE_STRING = 10;       //Значение ID обозначающего строку
    const PROPERTY_TYPE_TEXT = 11;       //Значение ID обозначающего строку

    const PROPERTY_SYSNAME__PRODUCT_REQUEST2CUSTOMER = 'ProductRequest2Customer';
    const PROPERTY_SYSNAME__EMPLOYEE2COMPANY = 'Employee2Company';
    const PROPERTY_SYSNAME__PRODUCT_MATERIAL2MATERIAL_COLLECTION = 'ProductMaterial2MaterialCollection';
    const PROPERTY_SYSNAME__PRODUCT_OFFER2EMPLOYEE = 'ProductOffer2Employee';
    const PROPERTY_SYSNAME__MATERIAL_COLLECTION2MANUFACTURER = 'MaterialCollection2Manufacturer';
    const PROPERTY_SYSNAME__PRODUCT_MATERIAL2MANUFACTURER = 'ProductMaterial2Manufacturer';
    const PROPERTY_SYSNAME__PRODUCT_MATERIAL2MATERIAL = 'ProductMaterial2Material';
    const PROPERTY_SYSNAME__PRODUCT_MATERIAL2PRICE_CATEGORY = 'ProductMaterial2PriceCategory';
    const PROPERTY_SYSNAME__PRODUCT_MATERIAL2MATERIAL_GROUP = 'ProductMaterial2MaterialGroup';

    const PROPERTY_SYSNAME__PRICE_CATEGORY2MANUFACTURER = 'PriceCategory2Manufacturer';
    const PROPERTY_SYSNAME__SELLER2MANUFACTURER = 'Seller2Manufacturer';
    const PROPERTY_SYSNAME__COMPANY2COMPANY_STATE = 'Company2CompanyState';
    const PROPERTY_SYSNAME__COMPANY2COMPANY_ROLE = 'Company2CompanyRole';
    const PROPERTY_SYSNAME__COMPANY_STATE2PERMISSION_GROUP = 'CompanyState2PermissionGroup';
    const PROPERTY_SYSNAME__COMPANY_ROLE2PERMISSION_GROUP = 'CompanyRole2PermissionGroup';
    const PROPERTY_SYSNAME__COMPANY2ADDRESS = 'Company2Address';

    const PROPERTY_SYSNAME__USER_ROLE2COMPANY_ROLE = 'UserRole2CompanyRole';
    const PROPERTY_SYSNAME__PERMISSION2USER_ROLE = 'Permission2UserRole';
    const PROPERTY_SYSNAME__PERMISSION2PERMISSION_GROUP = 'Permission2PermissionGroup';
    const PROPERTY_SYSNAME__PERMISSION_GROUP2ELEMENT_CLASS = 'PermissionGroup2ElementClass';

    const PROPERTY_SYSNAME__PRODUCT_MODEL2MANUFACTURER = 'ProductModel2Manufacturer';
    const PROPERTY_SYSNAME__MATERIAL2MATERIAL_TEMPLATE = 'Material2MaterialTemplate';

    const PROPERTY_SYSNAME__MATERIAL2MATERIAL_GROUP = 'Material2MaterialGroup';
    const PROPERTY_SYSNAME__MATERIAL2COMPONENT = 'Material2Component';
    const PROPERTY_SYSNAME__MATERIAL2PRODUCT_MODEL = 'Material2ProductModel';
    const PROPERTY_SYSNAME__MATERIAL2COMPOSITION = 'Material_composition';

    const PROPERTY_SYSNAME__PRODUCT2MANUFACTURER = 'Product2Manufacturer';
    const PROPERTY_SYSNAME__PRODUCT2MATERIAL = 'Product2Material';
    const PROPERTY_SYSNAME__PRODUCT2PRODUCT_MATERIAL = 'Product2ProductMaterial';
    const PROPERTY_SYSNAME__PRODUCT2PRODUCT_MODEL = 'Product2ProductModel';
    const PROPERTY_SYSNAME__PRODUCT2CONSTRUCTION_ELEMENT = 'Product2ConstructionElement';
    const PROPERTY_SYSNAME__VARIANT2PRODUCT = 'Variant2Product';
    const PROPERTY_SYSNAME__OPTION2PROPERTY = 'Option2Property';
    const PROPERTY_SYSNAME__VARIANT2OPTION = 'Variant2Option';

    const PROPERTY_SYSNAME__PRODUCT_MATERIAL2COMPONENT = 'ProductMaterial2Component';

    const PROPERTY_SYSNAME__PRODUCT2MATERIAL_COLLECTION_PRICE_CATEGORY = 'Product2PriceCategory';
    const PROPERTY_SYSNAME__PRODUCT2MODULAR_COMPONENT = 'Product2ModularComponent';
    const PROPERTY_SYSNAME__COMPONENT2MATERIAL = 'Component2Material';
    const PROPERTY_SYSNAME__PRODUCT_MODEL2PRODUCT_MATERIAL = 'ProductModel2ProductMaterial';
    const PROPERTY_SYSNAME__COMPONENT2PRODUCT_MODEL_CONSTRUCTION_ELEMENT = 'Component2ProductModel_constructionElement';

    const PROPERTY_SYSNAME__COMPONENT2PRODUCT_MODEL_MODULAR_COMPONENT = 'Component2ProductModel_modularComponent';
    const PROPERTY_SYSNAME__ADDRESS_TYPE = 'addressType';

    const PROPERTY_SYSNAME__VARIANT2PRODUCT_MODEL = 'Variant2ProductModel';
    const PROPERTY_SYSNAME__COMPONENT2MANUFACTURER = 'Component2Manufacturer';

    const PROPERTY_SYSNAME__MATERIAL_HIERARCHY = 'Material_hierarchy';
    const PROPERTY_SYSNAME__MATERIAL_COMPOSITION = 'Material_composition';

    const PROPERTY_SYSNAME__USER2ADDRESS = 'User2Address';
    const PROPERTY_SYSNAME__USER2USER_ROLE = 'User2UserRole';
    const PROPERTY_SYSNAME__USER2ACCESS_SPECIFICATION = 'User2AccessSpecification';

    const PROPERTY_SYSNAME__PROPERTIES = 'properties';

    const ALLOWED_WHERE_PARAMS = [
        'id',
        'name',
        'description',
        'property_unit_id',
        'is_specific',
        'property_type_id',
        'sysname',
    ];

    /**
     * @var int
     */
    public $id;

    /**
     * @var int
     */
    public $propertyTypeId;

    /**
     * @var string
     */
    public $name;
    /**
     * @var string
     */
    public $sysname;

    /**
     * @var string
     */
    public $description;

    /**
     * @var int
     */
    public $propertyUnitId;

    /**
     * @var bool
     */
    public $isSpecific = false;

    /**
     * @var int
     */
    public $propertyGroupId;

    /**
     * @var int
     */
    public $sortOrder = 1000;

    public $value;

    public $propertyUnit;

    public $propertyGroup;

    protected $searchdbPropertyValueId;

    /**
     * Присвоение свойству доменного слоя $repository - соответствующего компонента
     * @inheritdoc
     */
    public function __construct($config = [])
    {
        parent::__construct($config);
        $this->repository = Yii::$app->propertyRepository;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['id', 'integer', 'skipOnEmpty' => true],
            [['description'], 'string'],
            [['propertyTypeId', 'propertyUnitId'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['sysname'], 'string', 'max' => 50],
        ];
    }

    /**
     * Метод возвращает классы к которым принадлежит текущее свойство.
     * @return \commonprj\components\core\models\ElementClassRecord[]
     * @throws HttpException
     */
    public function getPropertyClasses()
    {
        return $this->repository->getPropertyClassesById($this->id);
    }

    /**
     * @return array
     */
    public function getTypeById()
    {
        return Property::getPropertyTypeNameByPropertyId($this->id);
    }

    /**
     * Метод возвращает массив значений с групировкой по multiplicityId
     * @param null $multiplicityId
     * @return array
     */
    public function getValues($multiplicityId = null)
    {
        return $this->repository->getValues($this->id, $multiplicityId);
    }

    /**
     * @param int $elementId
     * @return mixed
     */
    public function getValueByElementId(int $elementId)
    {
        return $this->repository->getValueByElementId($this->id, $elementId);
    }

    /**
     * @param $elementClassId
     * @throws HttpException
     */
    public function deletePropertyClass($elementClassId)
    {
        $this->repository->deletePropertyClassByClassId($elementClassId, $this->id);
    }

    /**
     * Устанавливает значение свойства для сущности
     * @param $attributes
     * @return bool
     */
    public function saveElementProperty($attributes): bool
    {
        return $this->repository->saveElementProperty($this, $attributes);
    }

    /**
     * @return array|\yii\db\ActiveRecord
     */
    public function getPropertyUnit()
    {
        return $this->repository->getPropertyUnitById($this->id);
    }


    /**
     * @param $propertyValueId
     * @return bool
     * @throws HttpException
     * @throws \yii\db\Exception
     */
    public function deletePropertyValue($propertyValueId)
    {
        return $this->repository->deletePropertyValueByValueId($propertyValueId, $this->id);
    }

    /**
     * @return array
     */
    public function getListItems()
    {
        return $this->repository->getListItems($this);
    }

    /**
     * @return \commonprj\components\core\entities\elementClass\ElementClass[]
     */
    public function getElementClasses()
    {
        return $this->repository->getElementClasses($this);
    }

    /**
     * Sets the attribute values in a massive way.
     * @param array $values attribute values (name => value) to be assigned to the model.
     * @param bool $safeOnly whether the assignments should only be done to the safe attributes.
     * A safe attribute is one that is associated with a validation rule in the current [[scenario]].
     * @see safeAttributes()
     * @see attributes()
     */
    public function setListItems(array $listItems)
    {
        return $this->repository->setListItems($this, $listItems);
    }

    /**
     * Возвращает PropertyTypeRecord приведенный к массиву
     * @return array
     */
    public function getType()
    {
        return $this->repository->getType($this);
    }

    /**
     * @return array|PropertyVariant[]
     */
    public function getPropertyVariants()
    {
        return $this->repository->getPropertyVariants($this);
    }

    /**
     * Инстанцирует Property с PropertyUnit и PropertyGroup
     * @param PropertyRecord $propertyRecord
     * @return mixed
     */
    public static function populate(PropertyRecord $propertyRecord)
    {
        $property = new Property();
        return $property->repository->populate($propertyRecord);
    }

    public function saveWithListItems(array $listItems)
    {
        return $this->repository->saveWithListItems($this, $listItems);
    }

    public function extraFields()
    {
        return [
            'elementClasses' => [$this, 'getElementClasses'],
            'propertyType'   => [$this, 'getType'],
        ];
    }

    /**
     * @return mixed
     */
    public function getSearchdbPropertyValueId()
    {
        return $this->searchdbPropertyValueId;
    }

    /**
     * @param int $searchdbPropertyValueId
     * @return Property
     */
    public function setSearchdbPropertyValueId($searchdbPropertyValueId)
    {
        $this->searchdbPropertyValueId = $searchdbPropertyValueId;
        return $this;
    }

    /**
     * @param $propertyValueId
     * @param $multiplicityId
     * @return PropertyVariant|null
     */
    public function getPropertyVariant($propertyValueId, $multiplicityId)
    {
        return $this->repository->getPropertyVariant($this, $propertyValueId, $multiplicityId);
    }

    /**
     * @return mixed
     */
    public function isPropertyTypeList()
    {
        return $this->repository->isPropertyTypeList($this->id);
    }

    /**
     * @param int $elementClassId
     * @param int $propertyValueId
     * @param int|null $multiplicityId
     * @param bool $isInHierarchy
     * @return \commonprj\components\core\entities\template\Template|null
     */
    public function getTemplate( int $elementClassId, int $propertyValueId, int $multiplicityId = null, bool $isInHierarchy = false)
    {
        return $this->repository->getTemplate($this, $elementClassId, $propertyValueId, $multiplicityId, $isInHierarchy);
    }
}
