<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/26/2018
 * Time: 6:26 PM
 */

namespace commonprj\extendedStdComponents;


use commonprj\services\search\ReturnFilter;
use yii\data\DataProviderInterface;
use yii\data\Pagination;

class Serializer extends \yii\rest\Serializer
{

    /**
     * @var ReturnFilter
     */
    public $returnFilters = null;

    /**
     * Serializes a data provider.
     * @param DataProviderInterface $dataProvider
     * @return array the array representation of the data provider.
     */
    protected function serializeDataProvider($dataProvider)
    {
        $result = parent::serializeDataProvider($dataProvider);

        if ($this->returnFilters) {
            $filters = $dataProvider->getFilters($this->returnFilters);
            $result = array_merge($result, ['filters' => $filters]);
        }

        return $result;
    }


    /**
     * Serializes a pagination into an array.
     * @param Pagination $pagination
     * @return array the array representation of the pagination
     * @see addPaginationHeaders()
     */
    protected function serializePagination($pagination)
    {
        return [
            $this->metaEnvelope => [
                'totalCount'  => $pagination->totalCount,
                'pageCount'   => $pagination->getPageCount(),
                'currentPage' => $pagination->getPage() + 1,
                'perPage'     => $pagination->getPageSize(),
            ],
        ];
    }


    /**
     * Adds HTTP headers about the pagination to the response.
     * @param Pagination $pagination
     */
    protected function addPaginationHeaders($pagination)
    {

    }


}