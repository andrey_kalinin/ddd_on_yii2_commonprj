<?php

namespace commonprj\components\core\models\i18n;

use Yii;

/**
 * This is the model class for table "text_rus".
 *
 * @property integer $id
 * @property integer $source_id
 * @property integer $source_recordset_id
 * @property string $value
 * @property integer $property_id
 *
 * @property SourceRecord $source
 */
class TextRecord extends \yii\db\ActiveRecord
{
    public static $tableName = null;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return self::$tableName ?? 'text_eng';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbI18n');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['source_id', 'source_recordset_id'], 'required'],
            [['source_id', 'source_recordset_id', 'property_id'], 'integer'],
            [['value'], 'string'],
            [['source_id'], 'exist', 'skipOnError' => true, 'targetClass' => SourceRecord::className(), 'targetAttribute' => ['source_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'source_id' => 'Source ID',
            'source_recordset_id' => 'Source Recordset ID',
            'value' => 'Value',
            'property_id' => 'Property ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSource()
    {
        return $this->hasOne(SourceRecord::className(), ['id' => 'source_id']);
    }
}
