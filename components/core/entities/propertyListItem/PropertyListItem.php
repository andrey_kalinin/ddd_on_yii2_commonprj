<?php
/**
 * Created by daSilva.Rodrigues
 * Date: 17.06.2016
 */

namespace commonprj\components\core\entities\propertyListItem;

use commonprj\extendedStdComponents\BaseCrudModel;
use Yii;
use yii\web\HttpException;

/**
 * Class PropertyListItem
 * @package commonprj\components\core\entities\propertyListItem
 *
 * @property integer $context_id
 * @property string $name
 */
class PropertyListItem extends BaseCrudModel
{
    public $id;
    public $propertyId;
    public $item;
    public $label;
    public $searchdbPropertyValueId;
    public $sortOrder;

    /**
     * Присвоение свойству доменного слоя $repository - соответствующего компонента
     * @inheritdoc
     */
    public function __construct($config = [])
    {
        parent::__construct($config);
        $this->repository = Yii::$app->propertyListItemRepository;
    }

    public function getTree($propertyId)
    {
        return $this->repository->getTree($this, $propertyId);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['propertyId'], 'integer'],
        ];
    }

}
