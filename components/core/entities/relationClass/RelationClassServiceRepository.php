<?php

namespace commonprj\components\core\entities\relationClass;


use commonprj\extendedStdComponents\BaseCrudModel;
use commonprj\extendedStdComponents\BaseServiceRepository;

/**
 * Class RelationClassServiceRepository
 * @package commonprj\components\core\entities\relationClass
 */
class RelationClassServiceRepository extends BaseServiceRepository implements IRelationClassRepository
{
    /**
     * @param mixed $condition
     * @return array
     */
    public function find(array $condition = null)
    {
        $this->requestUri = 'common/relation-class';
        $arModel = $this->getApiData();

        return $this->getArrayOfModels($arModel);
    }

    /**
     * @param $condition
     * @param bool $byClass
     * @return BaseCrudModel
     * @throws \yii\web\HttpException
     */
    public function findOne($condition, $byClass = false)
    {
        $this->requestUri = 'common/relation-class/' . $condition;
        $arModel = $this->getApiData();

        return $this->getOneModel($arModel);
    }

    /**
     * @param $relationClassId
     * @param $isRoot
     * @return BaseCrudModel
     * @internal param $condition
     */
    public function getElementClassesById($relationClassId, $isRoot)
    {
        $this->requestUri = 'common/relation-class/' . $relationClassId . '/element-classes';
        $this->requestParams = ['isRoot' => $isRoot];
        $arModel = $this->getApiData();

        return $this->getOneModel($arModel);
    }

}