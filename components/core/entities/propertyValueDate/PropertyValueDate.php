<?php
/**
 * Created by daSilva.Rodrigues
 * Date: 22.08.2016
 */

namespace commonprj\components\core\entities\propertyValueDate;

use commonprj\components\core\entities\abstractPropertyValue\AbstractPropertyValue;
use yii\db\ActiveRecord;

/**
 * Class DateProperty
 * @package commonprj\components\core\entities\propertyValueDate
 */
class PropertyValueDate extends AbstractPropertyValue
{
    public function __construct(array $config = [], ActiveRecord $activeRecord = null)
    {
        parent::__construct($config);
        $this->repository = \Yii::$app->propertyValueDateRepository;
    }
}