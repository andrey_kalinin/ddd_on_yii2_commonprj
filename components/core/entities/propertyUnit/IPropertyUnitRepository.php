<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 12.02.2018
 * Time: 11:02
 */

namespace commonprj\components\core\entities\propertyUnit;
use commonprj\components\core\entities\property\Property;
use commonprj\extendedStdComponents\IBaseRepository;

/**
 * Interface IPropertyUnitDBRepository
 * @package commonprj\components\core\entities\propertyUnit
 */
interface IPropertyUnitRepository extends IBaseRepository
{
    /**
     * @param PropertyUnit $propertyUnit
     * @return Property[]
     */
    public function getProperties(PropertyUnit $propertyUnit): array;
}