<?php

namespace commonprj\components\catalog\entities\ProductModel;

use commonprj\components\catalog\entities\material\Material;
use commonprj\components\catalog\entities\product\Product;
use commonprj\components\catalog\entities\productMaterial\ProductMaterial;
use commonprj\components\crm\entities\manufacturer\Manufacturer;
use commonprj\components\crm\entities\seller\Seller;
use commonprj\extendedStdComponents\BaseServiceRepository;
use Yii;
use yii\httpclient\Client;

/**
 * Class ProductModelServiceRepository
 * @package commonprj\components\catalog\entities\ProductModel
 */
class ProductModelServiceRepository extends BaseServiceRepository
{
    /**
     * @var Client
     */
    protected $restServer;

    /**
     * @var string
     */
    protected $baseUri = 'product-model/';

    /**
     * ProductModelServiceRepository constructor.
     */
    public function __construct()
    {
        $this->restServer = Yii::$app->catalogManager;
    }

    /**
     * @param array|null $condition
     * @return array
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\HttpException
     */
    public function find(array $condition = null)
    {
        $this->requestUri = $this->getBaseUri();
        $content = $this->getApiData();

        foreach ($content['items'] as &$productModel) {
            $productModel['properties'] = ArrayHelper::index($productModel['properties'], function ($item) {
                return $item['sysname'];
            });
        }

        return $content;
    }

    /**
     * @param array $condition
     * @return array
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\HttpException
     */
    public function getFilters(array $condition)
    {
        $filters = [
            'priceVolume'  => '',
            'country'  => '',
            'productStyle' => '',
        ];


        $this->requestUri = 'catalog/product-model/filters';
        $arModel = $this->getApiData();
        return array_values($arModel);
    }

    /**
     * @param ProductModel $productModel
     * @param int $manufacturerId
     * @return bool
     */
    public function bindManufacturer(ProductModel $productModel, int $manufacturerId)
    {
        $uri = $this->getBaseUri($productModel->id . '/manufacturer/' . $manufacturerId);
        $response = $this->restServer->post($uri)->send();
        return $response->getIsOk();
    }

    /**
     * @param ProductModel $productModel
     * @param int $manufacturerId
     * @return bool
     */
    public function unbindManufacturer(ProductModel $productModel, int $manufacturerId)
    {
        $uri = $this->getBaseUri($productModel->id . '/manufacturer/' . $manufacturerId);
        $response = $this->restServer->delete($uri)->send();
        return $response->getIsOk();
    }

    /**
     * @param ProductModel $productModel
     * @return \commonprj\extendedStdComponents\BaseCrudModel|null
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\HttpException
     */
    public function getManufacturer(ProductModel $productModel)
    {
        $this->requestUri = $this->getBaseUri($productModel->id . '/manufacturer');
        $this->requestParams = [];
        $content = $this->getApiData();

        if (!empty($content)) {
            $result = $this->getOneModel($content, Manufacturer::className());
        }

        return $result ?? null;
    }

    /**
     * @param ProductModel $productModel
     * @return array|\commonprj\extendedStdComponents\BaseCrudModel[]|null
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\HttpException
     */
    public function getSellers(ProductModel $productModel)
    {
        $this->requestUri = $this->getBaseUri($productModel->id . '/sellers');
        $this->requestParams = [];
        $content = $this->getApiData();

        if (!empty($content)) {
            $result = $this->getArrayOfModels($content, Seller::className());
        }

        return $result ?? null;
    }

    /**
     * @param ProductModel $productModel
     * @param int $productMaterialId
     * @return bool
     */
    public function bindProductMaterial(ProductModel $productModel, int $productMaterialId)
    {
        $uri = $this->getBaseUri($productModel->id . '/product-material/' . $productMaterialId);
        $response = $this->restServer->post($uri)->send();
        return $response->getIsOk();
    }

    /**
     * @param ProductModel $productModel
     * @param int $productMaterialId
     * @return bool
     */
    public function unbindProductMaterial(ProductModel $productModel, int $productMaterialId)
    {
        $uri = $this->getBaseUri($productModel->id . '/product-material/' . $productMaterialId);
        $response = $this->restServer->delete($uri)->send();
        return $response->getIsOk();
    }

    /**
     * @param ProductModel $productModel
     * @return array|\commonprj\extendedStdComponents\BaseCrudModel[]|null
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\HttpException
     */
    public function getProductMaterials(ProductModel $productModel)
    {
        $this->requestUri = $this->getBaseUri($productModel->id . '/product-materials');
        $this->requestParams = [];
        $content = $this->getApiData();

        if (!empty($content)) {
            $result = $this->getArrayOfModels($content, ProductMaterial::className());
        }

        return $result ?? null;
    }

    /**
     * @param ProductModel $productModel
     * @param int $materialId
     * @return bool
     */
    public function bindMaterial(ProductModel $productModel, int $materialId)
    {
        $uri = $this->getBaseUri($productModel->id . '/material/' . $materialId);
        $response = $this->restServer->post($uri)->send();
        return $response->getIsOk();
    }

    /**
     * @param ProductModel $productModel
     * @param int $materialId
     * @return bool
     */
    public function unbindMaterial(ProductModel $productModel, int $materialId)
    {
        $uri = $this->getBaseUri($productModel->id . '/material/' . $materialId);
        $response = $this->restServer->delete($uri)->send();
        return $response->getIsOk();
    }

    /**
     * @param ProductModel $productModel
     * @return array|\commonprj\extendedStdComponents\BaseCrudModel[]|null
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\HttpException
     */
    public function getMaterials(ProductModel $productModel)
    {
        $this->requestUri = $this->getBaseUri($productModel->id . '/materials');
        $this->requestParams = [];
        $content = $this->getApiData();

        if (!empty($content)) {
            $result = $this->getArrayOfModels($content, Material::className());
        }

        return $result ?? null;
    }

    /**
     * @param ProductModel $productModel
     * @param int $constructionElementId
     * @return bool
     */
    public function bindConstructionElement(ProductModel $productModel, int $constructionElementId)
    {
        $uri = $this->getBaseUri($productModel->id . '/construction-element/' . $constructionElementId);
        $response = $this->restServer->post($uri)->send();
        return $response->getIsOk();
    }

    /**
     * @param ProductModel $productModel
     * @param int $constructionElementId
     * @return bool
     */
    public function unbindConstructionElement(ProductModel $productModel, int $constructionElementId)
    {
        $uri = $this->getBaseUri($productModel->id . '/construction-element/' . $constructionElementId);
        $response = $this->restServer->delete($uri)->send();
        return $response->getIsOk();
    }

    /**
     * @param ProductModel $productModel
     * @return array|\commonprj\extendedStdComponents\BaseCrudModel[]|null
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\HttpException
     */
    public function getConstructionElements(ProductModel $productModel)
    {
        $this->requestUri = $this->getBaseUri($productModel->id . '/construction-elements');
        $this->requestParams = [];
        $content = $this->getApiData();

        if (!empty($content)) {
            $result = $this->getArrayOfModels($content, ConstructionElement::className());
        }

        return $result ?? null;
    }

    /**
     * @param ProductModel $productModel
     * @param int $modularComponentId
     * @return bool
     */
    public function bindModularComponent(ProductModel $productModel, int $modularComponentId)
    {
        $uri = $this->getBaseUri($productModel->id . '/modular-component/' . $modularComponentId);
        $response = $this->restServer->post($uri)->send();
        return $response->getIsOk();
    }

    /**
     * @param ProductModel $productModel
     * @param int $modularComponentId
     * @return bool
     */
    public function unbindModularComponent(ProductModel $productModel, int $modularComponentId)
    {
        $uri = $this->getBaseUri($productModel->id . '/modular-component/' . $modularComponentId);
        $response = $this->restServer->delete($uri)->send();
        return $response->getIsOk();
    }

    /**
     * @param ProductModel $productModel
     * @return array|\commonprj\extendedStdComponents\BaseCrudModel[]|null
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\HttpException
     */
    public function getModularComponents(ProductModel $productModel)
    {
        $this->requestUri = $this->getBaseUri($productModel->id . '/modular-components');
        $this->requestParams = [];
        $content = $this->getApiData();

        if (!empty($content)) {
            $result = $this->getArrayOfModels($content, ModularComponent::className());
        }

        return $result ?? null;
    }

    /**
     * @param ProductModel $productModel
     * @return array|\commonprj\extendedStdComponents\BaseCrudModel[]|null
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\HttpException
     */
    public function getProducts(ProductModel $productModel)
    {
        $this->requestUri = $this->getBaseUri($productModel->id . '/products');
        $this->requestParams = [];
        $content = $this->getApiData();

        if (!empty($content)) {
            $result = $this->getArrayOfModels($content, Product::className());
        }

        return $result ?? null;
    }

}