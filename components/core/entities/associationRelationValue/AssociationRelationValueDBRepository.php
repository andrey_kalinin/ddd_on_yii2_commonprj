<?php
/**
 * @package         FurniPrice
 * @subpackage      Core
 * @category        DBRepository
 * @created         2017.12.20
 * @author          Vasiliy Konakov
 * @updated         2018.01.19
 * @updatedBy       Vasiliy Konakov
 */

namespace commonprj\components\core\entities\associationRelationValue;

use commonprj\components\core\entities\abstractPropertyValue\AbstractPropertyValue;
use commonprj\extendedStdComponents\IBaseCrudModel;
use yii\db\Exception;
use commonprj\components\core\entities\relationValue\RelationValueDBRepository;

/**
 * Class AssociationRelationValueDBRepository
 * @package commonprj\components\core\entities\associationRelationValue
 */
class AssociationRelationValueDBRepository extends RelationValueDBRepository
{

    /**
     * @param AbstractPropertyValue $propertyValue
     * @return bool
     * @throws Exception
     */
    public function save(IBaseCrudModel $propertyValue, $runValidation = true, $attributes = null): bool
    {
        $propertyValue->isParent = false;

        return parent::save($propertyValue, $runValidation, $attributes);
    }

    /**
     * @param AbstractPropertyValue $propertyValue
     * @return AbstractPropertyValue
     */
    protected function createReverseRecord(AbstractPropertyValue $propertyValue)
    {
        $reversePropertyValue = clone $propertyValue;

        $reversePropertyValue->elementId = $propertyValue->value;
        $reversePropertyValue->value = $propertyValue->elementId;

        return $reversePropertyValue;
    }

}
