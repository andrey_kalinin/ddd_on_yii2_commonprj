<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 25.12.2017
 * Time: 12:17
 */

namespace commonprj\services;

use commonprj\components\core\entities\abstractPropertyValue\AbstractPropertyValue;
use commonprj\components\core\helpers\ClassAndContextHelper;
use commonprj\components\core\helpers\PostgreSqlArrayHelper;
use commonprj\components\core\models as CoreModels;
use commonprj\components\core\models\search as SearchModels;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Exception;
use yii\db\StaleObjectException;
use yii\db\Transaction;
use yii\helpers\Inflector;
use yii\web\HttpException;

class SearchDBSynchService
{
    const MULTIPLE_VALUE_RECORDS = [
        SearchModels\PropertyRangeRecord::class,
        SearchModels\PropertyArrayRecord::class,
    ];

    const LIST_VALUE_CLASS = CoreModels\PropertyValueListItemRecord::class;
    const TREE_VALUE_CLASS = CoreModels\PropertyValueTreeItemRecord::class;
    const COLOR_VALUE_CLASS = CoreModels\PropertyValueColorRecord::class;

    /**
     * @var ActiveRecord
     */
    private $coreActiveRecord;

    /**
     * @var ActiveRecord::class
     */
    private $searchRecordClass;

    /**
     * @var int
     */
    private $multiplicityId;

    /**
     * @var array
     */
    private $attributes;

    /**
     * @var int
     */
    private $propertyValueIdInSearchDB;

    /**
     * SearchDBSynchronizator constructor.
     * @param ActiveRecord $coreActiveRecord
     * @param int $multiplicityId
     */
    public function __construct(ActiveRecord $coreActiveRecord, $multiplicityId = AbstractPropertyValue::MULTIPLICITY_SCALAR)
    {
        $this->coreActiveRecord = $coreActiveRecord;
        $this->searchRecordClass = ClassAndContextHelper::getSearchRecordClassByCoreRecordClass($coreActiveRecord::className());
        $this->multiplicityId = $multiplicityId;

        $this->attributes = $this->coreActiveRecord->getAttributes(null, ['id', 'element_id', 'searchdb_property_value_id']);
    }

    /**
     * Save basic record
     * @return bool|ActiveRecord
     */
    public function save()
    {
        $attributes = $this->coreActiveRecord->getAttributes(null, ['searchdb_property_value_id']);
        // save record
        /**
         * @var ActiveRecord $searchDbRecord
         */

//        $primaryKeys = (new $this->searchRecordClass())->getPrimaryKey();
        $primaryKeys = $this->searchRecordClass::primaryKey();


        if ($primaryKeys) {
            $conditions = [];
            foreach ($primaryKeys as $key) {
                $conditions[$key] = $attributes[$key];
            }

            $existedSearchDbRecord = $this->searchRecordClass::findOne($conditions);
        }

        $searchDbRecord = $existedSearchDbRecord ?? new $this->searchRecordClass();
        $searchDbRecord->setAttributes($attributes);

        if (isset($attributes['id']) && $attributes['id']) {
            $searchDbRecord->id = $attributes['id'] ?? null;
        }

        $result = $searchDbRecord->save();

        if (!$result) {
            return $searchDbRecord;
        }

        if ($searchDbRecord->hasAttribute('id')) {
            $this->propertyValueIdInSearchDB = $searchDbRecord->getAttribute('id');
        }

        return true;
    }

    /**
     * Delete basic record
     * @return bool|false|int
     * @throws \Exception
     * @throws \Throwable
     * @throws StaleObjectException
     */
    public function delete()
    {
        /**
         * @var ActiveRecord $record
         */
        $condition = [];
        foreach ($this->coreActiveRecord->primaryKey() as $pk) {
            $condition[$pk] = $this->coreActiveRecord->getAttribute($pk);
        }
        $record = $this->searchRecordClass::findOne($condition);

        if ($record) {
            return $record->delete();
        }

        return true;
    }

    /**
     * @return array|bool|false|int|null|ActiveRecord
     * @throws \Exception
     * @throws \Throwable
     * @throws Exception
     * @throws StaleObjectException
     */
    public function deletePropertyValue()
    {
        // list
        if (in_array($this->coreActiveRecord::className(), [self::LIST_VALUE_CLASS, self::TREE_VALUE_CLASS])) {
            return $this->deletePropertyValue2elementRecord($this->coreActiveRecord->getAttribute('value'));
        }

        // array|range
        if ($this->isMultipleValue()) {
            return $propertyValueRecord = $this->searchRecordClass::deleteAll([
                'name'        => $this->attributes['name'],
                'property_id' => $this->attributes['property_id'],
            ]);
        }

        /**
         * @var ActiveRecord $propertyValueRecord
         */

        $conditions = [];

        foreach ($this->attributes as $key => $value) {
            $oldValue = $this->coreActiveRecord->getOldAttribute($key);

            if ($oldValue) {
                if ($key == 'value') {
                    if ($this->coreActiveRecord instanceof CoreModels\PropertyValueGeolocationRecord) {
                        //для point в postgres точка сравнивается так
                        $conditions[] = ['~=', 'value', $oldValue];
                    } else {
                        $conditions[] = ['value' => $oldValue];
                    }
                } else {
                    $conditions[] = ['=', $key, $oldValue];
                }
            }
        }

        $recordQuery = $this->searchRecordClass::find();

        foreach ($conditions as $condition) {
            $recordQuery = $recordQuery->where($condition);
        }

        $propertyValueRecord = $recordQuery->one();

        if (!$propertyValueRecord) {
            return true;
        }

        try {
            /**
             * @var Transaction $transaction
             */
            $transaction = Yii::$app->get('dbSearch')->beginTransaction();

            $result = $this->deletePropertyValue2elementRecord($propertyValueRecord->getAttribute('id'));

            if ($result !== true) {
                $transaction->rollBack();
                return $result;
            }

            $conditions = [];
            $conditions[] = ['=', 'property_id', $this->coreActiveRecord->property_id];

            if ($this->coreActiveRecord instanceof CoreModels\PropertyValueGeolocationRecord) {
                $value = $this->coreActiveRecord->value;
                //для point в postgres точка сравнивается так
                if (is_array($value)) {
                    $value = "(" . implode(',', $value) . ")";
                }

                $conditions[] = ['~=', 'value', $value];
            } else {
                $conditions[] = ['value' => $this->coreActiveRecord->value];
            }

            $recordQuery = $this->searchRecordClass::find();

            foreach ($conditions as $condition) {
                $recordQuery = $recordQuery->where($condition);
            }

            $propertyValueCount = $recordQuery->count();

            if ($propertyValueCount < 2) {
                $result = $propertyValueRecord->delete();

                if (!$result) {
                    $transaction->rollBack();
                    return $result;
                }
            }

            $transaction->commit();
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }

        return true;
    }

    /**
     * @return bool|ActiveRecord
     * @throws \Throwable
     * @throws Exception
     */
    public function saveScalarPropertyValue()
    {
        /**
         * @var Transaction $transaction
         */
        $transaction = Yii::$app->get('dbSearch')->beginTransaction();

        try {
            switch ($this->coreActiveRecord::className()) {
                case self::LIST_VALUE_CLASS:
                    $propertyValueId = $this->attributes['property_list_item_id'];
                    break;
                case self::COLOR_VALUE_CLASS:
                    $attributes = [
                        'property_id' => $this->coreActiveRecord->property_id,
                        'value' => strtoupper($this->coreActiveRecord->value),
                        'element_id' => $this->coreActiveRecord->element_id,

                    ];
                    $propertyValueId = $this->findPropertyValueRecordId($attributes);
                    break;
                default:
                    $dirtyAttributes = $this->coreActiveRecord->getDirtyAttributes();

                    if (empty($dirtyAttributes)) {
                        $transaction->commit();
                        $this->propertyValueIdInSearchDB = $this->coreActiveRecord->searchdb_property_value_id;
                        return true;
                    }

                    if (!$this->coreActiveRecord->getIsNewRecord()) {
                        $this->deletePropertyValue();
                    }


                    // ищем существующее значение
                    $propertyValueId = $this->findPropertyValueRecordId($this->attributes);

                    if (!$propertyValueId) {
                        /**
                         * @var ActiveRecord $searchDbRecord
                         */
                        $searchDbRecord = new $this->searchRecordClass($this->attributes);

                        if ($this->coreActiveRecord instanceof CoreModels\PropertyValueGeolocationRecord) {

                            if (is_array($searchDbRecord->value)) {
                                $searchDbRecord->value = "(" . implode(',', $searchDbRecord->value) . ")";
                            }

                        } elseif ($searchDbRecord instanceof SearchModels\PropertyValueMoneyRecord) {
                            $currencyService = $this->getCurrencyService();
                            $valueInBaseCurrency = (int)$currencyService->calcInCurrency($searchDbRecord->value);
                            $searchDbRecord->value_in_base_currency = $valueInBaseCurrency;

                        }

                        $result = $searchDbRecord->save();

                        if ($result !== true) {
                            $transaction->rollBack();
                            throw new HttpException(404, basename(__FILE__, '.php') . __LINE__);
                            return $searchDbRecord;
                        }

                        $propertyValueId = $searchDbRecord->getAttribute('id');
                    }

                    $this->propertyValueIdInSearchDB = $propertyValueId;
                    break;
            }

            // create propertyValue2element record
            $result = $this->createPropertyValue2elementRecord($propertyValueId);

            if ($result !== true) {
                $transaction->rollBack();
                return $result;
            }

            $transaction->commit();
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
        return true;
    }

    /**
     * @param string $scalarPropertyValueRecordClass
     * @param array $values
     * @return bool|ActiveRecord
     * @throws \Throwable
     * @throws Exception
     */
    public function saveMultiplePropertyValue(string $scalarPropertyValueRecordClass, array $values = [])
    {

        if ($scalarPropertyValueRecordClass == self::COLOR_VALUE_CLASS) {
            if (!empty($values)) {
                $records = SearchModels\PropertyValueColorRecord::findAll(['value' => $values]);
                foreach ((array)$records as $record) {
                    $this->createPropertyValue2elementRecord($record->id);
                }
            }
            return true;
        }
        $searchScalarPropertyValueRecordClass = ClassAndContextHelper::getSearchRecordClassByCoreRecordClass($scalarPropertyValueRecordClass);

        /**
         * @var Transaction $transaction
         */
        $transaction = Yii::$app->get('dbSearch')->beginTransaction();

        try {
            // save scalar values
            if (in_array($scalarPropertyValueRecordClass, [self::LIST_VALUE_CLASS, self::TREE_VALUE_CLASS])) {
                $valueIds = $values;
            } else {
                $valueIds = $this->getMultiplePropertyValueIds($searchScalarPropertyValueRecordClass, $values);
            }

            if (!is_array($valueIds)) {
                $transaction->rollBack();
                return $valueIds;
            }

            foreach ($valueIds as $valueId) {
                $this->createPropertyValue2elementRecord($valueId);
            }

            switch ($this->multiplicityId) {
                case AbstractPropertyValue::MULTIPLICITY_ARRAY:
                    $this->attributes['value_ids'] = PostgreSqlArrayHelper::array2pg($valueIds);
                    break;
                case AbstractPropertyValue::MULTIPLICITY_RANGE:
                    foreach ($valueIds as $key => $valueId) {
                        $this->attributes[$key] = $valueId;
                    }
                    break;
            }

            if (!in_array($searchScalarPropertyValueRecordClass, [self::LIST_VALUE_CLASS, self::TREE_VALUE_CLASS])) {
                // save array value
                $existedValueId = $this->findPropertyValueRecordId($this->attributes, $this->multiplicityId);

                if (!$existedValueId) {
                    /**
                     * @var ActiveRecord $searchDbRecord
                     */
                    $searchDbRecord = new $this->searchRecordClass($this->attributes);
                    $result = $searchDbRecord->save();

                    if (!$result) {
                        $transaction->rollBack();
                        return $searchDbRecord;
                    }
                } else {
                    $result = true;
                }

                if ($result !== true) {
                    $transaction->rollBack();
                    return $result;
                }
            }

            $transaction->commit();
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }

        return true;
    }

    public function getPropertyValueIdInSearchDB()
    {
        return $this->propertyValueIdInSearchDB;
    }

    /**
     * Возвращает int[] id созданных или найденных скалярных значений для range & array values
     * @param string $searchScalarPropertyValueRecordClass
     * @param array $values
     * @return array|ActiveRecord
     */
    private function getMultiplePropertyValueIds(string $searchScalarPropertyValueRecordClass, array $values)
    {
        $valueIds = [];

        foreach ($values as $key => $value) {
            $scalarValueAttributes = [
                'property_id' => $this->attributes['property_id'],
                'value'       => $value,
            ];

            $existedValueId = $this->findPropertyValueRecordId($scalarValueAttributes, AbstractPropertyValue::MULTIPLICITY_SCALAR, $searchScalarPropertyValueRecordClass);
            if ($existedValueId) {
                $valueIds[Inflector::underscore($key)] = $existedValueId;
            } else {
                /**
                 * @var ActiveRecord $valueRecord
                 */
                $valueRecord = new $searchScalarPropertyValueRecordClass($scalarValueAttributes);
                $result = $valueRecord->save();

                if (!$result) {
                    return $valueRecord;
                } else {
                    $valueIds[Inflector::underscore($key)] = $valueRecord->getAttribute('id');
                }
            }
        }

        return $valueIds;
    }

    /**
     * @param int $propertyValueId
     * @return bool|SearchModels\PropertyValue2elementRecord
     */
    private function createPropertyValue2elementRecord(int $propertyValueId)
    {
        if ($this->isMultipleValue() && $this->multiplicityId == AbstractPropertyValue::MULTIPLICITY_SCALAR) {
            return true;
        }

        $attributes = [
            'property_id'              => $this->attributes['property_id'],
            'property_multiplicity_id' => $this->multiplicityId,
            'property_value_id'        => $propertyValueId,
            'element_id'               => $this->coreActiveRecord->getAttribute('element_id'),
            'is_active'                => $this->coreActiveRecord->getElement()->one()->getAttribute('is_active'),
        ];

        $existedPropertyValue2element = SearchModels\PropertyValue2elementRecord::find()->where($attributes)->one();

        if ($existedPropertyValue2element) {
            return true;
        }

        $propertyValue2element = new SearchModels\PropertyValue2elementRecord($attributes);
        $result = $propertyValue2element->save();

        if (!$result) {
            return $propertyValue2element;
        } else {
            return true;
        }

    }

    /**
     * @param int $propertyValueId
     * @return array|bool|null|ActiveRecord
     * @throws \Exception
     * @throws \Throwable
     * @throws StaleObjectException
     */
    private function deletePropertyValue2elementRecord(int $propertyValueId)
    {
        $condition = [
            'property_id'       => $this->attributes['property_id'],
            'property_value_id' => $propertyValueId,
            'element_id'        => $this->coreActiveRecord->getAttribute('element_id'),
        ];

        $record = SearchModels\PropertyValue2elementRecord::find()->where($condition)->one();

        if (!$record) {
            return true;
        }

        $result = $record->delete();

        if ($result != true) {
            return $record;
        }

        return true;
    }

    /**
     * @return bool
     */
    private function isMultipleValue(): bool
    {
        return in_array($this->searchRecordClass, self::MULTIPLE_VALUE_RECORDS);
    }

    /**
     * @param array $attributes
     * @param int $multiplicityId
     * @param string|null $searchRecordClass
     * @return bool|int
     */
    private function findPropertyValueRecordId(array $attributes, $multiplicityId = AbstractPropertyValue::MULTIPLICITY_SCALAR, string $searchRecordClass = null)
    {

        if (!$searchRecordClass) {
            $searchRecordClass = $this->searchRecordClass;
        }

        if ($multiplicityId == AbstractPropertyValue::MULTIPLICITY_SCALAR) {
            $conditions = [
                'property_id' => $attributes['property_id'],
            ];

            // Создаем запрос на where в зависимости от текущего типа свойства
            if ($this->coreActiveRecord instanceof CoreModels\PropertyValueGeolocationRecord) {
                if (is_array($attributes['value'])) {
                    $value = "(" . implode(',', $attributes['value']) . ")";
                } else {
                    $value = $attributes['value'];
                }
                //для point в postgres точка сравнивается так
                $conditions[] = ['~=', 'value', $value];
            } else {
                $conditions[] = ['value' => $attributes['value']];
            }

        } else {
            $conditions = [
                'property_id' => $attributes['property_id'],
                [
                    'name' => $attributes['name'],
                ],
            ];
        }
        $recordQuery = $searchRecordClass::find();

        foreach ($conditions as $condition) {
            $recordQuery = $recordQuery->where($condition);
        }
        $record = $recordQuery->one();

        if ($record) {
            return $record->getattribute('id');
        } else {
            return false;
        }
    }


    /**
     * @return CurrencyService
     */
    private function getCurrencyService()
    {
        return Yii::$app->currency;
    }
}
