<?php

namespace commonprj\services\search;


class ReturnFilter
{
    public $filterKeys = [];

    public function __construct($filterKeys)
    {
        $this->filterKeys = $filterKeys;
    }

    /**
     * @return array
     */
    public function getFilterKeys(): array
    {
        return $this->filterKeys;
    }

    
}