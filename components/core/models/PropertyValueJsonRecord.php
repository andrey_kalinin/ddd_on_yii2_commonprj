<?php

namespace commonprj\components\core\models;

use commonprj\components\core\entities\property\Property;

/**
 * This is the model class for table "property_value_json".
 *
 * @property integer $id
 * @property integer $property_id
 * @property string $value
 * @property integer $element_id
 *
 * @property PropertyRecord $property
 */
class PropertyValueJsonRecord extends AbstractPropertyValueRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'property_value_json';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['property_id', 'element_id'], 'required'],
            [['property_id', 'element_id'], 'integer'],
            [['value'], 'string'],
            [['property_id', 'element_id', 'value'], 'unique', 'targetAttribute' => ['property_id', 'element_id', 'value'], 'message' => 'The combination of Property ID, Value and Element ID has already been taken.'],
            [['property_id'], 'exist', 'skipOnError' => true, 'targetClass' => PropertyRecord::className(), 'targetAttribute' => ['property_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'property_id' => 'Property ID',
            'value' => 'Value',
            'element_id' => 'Element ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty()
    {
        return $this->hasOne(PropertyRecord::className(), ['id' => 'property_id']);
    }
}
