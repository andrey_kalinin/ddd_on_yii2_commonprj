<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 3/5/2018
 * Time: 3:55 PM
 */

namespace commonprj\extendedStdComponents;


interface IBaseCrudModel
{
    /**
     * Удаляет запись текущего инстанса вместе со всеми зависимостями.
     */
    public function delete(): bool;

    /**
     * @return bool
     */
    public function update(): bool;
    /**
     * @return bool
     */
    public function save(): bool;

    /**
     * @return array
     */
    public static function primaryKey();
}