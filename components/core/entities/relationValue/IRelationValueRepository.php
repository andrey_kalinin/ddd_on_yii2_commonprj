<?php
/**
 * @package         FurniPrice
 * @subpackage      Core
 * @category        DBRepository
 * @created         20.12.2017
 * @author          Vasiliy Konakov
 */

namespace commonprj\components\core\entities\relationValue;

use commonprj\components\core\entities\element\Element;

/**
 * Interface IRelationValueRepository
 * @package commonprj\components\core\entities\relationValue
 */
interface IRelationValueRepository
{

}
