<?php

namespace commonprj\components\crm\entities\companyState;

use Yii;
use commonprj\components\core\entities\element\Element;
use commonprj\components\core\entities\element\ElementDBRepository;
use commonprj\components\core\entities\property\Property;
use commonprj\components\crm\entities\permissionGroup\PermissionGroup;

class CompanyStateDBRepository extends ElementDBRepository implements ICompanyStateRepository
{

    /**
     * @param CompanyState $companyState
     * @param int $permissionGroupId
     * @return bool
     */
    public function bindPermissionGroup(CompanyState $companyState, int $permissionGroupId): bool
    {
        $propertyId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($companyState,
            Property::PROPERTY_SYSNAME__COMPANY_STATE2PERMISSION_GROUP);

        return $companyState->bindAssociatedEntity($propertyId, $permissionGroupId);
    }

    /**
     * @param CompanyState $companyState
     * @param int $permissionGroupId
     * @return bool
     */
    public function unbindPermissionGroup(CompanyState $companyState, int $permissionGroupId): bool
    {
        $propertyId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($companyState,
            Property::PROPERTY_SYSNAME__COMPANY_STATE2PERMISSION_GROUP);

        return $companyState->unbindAssociatedEntity($propertyId, $permissionGroupId);
    }


    /**
     * @param CompanyState $companyState
     * @return PermissionGroup[]
     * @throws \yii\web\HttpException
     */
    public function getPermissionGroups(CompanyState $companyState): array
    {
        $propertyId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($companyState,
            Property::PROPERTY_SYSNAME__COMPANY_STATE2PERMISSION_GROUP);

        $elements = $companyState->getAssociatedEntities($propertyId);

        foreach ($elements as &$element) {
            /** @var Element $element */
            $element->properties = $element->getProperties();
        }

        return $elements;
    }
}