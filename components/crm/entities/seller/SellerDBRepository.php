<?php

namespace commonprj\components\crm\entities\seller;

use Yii;
use commonprj\components\core\entities\property\Property;
use commonprj\components\crm\entities\company\CompanyDBRepository;
use commonprj\components\crm\entities\manufacturer\Manufacturer;

class SellerDBRepository extends CompanyDBRepository implements ISellerRepository
{

    protected $organizationCodePrefix = "%s.S.";

    /**
     * @param Seller $seller
     * @param int $manufacturerId
     * @return bool
     * @throws \yii\web\HttpException
     */
    public function bindManufacturer(Seller $seller, int $manufacturerId): bool
    {
        $propertyManufacturerId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($seller,
            Property::PROPERTY_SYSNAME__SELLER2MANUFACTURER);

        return $seller->bindAssociatedEntity($propertyManufacturerId, $manufacturerId);
    }

    /**
     * @param Seller $seller
     * @param int $manufacturerId
     * @return bool
     * @throws \yii\web\HttpException
     */
    public function unbindManufacturer(Seller $seller, int $manufacturerId): bool
    {
        $propertyManufacturerId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($seller,
            Property::PROPERTY_SYSNAME__SELLER2MANUFACTURER);

        return $seller->unbindAssociatedEntity($propertyManufacturerId, $manufacturerId);
    }

    /**
     * @param Seller $seller
     * @return Manufacturer[]
     * @throws \yii\web\HttpException
     */
    public function getManufacturers(Seller $seller): array
    {
        $propertySeller2ManufacturerId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($seller,
            Property::PROPERTY_SYSNAME__SELLER2MANUFACTURER);

        $manufacturers = $seller->getAssociatedEntities($propertySeller2ManufacturerId);

        foreach ($manufacturers as &$manufacturer) {
            /** @var Manufacturer $manufacturer */
            $manufacturer->properties = $manufacturer->getProperties();
        }

        return $manufacturers;
    }

    /**
     * @param Seller $seller
     * @return array
     */
    public function findProductRequests(Seller $seller): array
    {
        // TODO: Implement findProductRequests() method.
    }


    protected function getDefaultOrganizationCode()
    {
        return 100000;
    }

    /**
     * @return string
     */
    protected function getEntityClass()
    {
        return Seller::class;
    }
}
