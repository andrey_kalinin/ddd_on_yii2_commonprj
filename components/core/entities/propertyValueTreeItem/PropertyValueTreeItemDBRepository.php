<?php

namespace commonprj\components\core\entities\propertyValueTreeItem;

use commonprj\components\core\entities\propertyValueListItem\PropertyValueListItemDBRepository;
use commonprj\components\core\models\PropertyTreeItemRecord;
use commonprj\components\core\models\PropertyValueTreeItemRecord;
use yii\db\ActiveRecord;

/**
 * Class PropertyValueTreeItemDBRepository
 * @package commonprj\components\core\entities\propertyValueTreeItem
 */
class PropertyValueTreeItemDBRepository extends PropertyValueListItemDBRepository
{
    /**
     * @var ActiveRecord::class
     */
    public $activeRecordClass = PropertyValueTreeItemRecord::class;


    protected function getListItemClass()
    {
        return PropertyTreeItemRecord::class;
    }
}
