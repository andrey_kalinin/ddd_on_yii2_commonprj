<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 1/19/2018
 * Time: 3:12 PM
 */

namespace commonprj\components\crm\entities\userRole;


use commonprj\components\crm\entities\companyRole\CompanyRole;
use commonprj\components\crm\entities\permission\Permission;

class UserRoleServiceRepository implements IUserRoleRepository
{
    /**
     * @param UserRole $userRole
     * @param int $permissionId
     * @return bool
     */
    public function bindPermission(UserRole $userRole, int $permissionId): bool
    {
        // TODO: Implement bindPermission() method.
    }

    /**
     * @param UserRole $userRole
     * @param int $permissionId
     * @return bool
     */
    public function unbindPermission(UserRole $userRole, int $permissionId): bool
    {
        // TODO: Implement unbindPermission() method.
    }

    /**
     * @param UserRole $userRole
     * @return Permission[]
     */
    public function getPermissions(UserRole $userRole): array
    {
        // TODO: Implement getPermissions() method.
    }

    /**
     * @param UserRole $userRole
     * @param int $companyRoleId
     * @return bool
     */
    public function bindCompanyRole(UserRole $userRole, int $companyRoleId): bool
    {
        // TODO: Implement bindCompanyRole() method.
    }

    /**
     * @param UserRole $userRole
     * @param int $companyRoleId
     * @return bool
     */
    public function unbindCompanyRole(UserRole $userRole, int $companyRoleId): bool
    {
        // TODO: Implement unbindCompanyRole() method.
    }

    /**
     * @param UserRole $userRole
     * @return CompanyRole
     */
    public function getCompanyRole(UserRole $userRole): CompanyRole
    {
        // TODO: Implement getCompanyRole() method.
    }

}