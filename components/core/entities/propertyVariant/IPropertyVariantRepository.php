<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 12/15/2017
 * Time: 12:34 PM
 */

namespace commonprj\components\core\entities\propertyVariant;

use commonprj\components\core\entities\abstractPropertyValue\AbstractPropertyValue;
use commonprj\components\core\entities\property\Property;
use commonprj\components\core\entities\propertyValidationRule\PropertyValidationRule;
use commonprj\components\core\entities\template\Template;
use commonprj\extendedStdComponents\IBaseRepository;


interface IPropertyVariantRepository extends IBaseRepository
{

    /**
     * @param PropertyVariant $propertyVariant
     * @return Property
     */
    public function getProperty(PropertyVariant $propertyVariant): ?Property;

    /**
     * @param PropertyVariant $propertyVariant
     * @return AbstractPropertyValue
     */
    public function getPropertyValue(PropertyVariant $propertyVariant): AbstractPropertyValue;

    /**
     * @param PropertyVariant $propertyVariant
     * @return PropertyValidationRule|null
     */
    public function getPropertyValidationRule(PropertyVariant $propertyVariant): ?PropertyValidationRule;

    /**
     * @param PropertyVariant $propertyVariant
     * @param int $elementClassId
     * @return Template|null
     */
    public function getTemplate(PropertyVariant $propertyVariant, int $elementClassId): ?Template;

    /**
     * @param PropertyVariant $propertyVariant
     * @param int $elementClassId
     * @return array
     */
    public function getElementCategories(PropertyVariant $propertyVariant, int $elementClassId): array;

}