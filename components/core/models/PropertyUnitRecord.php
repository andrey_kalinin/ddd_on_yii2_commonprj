<?php

namespace commonprj\components\core\models;

use Yii;

/**
 * This is the model class for table "property_unit".
 *
 * @property integer $id
 * @property string $name
 *
 * @property PropertyRecord[] $properties
 */
class PropertyUnitRecord extends AbstractBasicRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'property_unit';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 50],
            [['name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperties()
    {
        return $this->hasMany(PropertyRecord::className(), ['property_unit_id' => 'id']);
    }
}
