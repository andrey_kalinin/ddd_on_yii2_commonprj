<?php
/**
 * Created by daSilva.Rodrigues
 * Date: 23.09.2016
 */

namespace commonprj\components\core\entities\elementType;

use commonprj\components\core\entities\elementCategory\ElementCategory;
use commonprj\components\core\entities\elementClass\ElementClass;
use commonprj\extendedStdComponents\BaseCrudModel;
use yii\web\HttpException;

/**
 * Class ElementTypeRepository
 * @package commonprj\components\core\entities\elementType
 */
interface IElementTypeRepository
{

    /**
     * @param int $id
     * @return ElementCategory[]
     */
    public function getElementCategoriesById(int $id);

    /**
     * @param int $id
     * @return ElementClass
     */
    public function getElementClassById(int $id);

    /**
     * @param $id
     * @return array|BaseCrudModel
     * @throws HttpException
     */
    public function getVariantById($id);
}