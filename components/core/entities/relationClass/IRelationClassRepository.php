<?php
/**
 * Created by daSilva.Rodrigues
 * Date: 23.09.2016
 */
namespace commonprj\components\core\entities\relationClass;

use yii\web\HttpException;

/**
 * Class RelationClassRepository
 * @package commonprj\components\core\entities\relationClass
 */
interface IRelationClassRepository
{
    /**
     * @param $condition
     * @param $isRoot
     * @return array
     * @throws HttpException
     */
    public function getElementClassesById($condition, $isRoot);

}