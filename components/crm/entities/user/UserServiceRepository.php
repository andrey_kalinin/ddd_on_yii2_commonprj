<?php

namespace commonprj\components\crm\entities\User;

use commonprj\components\core\entities\element\ElementServiceRepository;
use commonprj\components\crm\entities\address\Address;
use commonprj\components\crm\entities\userRole\UserRole;
use commonprj\components\crm\specifications\accessSpecification\AccessSpecification;
use Yii;
use yii\httpclient\Client;

/**
 * Class UserServiceRepository
 * @package commonprj\components\crm\entities\User
 */
class UserServiceRepository extends ElementServiceRepository
{
    /**
     * @var Client
     */
    protected $restServer;

    /**
     * @var string
     */
    protected $baseUri = 'user/';

    /**
     * UserServiceRepository constructor.
     */
    public function __construct()
    {
        $this->restServer = Yii::$app->catalogManager;
    }

    /**
     * @param User $user
     * @param int $addressId
     * @return bool
     */
    public function bindAddress(User $user, int $addressId)
    {
        $uri = $this->getBaseUri($user->id . '/address/' . $addressId);
        $response = $this->restServer->post($uri)->send();
        return $response->getIsOk();
    }

    /**
     * @param User $user
     * @param int $addressId
     * @return bool
     */
    public function unbindAddress(User $user, int $addressId)
    {
        $uri = $this->getBaseUri($user->id . '/address/' . $addressId);
        $response = $this->restServer->delete($uri)->send();
        return $response->getIsOk();
    }

    /**
     * @param User $user
     * @param int|null $addressTypeId
     * @return array|\commonprj\extendedStdComponents\BaseCrudModel[]|null
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\HttpException
     */
    public function getAddresses(User $user, int $addressTypeId = null)
    {
        $this->requestUri = $this->getBaseUri($user->id . '/addresses');
        $this->requestParams = [];

        $queryParams = [];
        if (!is_null($addressTypeId)) {
            $queryParams = [
                'typeId' => $addressTypeId
            ];
        }

        $content = $this->getApiData($queryParams);

        if (!empty($content)) {
            $result = $this->getArrayOfModels($content, Address::className());
        }

        return $result ?? [];
    }

    /**
     * @param User $user
     * @return \commonprj\extendedStdComponents\BaseCrudModel|null
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\HttpException
     */
    public function getAccessSpecification(User $user)
    {
        $this->requestUri = $this->getBaseUri($user->id . '/access-specification');
        $this->requestParams = [];
        $content = $this->getApiData();

        if (!empty($content)) {
            $result = $this->getOneModel($content, AccessSpecification::className());
        }

        return $result ?? null;
    }

    /**
     * @param User $user
     * @param int $userRoleId
     * @return bool
     */
    public function bindUserRole(User $user, int $userRoleId)
    {
        $uri = $this->getBaseUri($user->id . '/user-role/' . $userRoleId);
        $response = $this->restServer->post($uri)->send();
        return $response->getIsOk();
    }

    /**
     * @param User $user
     * @param int $userRoleId
     * @return bool
     */
    public function unbindUserRole(User $user, int $userRoleId)
    {
        $uri = $this->getBaseUri($user->id . '/user-role/' . $userRoleId);
        $response = $this->restServer->delete($uri)->send();
        return $response->getIsOk();
    }

    /**
     * @param User $user
     * @return \commonprj\extendedStdComponents\BaseCrudModel|null
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\HttpException
     */
    public function getUserRole(User $user)
    {
        $this->requestUri = $this->getBaseUri($user->id . '/user-role');
        $this->requestParams = [];
        $content = $this->getApiData();

        if (!empty($content)) {
            $result = $this->getOneModel($content, UserRole::className());
        }

        return $result ?? null;
    }

}