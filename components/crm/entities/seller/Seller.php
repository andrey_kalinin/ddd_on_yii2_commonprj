<?php

namespace commonprj\components\crm\entities\seller;

use commonprj\components\core\entities\elementClass\ElementClass;
use commonprj\components\core\helpers\ClassAndContextHelper;
use commonprj\components\crm\entities\company\Company;
use commonprj\components\crm\entities\manufacturer\Manufacturer;
use Yii;

/**
 * Class Seller
 * @property ISellerRepository $repository
 * @package commonprj\components\crm\entities\seller
 */
class Seller extends Company
{
    /**
     * @var Manufacturer[]
     */
    public $manufacturers;

    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->repository = Yii::$app->sellerRepository;
    }

    /**
     * @param int $manufacturerId
     * @return bool
     */
    public function bindManufacturer(int $manufacturerId): bool
    {
        return $this->repository->bindManufacturer($this, $manufacturerId);
    }

    /**
     * @param int $manufacturerId
     * @return bool
     */
    public function unbindManufacturer(int $manufacturerId): bool
    {
        return $this->repository->unbindManufacturer($this, $manufacturerId);
    }

    /**
     * @return Manufacturer[]
     */
    public function getManufacturers(): array
    {
        return $this->repository->getManufacturers($this);
    }

    /**
     * @return array
     */
    public function findProductRequests(): array
    {
        return $this->repository->findProductRequests($this);
    }
}
