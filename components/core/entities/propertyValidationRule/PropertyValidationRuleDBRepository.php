<?php

namespace commonprj\components\core\entities\propertyValidationRule;

use commonprj\components\core\entities\property\Property;
use commonprj\components\core\entities\propertyVariant\PropertyVariant;
use commonprj\components\core\models\search\PropertyValidationRuleRecord;
use commonprj\extendedStdComponents\BaseCrudModel;
use commonprj\extendedStdComponents\BaseDBRepository;
use commonprj\extendedStdComponents\IBaseCrudModel;

/**
 * Class PropertyValidationRuleDBRepository
 * @package commonprj\components\core\entities\propertyValidationRule
 */
class PropertyValidationRuleDBRepository extends BaseDBRepository implements IPropertyValidationRuleRepository
{
    /**
     * @var string
     */
    public $activeRecordClass = PropertyValidationRuleRecord::class;

    /**
     * @param BaseCrudModel $entity
     * @param array|int $entityId
     * @throws \yii\web\NotFoundHttpException
     */
    public function populateRecord(BaseCrudModel $entity, $entityId)
    {
        parent::populateRecord($entity, $entityId);
        $entity->data = json_decode($entity->data);
        $propertyIds = [];
        $propertyValueIds = [];
        foreach ($entity->data as $object) {
            if (!empty($object->propertyId)) {
                $propertyIds[] = $object->propertyId;
                $propertyValueIds[$object->propertyId] = $object->propertyValueIds;
            }
        }
        $entity->relatedProperties = Property::findAll(['id' => $propertyIds]);
        foreach ($entity->relatedProperties as &$property) {
            $property->value = $propertyValueIds[$property->id];
        }
    }

    /**
     * @param IBaseCrudModel $entity
     * @param bool $runValidation
     * @param null $attributes
     * @return bool
     */
    public function save(IBaseCrudModel $entity, $runValidation = true, $attributes = null): bool
    {
        $entity->data = json_encode($entity->data);
        return parent::save($entity, $runValidation, $attributes);
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return array_merge(parent::attributes(), ['relatedProperties']);
    }

    /**
     * @param IBaseCrudModel $entity
     * @return bool
     */
    public function delete(IBaseCrudModel $entity): bool
    {
        $propertyVariantId = $entity->property_variant_id;

        $result =  parent::delete($entity);

        if ($result === true) {
            /**
             * @var PropertyVariant $propertyVariant
             */
            $propertyVariant = PropertyVariant::findAll($propertyVariantId);
            $propertyVariant->tryToDelete();
        }

        return $result;
    }
}