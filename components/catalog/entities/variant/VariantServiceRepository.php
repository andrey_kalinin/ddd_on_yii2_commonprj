<?php

namespace commonprj\components\catalog\entities\Variant;

use commonprj\components\catalog\entities\option\Option;
use commonprj\components\core\entities\element\ElementServiceRepository;
use Yii;
use yii\httpclient\Client;

/**
 * Class VariantServiceRepository
 * @package commonprj\components\catalog\entities\Variant
 */
class VariantServiceRepository extends ElementServiceRepository
{
    /**
     * @var Client
     */
    protected $restServer;

    /**
     * @var string
     */
    protected $baseUri = 'variant/';

    /**
     * VariantServiceRepository constructor.
     */
    public function __construct()
    {
        $this->restServer = Yii::$app->catalogManager;
    }

    /**
     * @param Variant $variant
     * @param int $optionId
     * @return bool
     */
    public function bindOption(Variant $variant, int $optionId)
    {
        $uri = $this->getBaseUri($variant->id . '/option/' . $optionId);
        $response = $this->restServer->post($uri)->send();
        return $response->getIsOk();
    }

    /**
     * @param Variant $variant
     * @param int $optionId
     * @return bool
     */
    public function unbindOption(Variant $variant, int $optionId)
    {
        $uri = $this->getBaseUri($variant->id . '/option/' . $optionId);
        $response = $this->restServer->delete($uri)->send();
        return $response->getIsOk();
    }

    /**
     * @param Variant $variant
     * @return \commonprj\extendedStdComponents\BaseCrudModel|null
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\HttpException
     */
    public function getOption(Variant $variant)
    {
        $this->requestUri = $this->getBaseUri($variant->id . '/option');
        $this->requestParams = [];
        $content = $this->getApiData();

        if (!empty($content)) {
            $result = $this->getOneModel($content, Option::className());
        }

        return $result ?? null;
    }

    /**
     * @return array
     * @throws \ReflectionException
     */
    public function attributes($entity)
    {
        return array_merge(parent::attributes($entity), ['productMaterials', 'priceCategory', 'materials', 'propertyValue;']);
    }
}