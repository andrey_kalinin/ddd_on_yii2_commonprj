<?php

namespace commonprj\components\crm\entities\Manufacturer;

use commonprj\components\catalog\entities\component\Component;
use commonprj\components\catalog\entities\materialCollection\MaterialCollection;
use commonprj\components\catalog\entities\priceCategory\PriceCategory;
use commonprj\components\catalog\entities\productMaterial\ProductMaterial;
use commonprj\components\catalog\entities\productModel\ProductModel;
use commonprj\components\crm\entities\company\CompanyServiceRepository;
use commonprj\components\crm\entities\seller\Seller;
use Yii;
use yii\httpclient\Client;

/**
 * Class ManufacturerServiceRepository
 * @package commonprj\components\crm\entities\Manufacturer
 */
class ManufacturerServiceRepository extends CompanyServiceRepository
{
    /**
     * @var Client
     */
    protected $restServer;

    /**
     * @var string
     */
    protected $baseUri = 'manufacturer/';

    /**
     * ManufacturerServiceRepository constructor.
     */
    public function __construct()
    {
        $this->restServer = Yii::$app->crmRestServer;
    }

    /**
     * @param Manufacturer $manufacturer
     * @return array|\commonprj\extendedStdComponents\BaseCrudModel[]|null
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\HttpException
     */
    public function getSellers(Manufacturer $manufacturer)
    {
        $this->requestUri = $this->getBaseUri($manufacturer->id . '/sellers');
        $this->requestParams = [];
        $content = $this->getApiData();

        if (!empty($content)) {
            $result = $this->getArrayOfModels($content, Seller::className());
        }

        return $result ?? [];
    }

    /**
     * @param Manufacturer $manufacturer
     * @return array|\commonprj\extendedStdComponents\BaseCrudModel[]|null
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\HttpException
     */
    public function getPriceCategories(Manufacturer $manufacturer)
    {
        $this->requestUri = $this->getBaseUri($manufacturer->id . '/price-categories');
        $this->requestParams = [];
        $content = $this->getApiData();

        if (!empty($content)) {
            $result = $this->getArrayOfModels($content, PriceCategory::className());
        }

        return $result ?? [];
    }

    /**
     * @param Manufacturer $manufacturer
     * @return array|\commonprj\extendedStdComponents\BaseCrudModel[]|null
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\HttpException
     */
    public function getProductMaterials(Manufacturer $manufacturer)
    {
        $this->requestUri = $this->getBaseUri($manufacturer->id . '/product-materials');
        $this->requestParams = [];
        $content = $this->getApiData();

        if (!empty($content)) {
            $result = $this->getArrayOfModels($content, ProductMaterial::className());
        }

        return $result ?? [];
    }

    /**
     * @param Manufacturer $manufacturer
     * @return array|\commonprj\extendedStdComponents\BaseCrudModel[]|null
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\HttpException
     */
    public function getMaterialCollections(Manufacturer $manufacturer)
    {
        $this->requestUri = $this->getBaseUri($manufacturer->id . '/material-collections');
        $this->requestParams = [];
        $content = $this->getApiData();

        if (!empty($content)) {
            $result = $this->getArrayOfModels($content, MaterialCollection::className());
        }

        return $result ?? [];
    }

    /**
     * @param Manufacturer $manufacturer
     * @return array|\commonprj\extendedStdComponents\BaseCrudModel[]|null
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\HttpException
     */
    public function getProductModels(Manufacturer $manufacturer)
    {
        $this->requestUri = $this->getBaseUri($manufacturer->id . '/product-models');
        $this->requestParams = [];
        $content = $this->getApiData();

        if (!empty($content)) {
            $result = $this->getArrayOfModels($content, ProductModel::className());
        }

        return $result ?? [];
    }

    /**
     * @param Manufacturer $manufacturer
     * @return array|\commonprj\extendedStdComponents\BaseCrudModel[]|null
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\HttpException
     */
    public function getComponents(Manufacturer $manufacturer)
    {
        $this->requestUri = $this->getBaseUri($manufacturer->id . '/components');
        $this->requestParams = [];
        $content = $this->getApiData();

        if (!empty($content)) {
            $result = $this->getArrayOfModels($content, Component::className());
        }

        return $result ?? [];
    }

}