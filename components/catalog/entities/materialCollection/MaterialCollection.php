<?php

namespace commonprj\components\catalog\entities\materialCollection;

use commonprj\components\core\entities\element\Element;
use commonprj\components\crm\entities\manufacturer\Manufacturer;
use Yii;

class MaterialCollection extends Element
{
    public $productMaterials;

    /**
     * Присвоение свойству доменного слоя $repository - соответствующего компонента
     * @inheritdoc
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->repository = Yii::$app->materialCollectionRepository;
    }

    /**
     *
     * @param int $manufacturerId
     * @return bool
     */
    public function bindManufacturer(int $manufacturerId): bool {
        return $this->repository->bindManufacturer($this, $manufacturerId);
    }

    /**
     *
     * @param int $manufacturerId
     * @return bool
     */
    public function unbindManufacturer(int $manufacturerId): bool {
        return $this->repository->unbindManufacturer($this, $manufacturerId);
    }

    /**
     *
     * @return Manufacturer
     */
    public function getManufacturer()
    {
        return $this->repository->getManufacturer($this);
    }

}
