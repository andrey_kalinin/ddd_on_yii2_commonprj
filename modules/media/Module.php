<?php

namespace commonprj\modules\media;

/**
 * Class Module
 * @package commonprj\modules\media
 */
class Module extends \yii\base\Module
{
    /* @var $mediaServiceUrl string URL to MediaService */

    public $mediaServiceUrl;

    /**
     * @var int
     */
    public $recognizeColors = 10;

    /**
     * @var float|int
     */
    public $maxSize = 1024 * 1024 * 2;

    /**
     * @var string
     */
    public $extensions = 'jpg, gif, png';
}
