<?php

namespace commonprj\components\crm\entities\companyRole;

use commonprj\components\crm\entities\permissionGroup\PermissionGroup;

interface ICompanyRoleRepository
{
    /**
     * @param CompanyRole $companyRole
     * @return array
     */
    public function getUserRoles(CompanyRole $companyRole): array;

    /**
     * @param CompanyRole $companyRole
     * @param int $permissionGroupId
     * @return bool
     */
    public function bindPermissionGroup(CompanyRole $companyRole, int $permissionGroupId): bool;

    /**
     * @param CompanyRole $companyRole
     * @param int $permissionGroupId
     * @return bool
     */
    public function unbindPermissionGroup(CompanyRole $companyRole, int $permissionGroupId): bool;

    /**
     * @param CompanyRole $companyRole
     * @return PermissionGroup[]
     */
    public function getPermissionGroups(CompanyRole $companyRole): array;

}