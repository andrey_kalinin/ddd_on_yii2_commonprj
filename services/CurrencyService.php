<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 3/30/2018
 * Time: 6:53 PM
 */

namespace commonprj\services;

use commonprj\components\core\entities\property\Property;
use yii\httpclient\Client;
use yii\web\NotFoundHttpException;

/**
 * Class CurrencyService
 * @package commonprj\services
 */
class CurrencyService
{

    /**
     * Код текущей валюты
     * @var null
     */
    protected $currentCurrency = null;

    /**
     * ID текущей валюты в списке валют
     * @var null
     */
    protected $currentCurrencyListItemId = null;

    /**
     * sysname списка валют
     * @var string
     */
    private $propertyListName = 'currency';

    /**
     * базвоый код валюты
     * @var string
     */
    private $defaultCurrency = 'USD';

    /**
     * ID базовой валюты в списке валют
     * @var null
     */
    private $defaultCurrencyListItemId = null;

    /**
     * Списко существующих валют с ID из листа в качестве ключей
     * @var array
     */
    private $existCurrency = [];

    /**
     * uri получения курса валют
     * @var string
     */
    private $exchangeRatesUrl = 'https://openexchangerates.org';

    /**
     * ключ для АПИ получения курса валют
     * @var string
     */
    private $exchangeRatesKey = '0e5819027ff7455185d385a458353a08';


    /**
     * Получение нормализованного массива по валюте в формате, суммы переводит в центы
     * value - значение в переданной валюте
     * currency - валюта
     * valueInBaseCurrency - значение в базовой валюте
     * currencyListItemId - значение ID в листе валюты
     * @param $data
     * @return array
     * @throws NotFoundHttpException
     */
    public function getCurrencyData($data)
    {
        $value = $data['value'] ?? $data;
        $currency = $data['currency'] ?? $this->getCurrentCurrency() ?? $this->getDefaultCurrency();

        // Если текущая не совпадает с переданной, устанавливаем переданную как текущую
        if ($currency != $this->getCurrentCurrency()) {
            $this->setCurrentCurrency($currency);
        }

        $currencyListItemId = $this->getCurrentCurrencyListItemId();

        $result = [
            'value'              => $value * 100,
            'currency'           => $currency,
            'currencyListItemId' => $currencyListItemId,
        ];

        return $result;
    }


    /**
     * получение текущей валюты
     * @return string
     */
    public function getCurrentCurrency()
    {
        return $this->currentCurrency;
    }

    /**
     * получение валюты по-умолчанию
     * @return string
     */
    public function getDefaultCurrency()
    {
        return $this->defaultCurrency;
    }

    /**
     * Получение ID значения листа валют для валюты по-умолчанию
     * @return int|null|string
     */
    public function getDefaultCurrencyListItemId()
    {
        if (!$this->defaultCurrencyListItemId) {
            $existCurrency = $this->getExistCurrency();
            $currency = $this->getDefaultCurrency();

            foreach ($existCurrency as $listItemId => $listCurrency) {
                if ($listCurrency == $currency) {
                    $this->defaultCurrencyListItemId = $listItemId;
                    break;
                }
            }
        }

        return $this->defaultCurrencyListItemId;
    }

    /**
     * установка текущей валюты и значения id листа валюты
     * @param $currency
     * @return self
     */
    public function setCurrentCurrency($currency)
    {
        if (!$this->getCurrentCurrency() || $currency != $this->getCurrentCurrency()) {

            $existCurrency = $this->getExistCurrency();
            $currency = $currency ?? $this->getDefaultCurrency();

            foreach ($existCurrency as $listItemId => $listCurrency) {
                if ($listCurrency == $currency) {
                    $this->currentCurrency = $listCurrency;
                    $this->currentCurrencyListItemId = $listItemId;
                    break;
                }
            }

            if (!$this->currentCurrencyListItemId) {
                if ($currency != $this->getDefaultCurrency()) {
                    $this->setCurrentCurrency($this->getDefaultCurrency());
                } else {
                    throw new NotFoundHttpException(basename(__FILE__, '.php') . __LINE__);
                }
            }
        }

        return $this;
    }

    /**
     * Получения списка валют существующих в системе
     * @return array
     */
    public function getExistCurrency()
    {
        if (!$this->existCurrency) {
            $property = Property::findOne(['sysname' => $this->propertyListName]);
            $listItems = $property->getListItems();

            foreach ($listItems as $listItem) {
                $this->existCurrency[$listItem->id] = $listItem->item;
            }
        }

        return $this->existCurrency;
    }

    /**
     * получение id в листе валюты
     * @return null
     * @throws NotFoundHttpException
     */
    public function getCurrentCurrencyListItemId()
    {
        if (!$this->currentCurrencyListItemId) {
            $currentCurrency = $this->getCurrentCurrency() ?? $this->getDefaultCurrency();
            $this->setCurrentCurrency($currentCurrency);
        }

        return $this->currentCurrencyListItemId;
    }

    /**
     * высчитывает значение в базовой валюте по переданным значению и кода валюты или списка в листе валют
     * @param $value
     * @param null $currency
     * @param null $currencyListItemId
     * @return float
     */
    public function calcInCurrency($value, $currency = null, $currencyListItemId = null)
    {
        $isInDefaultCurrency = ($currency == $this->getDefaultCurrency())
            || ($currencyListItemId == $this->getDefaultCurrencyListItemId());

        $currency = $currency ?? $this->getCurrentCurrency();

        //переводим только если цена задана не в базовой валюте
        if (!$isInDefaultCurrency) {
            $exchangeAllRates = $this->getExchangeRates($this->defaultCurrency);
            $exchangeRates = $exchangeAllRates[$this->defaultCurrency];

            foreach ($exchangeRates as $currencyRate => $exchangeRate) {
                if ($currencyRate == $currency) {
                    $currencyValue = round($value / $exchangeRate, 2);
                    break;
                }
            }
        }

        return $currencyValue ?? $value;
    }

    /**
     * Получение текущих курсов валют
     * @return array
     */
    public function getExchangeRates($currencies = null)
    {
        //FIXME: DELETE ME
        return
            [
                "timestamp" => 1522832400,
                "USD"       => [
                    "BYN" => 1.964245,
                    "CNY" => 6.3024,
                    "DKK" => 6.061057,
                    "EUR" => 0.813592,
                    "GBP" => 0.710381,
                    "MXN" => 18.293125,
                    "PLN" => 3.422806,
                    "RUB" => 57.866,
                    "SEK" => 8.385826,
                    "TRY" => 4.012614,
                    "UAH" => 26.318011,
                    "USD" => 1,
                ],
                "RUB"       => [
                    "BYN" => 0.033945,
                    "CNY" => 0.108914,
                    "DKK" => 0.104743,
                    "EUR" => 0.01406,
                    "GBP" => 0.012276,
                    "MXN" => 0.316129,
                    "PLN" => 0.059151,
                    "RUB" => 1,
                    "SEK" => 0.144918,
                    "TRY" => 0.069343,
                    "UAH" => 0.45481,
                    "USD" => 0.017281,
                ],
                "BYN"       => [
                    "BYN" => 1,
                    "CNY" => 3.208561,
                    "DKK" => 3.085693,
                    "EUR" => 0.414201,
                    "GBP" => 0.361656,
                    "MXN" => 9.313057,
                    "PLN" => 1.742556,
                    "RUB" => 29.459667,
                    "SEK" => 4.269236,
                    "TRY" => 2.042828,
                    "UAH" => 13.398539,
                    "USD" => 0.509101,
                ],
                "GBP"       => [
                    "BYN" => 2.765059,
                    "CNY" => 8.871862,
                    "DKK" => 8.532125,
                    "EUR" => 1.14529,
                    "GBP" => 1,
                    "MXN" => 25.751156,
                    "PLN" => 4.81827,
                    "RUB" => 81.457727,
                    "SEK" => 11.804692,
                    "TRY" => 5.64854,
                    "UAH" => 37.047755,
                    "USD" => 1.407696,
                ],
                "EUR"       => [
                    "BYN" => 2.414287,
                    "CNY" => 7.746387,
                    "DKK" => 7.449749,
                    "EUR" => 1,
                    "GBP" => 0.873141,
                    "MXN" => 22.484392,
                    "PLN" => 4.207029,
                    "RUB" => 71.124088,
                    "SEK" => 10.307161,
                    "TRY" => 4.931972,
                    "UAH" => 32.347916,
                    "USD" => 1.229117,
                ],
                "PLN"       => [
                    "BYN" => 0.57387,
                    "CNY" => 1.841296,
                    "DKK" => 1.770786,
                    "EUR" => 0.237697,
                    "GBP" => 0.207543,
                    "MXN" => 5.344482,
                    "PLN" => 1,
                    "RUB" => 16.906011,
                    "SEK" => 2.449986,
                    "TRY" => 1.172317,
                    "UAH" => 7.689016,
                    "USD" => 0.292158,
                ],
                "UAH"       => [
                    "BYN" => 0.074635,
                    "CNY" => 0.239471,
                    "DKK" => 0.230301,
                    "EUR" => 0.030914,
                    "GBP" => 0.026992,
                    "MXN" => 0.69508,
                    "PLN" => 0.130056,
                    "RUB" => 2.198722,
                    "SEK" => 0.318634,
                    "TRY" => 0.152466,
                    "UAH" => 1,
                    "USD" => 0.037997,
                ],
                "CNY"       => [
                    "BYN" => 0.311666,
                    "CNY" => 1,
                    "DKK" => 0.961706,
                    "EUR" => 0.129092,
                    "GBP" => 0.112716,
                    "MXN" => 2.902565,
                    "PLN" => 0.543096,
                    "RUB" => 9.181582,
                    "SEK" => 1.330577,
                    "TRY" => 0.63668,
                    "UAH" => 4.175871,
                    "USD" => 0.15867,
                ],
                "TRY"       => [
                    "BYN" => 0.489518,
                    "CNY" => 1.570647,
                    "DKK" => 1.510501,
                    "EUR" => 0.202759,
                    "GBP" => 0.177037,
                    "MXN" => 4.558905,
                    "PLN" => 0.853012,
                    "RUB" => 14.421023,
                    "SEK" => 2.089866,
                    "TRY" => 1,
                    "UAH" => 6.558819,
                    "USD" => 0.249214,
                ],
                "DKK"       => [
                    "BYN" => 0.324076,
                    "CNY" => 1.039819,
                    "DKK" => 1,
                    "EUR" => 0.134233,
                    "GBP" => 0.117204,
                    "MXN" => 3.018141,
                    "PLN" => 0.564721,
                    "RUB" => 9.54718,
                    "SEK" => 1.383558,
                    "TRY" => 0.662032,
                    "UAH" => 4.342149,
                    "USD" => 0.164988,
                ],
                "SEK"       => [
                    "BYN" => 0.234234,
                    "CNY" => 0.751554,
                    "DKK" => 0.722774,
                    "EUR" => 0.09702,
                    "GBP" => 0.084712,
                    "MXN" => 2.181434,
                    "PLN" => 0.408166,
                    "RUB" => 6.900454,
                    "SEK" => 1,
                    "TRY" => 0.4785,
                    "UAH" => 3.138392,
                    "USD" => 0.119249,
                ],
                "MXN"       => [
                    "BYN" => 0.107376,
                    "CNY" => 0.344523,
                    "DKK" => 0.33133,
                    "EUR" => 0.044475,
                    "GBP" => 0.038833,
                    "MXN" => 1,
                    "PLN" => 0.187109,
                    "RUB" => 3.163265,
                    "SEK" => 0.458414,
                    "TRY" => 0.219351,
                    "UAH" => 1.438683,
                    "USD" => 0.054665,
                ],
            ];

        if ($currencies) {
            $currencies = (array)$currencies;
            $currencies = array_intersect($currencies, $this->getExistCurrency());
        } else {
            $currencies = array_values($this->getExistCurrency());
        }


        $client = new Client();

        $rates = [];
        foreach ((array)$currencies as $baseCurrency) {
            $request = $client->createRequest();
            $request
                ->setUrl($this->exchangeRatesUrl . '/api/latest.json')
                ->setData([
                    'app_id'  => $this->exchangeRatesKey,
                    'symbols' => implode(',', $this->getExistCurrency()),
                    'base'    => $baseCurrency,
                ]);

            $response = $request->send();

            $result = $response->getData();

            $rates["timestamp"] = $result["timestamp"];

            foreach ($result['rates'] ?? [] as $currency => $ratesData) {
                $rates[$baseCurrency][$currency] = $ratesData;
            }
        }

        return $rates;
    }


}