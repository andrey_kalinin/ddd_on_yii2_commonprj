<?php

namespace commonprj\components\catalog\entities\productMaterial;

use commonprj\components\catalog\entities\materialCollection\MaterialCollection;
use commonprj\components\catalog\entities\materialGroup\MaterialGroup;
use commonprj\components\catalog\entities\priceCategory\PriceCategory;
use commonprj\components\core\entities\element\ElementDBRepository;
use commonprj\components\core\entities\property\Property;
use commonprj\components\crm\entities\manufacturer\Manufacturer;
use commonprj\components\catalog\entities\material\Material;
use commonprj\extendedStdComponents\BaseCrudModel;
use Yii;

class ProductMaterialDBRepository extends ElementDBRepository implements IProductMaterialRepository
{

    /**
     * @param ProductMaterial $productMaterial
     * @param int $manufacturerId
     * @return bool
     * @throws \commonprj\components\core\entities\element\Exception
     * @throws \yii\web\HttpException
     */
    public function bindManufacturer(ProductMaterial $productMaterial, int $manufacturerId): bool
    {
        $propertyProductMaterial2ManufacturerId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($productMaterial, Property::PROPERTY_SYSNAME__PRODUCT_MATERIAL2MANUFACTURER);

        return $productMaterial->bindAssociatedEntity($propertyProductMaterial2ManufacturerId, $manufacturerId);
    }

    /**
     * @param ProductMaterial $productMaterial
     * @param int $manufacturerId
     * @return bool
     * @throws \commonprj\components\core\entities\element\Exception
     * @throws \yii\web\HttpException
     */
    public function unbindManufacturer(ProductMaterial $productMaterial, int $manufacturerId): bool
    {
        $propertyProductMaterial2ManufacturerId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($productMaterial, Property::PROPERTY_SYSNAME__PRODUCT_MATERIAL2MANUFACTURER);

        return $productMaterial->unbindAssociatedEntity($propertyProductMaterial2ManufacturerId, $manufacturerId);
    }

    /**
     * @param ProductMaterial $productMaterial
     * @return Manufacturer
     * @throws \yii\web\HttpException
     */
    public function getManufacturer(ProductMaterial $productMaterial): Manufacturer
    {
        $propertyProductMaterial2ManufacturerId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($productMaterial, Property::PROPERTY_SYSNAME__PRODUCT_MATERIAL2MANUFACTURER);
        $manufacturer = $productMaterial->getAssociatedEntity($propertyProductMaterial2ManufacturerId);
        $manufacturer->properties = $manufacturer->getProperties();

        return $manufacturer;
    }

    /**
     * @param ProductMaterial $productMaterial
     * @param int $materialId
     * @return bool
     * @throws \commonprj\components\core\entities\element\Exception
     * @throws \yii\web\HttpException
     */
    public function bindMaterial(ProductMaterial $productMaterial, int $materialId): bool
    {
        $propertyProductMaterial2MaterialId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($productMaterial, Property::PROPERTY_SYSNAME__PRODUCT_MATERIAL2MATERIAL);

        return $productMaterial->bindAssociatedEntity($propertyProductMaterial2MaterialId, $materialId);
    }

    /**
     * @param ProductMaterial $productMaterial
     * @param int $materialId
     * @return bool
     * @throws \commonprj\components\core\entities\element\Exception
     * @throws \yii\web\HttpException
     */
    public function unbindMaterial(ProductMaterial $productMaterial, int $materialId): bool
    {
        $propertyProductMaterial2MaterialId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($productMaterial, Property::PROPERTY_SYSNAME__PRODUCT_MATERIAL2MATERIAL);

        return $productMaterial->bindAssociatedEntity($propertyProductMaterial2MaterialId, $materialId);
    }

    /**
     * @param ProductMaterial $element
     * @return Material
     * @throws \yii\web\HttpException
     */
    public function getMaterial(ProductMaterial $element): Material
    {
        $relationId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($element, Property::PROPERTY_SYSNAME__PRODUCT_MATERIAL2MATERIAL);
        $material = $element->getAssociatedEntity($relationId);
        $material->properties = $material->getProperties();

        return $material;
    }

    /**
     * @param ProductMaterial $productMaterial
     * @param int $materialCollectionId
     * @return bool
     * @throws \commonprj\components\core\entities\element\Exception
     * @throws \yii\web\HttpException
     */
    public function bindMaterialCollection(ProductMaterial $productMaterial, int $materialCollectionId): bool
    {
        $propertyProductMaterial2MaterialCollectionId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($productMaterial, Property::PROPERTY_SYSNAME__PRODUCT_MATERIAL2MATERIAL_COLLECTION);

        return $productMaterial->bindAssociatedEntity($propertyProductMaterial2MaterialCollectionId, $materialCollectionId);
    }

    /**
     * @param ProductMaterial $productMaterial
     * @param int $materialCollectionId
     * @return bool
     * @throws \commonprj\components\core\entities\element\Exception
     * @throws \yii\web\HttpException
     */
    public function unbindMaterialCollection(ProductMaterial $productMaterial, int $materialCollectionId): bool
    {
        $propertyProductMaterial2MaterialCollectionId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($productMaterial, Property::PROPERTY_SYSNAME__PRODUCT_MATERIAL2MATERIAL_COLLECTION);

        return $productMaterial->unbindAssociatedEntity($propertyProductMaterial2MaterialCollectionId, $materialCollectionId);
    }

    /**
     * @param ProductMaterial $productMaterial
     * @return MaterialCollection
     * @throws \yii\web\HttpException
     */
    public function getMaterialCollection(ProductMaterial $productMaterial): MaterialCollection
    {
        $propertyProductMaterial2MaterialCollectionId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($productMaterial, Property::PROPERTY_SYSNAME__PRODUCT_MATERIAL2MATERIAL_COLLECTION);
        $materialCollection = $productMaterial->getAssociatedEntity($propertyProductMaterial2MaterialCollectionId);
        $materialCollection->properties = $materialCollection->getProperties();

        return $materialCollection;
    }

    /**
     * @param ProductMaterial $productMaterial
     * @param int $priceCategoryId
     * @return bool
     * @throws \commonprj\components\core\entities\element\Exception
     * @throws \yii\web\HttpException
     */
    public function bindPriceCategory(ProductMaterial $productMaterial, int $priceCategoryId): bool
    {
        $propertyProductMaterial2PriceCategoryId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($productMaterial, Property::PROPERTY_SYSNAME__PRODUCT_MATERIAL2PRICE_CATEGORY);

        return $productMaterial->bindAssociatedEntity($propertyProductMaterial2PriceCategoryId, $priceCategoryId);
    }

    /**
     * @param ProductMaterial $productMaterial
     * @param int $priceCategoryId
     * @return bool
     * @throws \commonprj\components\core\entities\element\Exception
     * @throws \yii\web\HttpException
     */
    public function unbindPriceCategory(ProductMaterial $productMaterial, int $priceCategoryId): bool
    {
        $propertyProductMaterial2PriceCategoryId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($productMaterial, Property::PROPERTY_SYSNAME__PRODUCT_MATERIAL2PRICE_CATEGORY);

        return $productMaterial->unbindAssociatedEntity($propertyProductMaterial2PriceCategoryId, $priceCategoryId);
    }

    /**
     * @param ProductMaterial $productMaterial
     * @return PriceCategory
     * @throws \yii\web\HttpException
     */
    public function getPriceCategory(ProductMaterial $productMaterial): PriceCategory
    {
        $propertyProductMaterial2PriceCategoryId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($productMaterial, Property::PROPERTY_SYSNAME__PRODUCT_MATERIAL2PRICE_CATEGORY);
        $priceCategory = $productMaterial->getAssociatedEntity($propertyProductMaterial2PriceCategoryId);
        $priceCategory->properties = $priceCategory->getProperties();

        return $priceCategory;
    }

    /**
     * @param ProductMaterial $productMaterial
     * @return MaterialGroup
     * @throws \yii\web\HttpException
     */
    public function getMaterialGroup(ProductMaterial $productMaterial): MaterialGroup
    {
        $relationId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($productMaterial, Property::PROPERTY_SYSNAME__PRODUCT_MATERIAL2MATERIAL_GROUP);
        $materialGroup = $productMaterial->getAssociatedEntity($relationId);
        $materialGroup->properties = $materialGroup->getProperties();

        return $materialGroup;
    }

    /**
     * @param ProductMaterial $productMaterial
     * @param int $materialGroupId
     * @return bool
     * @throws \commonprj\components\core\entities\element\Exception
     * @throws \yii\web\HttpException
     */
    public function bindMaterialGroup(ProductMaterial $productMaterial, int $materialGroupId): bool
    {
        $relationId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($productMaterial, Property::PROPERTY_SYSNAME__PRODUCT_MATERIAL2MATERIAL_GROUP);
        return $productMaterial->bindAssociatedEntity($relationId, $materialGroupId);
    }

    /**
     * @param ProductMaterial $productMaterial
     * @param int $materialGroupId
     * @return bool
     * @throws \commonprj\components\core\entities\element\Exception
     * @throws \yii\web\HttpException
     */
    public function unbindMaterialGroup(ProductMaterial $productMaterial, int $materialGroupId): bool
    {
        $relationId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($productMaterial, Property::PROPERTY_SYSNAME__PRODUCT_MATERIAL2MATERIAL_GROUP);
        return $productMaterial->unbindAssociatedEntity($relationId, $materialGroupId);
    }


    /**
     * @param BaseCrudModel $entity
     * @param int $entitiesId
     * @throws \commonprj\components\core\entities\element\NotFoundHttpException
     * @throws \yii\web\HttpException
     */
    public function populateRecord(BaseCrudModel $entity, $entitiesId)
    {
        parent::populateRecord($entity, $entitiesId);

        $relations = [
            'priceCategoryId'      => Yii::$app->propertyService->getPropertyIdByElementAndSysname($entity, Property::PROPERTY_SYSNAME__PRODUCT_MATERIAL2PRICE_CATEGORY),
            'materialCollectionId' => Yii::$app->propertyService->getPropertyIdByElementAndSysname($entity, Property::PROPERTY_SYSNAME__PRODUCT_MATERIAL2MATERIAL_COLLECTION),
            'materialId'           => Yii::$app->propertyService->getPropertyIdByElementAndSysname($entity, Property::PROPERTY_SYSNAME__PRODUCT_MATERIAL2MATERIAL),
            'materialGroupId'      => Yii::$app->propertyService->getPropertyIdByElementAndSysname($entity, Property::PROPERTY_SYSNAME__PRODUCT_MATERIAL2MATERIAL_GROUP),
        ];

        $relationIds = $this->getAssociatedEntitiesIds($entity, array_values($relations));

        foreach ($relations as $relation => $id) {
            $entity->{$relation} = reset($relationIds[$id]);
        }
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return array_merge(parent::attributes(), ['priceCategoryId', 'materialCollectionId', 'materialId', 'materialGroupId']);
    }
}
