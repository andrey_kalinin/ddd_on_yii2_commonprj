<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 27.12.2017
 * Time: 11:02
 */

namespace commonprj\components\core\models;

use commonprj\services\SearchDBSynchService;
use yii\db\ActiveRecord;
use Yii;

abstract class AbstractBasicRecord extends ActiveRecord
{
    /**
     * @param bool $runValidation
     * @param null $attributeNames
     * @return bool|ActiveRecord
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        return $this->searchDbSynchronizationWrapper('save', $runValidation, $attributeNames);
    }

    /**
     * @return bool|false|int
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function delete()
    {
        return $this->searchDbSynchronizationWrapper('delete');
    }

    /**
     * @param $method
     * @param bool $runValidation
     * @param null $attributeNames
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    private function searchDbSynchronizationWrapper($method, $runValidation = true, $attributeNames = null)
    {
        $transaction = Yii::$app->getDb()->beginTransaction();

        try {
            if ($method == 'save') {
                $result = parent::$method($runValidation, $attributeNames);
            } else {
                $result = parent::$method();
            }

            if ($result != true) {
                return $result;
            }

            $searchDbSynch = new SearchDBSynchService($this);
            $result = $searchDbSynch->$method();

            if ($result === false) {
                $transaction->rollBack();
                throw new \Exception(sprintf("Не удалась операция SearchDBSynchService"));
            }

            if ($this->hasAttribute('searchdb_property_value_id')) {
                $this->searchdb_property_value_id = $searchDbSynch->getPropertyValueIdInSearchDB();

                if ($method == 'save') {
                    $result = parent::$method($runValidation, $attributeNames);
                } else {
                    $result = parent::$method();
                }
                if ($result === false) {
                    $transaction->rollBack();
                    throw new \Exception(sprintf("Не удалась операция SearchDBSynchService"));
                }
            }

            $transaction->commit();
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }

        if ($this->hasAttribute('searchdb_property_value_id')) {
            $this->setAttribute('searchdb_property_value_id', $searchDbSynch->getPropertyValueIdInSearchDB());
        }

        return true;
    }
}