<?php

namespace commonprj\components\crm\entities\Customer;

use commonprj\components\crm\entities\productRequest\ProductRequest;
use commonprj\components\crm\entities\user\UserServiceRepository;
use Yii;
use yii\httpclient\Client;

/**
 * Class CustomerServiceRepository
 * @package commonprj\components\crm\entities\Customer
 */
class CustomerServiceRepository extends UserServiceRepository
{
    /**
     * @var Client
     */
    protected $restServer;

    /**
     * @var string
     */
    protected $baseUri = 'customer/';

    /**
     * CustomerServiceRepository constructor.
     */
    public function __construct()
    {
        $this->restServer = Yii::$app->crmRestServer;
    }

    /**
     * @param Customer $customer
     * @return array|\commonprj\extendedStdComponents\BaseCrudModel[]|null
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\HttpException
     */
    public function getProductRequests(Customer $customer)
    {
        $this->requestUri = $this->getBaseUri($customer->id . '/product-requests');
        $this->requestParams = [];
        $content = $this->getApiData();

        if (!empty($content)) {
            $result = $this->getArrayOfModels($content, ProductRequest::className());
        }

        return $result ?? null;
    }

}