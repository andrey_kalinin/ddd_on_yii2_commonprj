<?php
/**
 * @package         FurniPrice
 * @subpackage      Catalog
 * @category        Entity
 * @created         2018.01.22
 * @author          Vasiliy Konakov
 * @updated         2018.01.23
 * @updatedBy       Vasiliy Konakov
 */

namespace commonprj\components\catalog\entities\materialGroup;

use Yii;
use yii\db\Exception;
use yii\web\HttpException;
use commonprj\components\core\models\ElementRecord;
use commonprj\components\core\entities\property\Property;
use commonprj\components\catalog\entities\material\Material;
use commonprj\components\core\entities\element\ElementDBRepository;

/**
 * Class MaterialGroupDBRepository
 * @package commonprj\components\catalog\entities\materialGroup
 */
class MaterialGroupDBRepository extends ElementDBRepository implements IMaterialGroupRepository
{
    /**
     * getMaterials
     * @param MaterialGroup $element
     * @return Material[]
     * @throws HttpException|Exception
     */
    public function getMaterials(MaterialGroup $element): array
    {
        $materials = [];
        foreach ($this->getAssociatedEntities($element,
            Yii::$app->propertyService->getPropertyIdByElementAndSysname($element,
                Property::PROPERTY_SYSNAME__MATERIAL2MATERIAL_GROUP)) as $item) {
            $materials[$item->id] = Material::findOne($item->id);
        }
        
        $materialsTree = (new Material())->getFullListInHierarchy();

        $result = [];
        foreach ($materials as $material) {
            $tree = $this->findBranch($material->id, $materialsTree);

            if(!is_null($tree)) {
                $result[] = $tree;
            }
        }

        return $result;
    }

    /**
     * @param int $id
     * @param array $tree
     * @return mixed|null
     */
    private function findBranch(int $id, array $tree)
    {
        foreach ($tree as $key => $element) {
            if ($id == $element['id']) {
                return $tree[$key];
            }
        }

        foreach ($tree as $key => $element) {
            $result = $this->findBranch($id, (array)$element['hierarchyChildren']);
            if (!empty($result)) {
                return $result;
            }
        }

        return null;
    }
}