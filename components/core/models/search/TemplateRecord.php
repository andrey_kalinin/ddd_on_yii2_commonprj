<?php

namespace commonprj\components\core\models\search;

use Yii;

/**
 * This is the model class for table "template".
 *
 * @property int $id
 * @property int $element_class_id
 * @property int $property_variant_id
 * @property string $property_ids
 *
 * @property ElementClassRecord $elementClass
 * @property PropertyVariantRecord $propertyVariant
 */
class TemplateRecord extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'template';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbSearch');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['element_class_id', 'property_variant_id'], 'default', 'value' => null],
            [['element_class_id', 'property_variant_id'], 'integer'],
            [['property_ids'], 'string'],
            [['element_class_id'], 'exist', 'skipOnError' => true, 'targetClass' => ElementClassRecord::className(), 'targetAttribute' => ['element_class_id' => 'id']],
            [['property_variant_id'], 'exist', 'skipOnError' => true, 'targetClass' => PropertyVariantRecord::className(), 'targetAttribute' => ['property_variant_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'element_class_id' => 'Element Class ID',
            'property_variant_id' => 'Property Variant ID',
            'property_ids' => 'Property Ids',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getElementClass()
    {
        return $this->hasOne(ElementClassRecord::className(), ['id' => 'element_class_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertyVariant()
    {
        return $this->hasOne(PropertyVariantRecord::className(), ['id' => 'property_variant_id']);
    }

    
}
