<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 23.01.2018
 * Time: 10:46
 */

namespace commonprj\services\search\exception;

use yii\web\BadRequestHttpException;

class SearchServiceBadRequestException extends BadRequestHttpException
{

}