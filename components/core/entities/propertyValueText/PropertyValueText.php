<?php
/**
 * Created by daSilva.Rodrigues
 * Date: 22.08.2016
 */

namespace commonprj\components\core\entities\propertyValueText;

use Yii;
use commonprj\components\core\entities\abstractPropertyValue\AbstractPropertyValue;
use yii\db\ActiveRecord;

/**
 * Class TextProperty
 * @package commonprj\components\core\entities\textProperty
 */
class PropertyValueText extends AbstractPropertyValue
{
    public $searchdbPropertyValueId = null;

    public function __construct(array $config = [], ActiveRecord $activeRecord = null)
    {
        parent::__construct($config);
        $this->repository = Yii::$app->propertyValueTextRepository;
    }

    /**
     * Sets the attribute values in a massive way.
     * @param array $values attribute values (name => value) to be assigned to the model.
     * @param bool $safeOnly whether the assignments should only be done to the safe attributes.
     * A safe attribute is one that is associated with a validation rule in the current [[scenario]].
     * @see safeAttributes()
     * @see attributes()
     */
    public function setAttributes($values, $safeOnly = true)
    {
        $localization = Yii::$app->localization;
        $value = $localization->translate('search', 'property_value_text', 'value', $values['searchdbPropertyValueId'], $values['propertyId']);
        $values['value'] = $value ?? $values['value'];

        parent::setAttributes($values, $safeOnly);
    }
}