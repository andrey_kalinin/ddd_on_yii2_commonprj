<?php
/**
 * @package         FurniPrice
 * @subpackage      Core
 * @category        DBRepository
 * @created         2017.12.20
 * @author          Vasiliy Konakov
 * @updated         2018.01.19
 * @updatedBy       Vasiliy Konakov
 */

namespace commonprj\components\core\entities\aggregationRelationValue;

use commonprj\components\core\entities\relationValue\RelationValueDBRepository;

/**
 * Class AggregationRelationValueDBRepository
 * @package commonprj\components\core\entities\aggregationRelationValue
 */
class AggregationRelationValueDBRepository extends RelationValueDBRepository
{

}
