<?php

namespace commonprj\components\core\entities\propertyVariant;

use commonprj\components\core\entities\abstractPropertyValue\AbstractPropertyValue;
use commonprj\components\core\entities\elementCategory\ElementCategory;
use commonprj\components\core\entities\property\Property;
use commonprj\components\core\entities\property\PropertyDBRepository;
use commonprj\components\core\entities\propertyValidationRule\PropertyValidationRule;
use commonprj\components\core\entities\template\Template;
use commonprj\components\core\models\search\PropertyRecord;
use commonprj\components\core\helpers\PostgreSqlArrayHelper;
use commonprj\components\core\models\search\PropertyArrayRecord;
use commonprj\components\core\models\search\PropertyVariantRecord;
use commonprj\extendedStdComponents\BaseCrudModel;
use commonprj\extendedStdComponents\BaseDBRepository;
use commonprj\extendedStdComponents\IBaseCrudModel;
use yii\db\ActiveRecord;
use yii\helpers\BaseInflector;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;

/**
 * Class PropertyVariantDBRepository
 * @package commonprj\components\core\entities\propertyVariant
 */
class PropertyVariantDBRepository extends BaseDBRepository implements IPropertyVariantRepository
{
    /**
     * @var string
     */
    public $activeRecordClass = PropertyVariantRecord::class;
    /**
     * @var null
     */
    public $propertyValidationRule = null;

    /**
     * @param PropertyVariant $propertyVariant
     * @return array
     */
    public function getElementCategories(PropertyVariant $propertyVariant, int $elementClassId): array
    {
        return $this->getTree($propertyVariant->id, $elementClassId);
    }

    /**
     * @param PropertyVariant $propertyVariant
     * @return Property|null
     */
    public function getProperty(PropertyVariant $propertyVariant): ?Property
    {
        return Property::findOne($propertyVariant->propertyId);
    }

    /**
     * @param PropertyVariant $propertyVariant
     * @return PropertyValidationRule
     */
    public function getPropertyValidationRule(PropertyVariant $propertyVariant): ?PropertyValidationRule
    {
        $this->propertyValidationRule = PropertyValidationRule::findOne(['propertyVariantId' => $propertyVariant->id]);

        return $this->propertyValidationRule;
    }

    /**
     * @param PropertyVariant $propertyVariant
     * @return AbstractPropertyValue
     */
    public function getPropertyValue(PropertyVariant $propertyVariant): AbstractPropertyValue
    {
        $valueRecord = PropertyDBRepository::getPropertyValueClassByTypeId(PropertyDBRepository::getPropertyTypeIdByPropertyId($propertyVariant->propertyId));

        return $valueRecord::findOne($propertyVariant->propertyValueId);
    }

    /**
     * @param PropertyVariant $propertyVariant
     * @param int $elementClassId
     * @return Template|null
     */
    public function getTemplate(PropertyVariant $propertyVariant, int $elementClassId): ?Template
    {
        $template = Template::findOne(['propertyVariantId' => $propertyVariant->id, 'elementClassId' => $elementClassId]);

        return $template ?? null;
    }

    /**
     * @param $propertyVariantId
     * @param $elementClassId
     * @return array
     */
    public function getTree($propertyVariantId, $elementClassId)
    {

        $listElementCategoryQuery = ElementCategory::find()
            ->indexBy('id');

        if ($propertyVariantId) {
            $listElementCategoryQuery->andWhere(['propertyVariantId' => $propertyVariantId]);
        }
        if ($elementClassId) {
            $listElementCategoryQuery->andWhere(['elementClassId' => $elementClassId]);
        }

        $listItems = $listElementCategoryQuery->all();

        if (!$listItems) {
            return [];
        }

        foreach ($listItems as $listItem) {
            $table[] = [
                'element' => $listItem['id'],
                'parent'  => $listItem['parent_id'] ?? 0,
            ];
        }

        $result = $this->buildTreeFromArray($table, $listItems);

        return $result;
    }

    /**
     * @param BaseCrudModel $entity
     * @param array|int $entityRecord
     * @throws NotFoundHttpException
     */
    public function populateRecord(BaseCrudModel $entity, $entityRecord)
    {
        if (!$entityRecord instanceof ActiveRecord) {
            $entityRecord = PropertyVariantRecord::findOne($entityRecord);
        }
        if (!$entityRecord) {
            throw new NotFoundHttpException("Entity id not found.");
        }

        foreach ($entityRecord->getAttributes() as $field => $value) {
            $field = lcfirst(BaseInflector::camelize($field));

            $entity->setOldAttribute($field, $value);
            $entity->{$field} = $value;
        }
    }

    /**
     * @param IBaseCrudModel $entity
     * @param bool $runValidation
     * @param null $attributes
     * @return bool
     */
    public function save(IBaseCrudModel $entity, $runValidation = true, $attributes = null): bool
    {
        //пробуем найти старую запись
        $oldPropertyMultiplicityId = $entity->getOldAttributes('propertyMultiplicityId');

        if ($oldPropertyMultiplicityId == AbstractPropertyValue::MULTIPLICITY_ARRAY) {
            $oldPropertyArrayRecordId = $entity->getOldAttributes('propertyValueId');
            $propertyArrayRecord = PropertyArrayRecord::findOne(['id' => $oldPropertyArrayRecordId]);
        }

        $propertyMultiplicityId = $entity->propertyMultiplicityId;
        //Если новая запись массив
        if ($propertyMultiplicityId == AbstractPropertyValue::MULTIPLICITY_ARRAY) {
            $oldPropertyArrayRecord = $propertyArrayRecord ?? null;

            //Если не было старой записи в массиве - создаем новый объект
            if (!$oldPropertyArrayRecord) {
                $propertyArrayRecord = new PropertyArrayRecord();
                $propertyArrayRecord->name = 'variant_' . uniqid();
            } else {
                $propertyArrayRecord = $oldPropertyArrayRecord;
            }

            $propertyArrayRecord->property_id = $entity->propertyId;
            $propertyArrayRecord->value_ids = PostgreSqlArrayHelper::array2pg($entity->propertyValueId);

            $result = $propertyArrayRecord->save();

            if ($result !== true) {
                return false;
            }

            $entity->propertyValueId = $propertyArrayRecord->id;
        } else {
            $oldPropertyArrayRecord = $propertyArrayRecord ?? null;
            //Если новая запись не массив - удаляем старый массив
            if ($oldPropertyArrayRecord) {
                $oldPropertyArrayRecord->delete();
            }
        }

        return parent::save($entity, $runValidation, $attributes); // TODO: Change the autogenerated stub
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return array_merge(parent::attributes(), ['propertySysname']); // TODO: Change the autogenerated stub
    }
    
    /**
     * @param PropertyVariant $entity
     * @param int $elementClass
     * @return Template|null
     * @throws HttpException
     */
    public function getTemplateInHierarchy(PropertyVariant $entity, int $elementClass): ?Template
    {
        $property = PropertyRecord::findOne($entity->propertyId);

        if ($property->property_type_id !== Property::PROPERTY_TYPE_TREE) {
            throw new HttpException(400, "Property id:{$entity->propertyId} должно юыть типа `tree`");
        }

    }
}
