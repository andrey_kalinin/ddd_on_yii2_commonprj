<?php
/**
 * @package         FurniPrice
 * @subpackage      Core
 * @category        DBRepository
 * @created         17.06.2016
 * @author          daSilva.Rodrigues
 * @refactoredBy    Vasiliy Konakov
 * @updated         13.12.2017
 */

namespace commonprj\components\core\entities\propertyValueBigint;

use commonprj\components\core\entities\abstractPropertyValue\AbstractPropertyValue;
use yii\db\ActiveRecord;

/**
 * Class propertyValueBigint
 * @package commonprj\components\core\entities\propertyValueBigint
 */
class PropertyValueBigint extends AbstractPropertyValue
{

    public $elementId;
    public $propertyId;
    public $value;
    public $isParent = false;


    public function __construct(array $config = [], ActiveRecord $activeRecord = null)
    {
        parent::__construct($config);
        $this->repository = \Yii::$app->propertyValueBigintRepository;
    }

}
