<?php

namespace commonprj\components\core\entities\elementType;

use commonprj\components\core\entities\elementCategory\ElementCategory;
use commonprj\extendedStdComponents\BaseCrudModel;
use commonprj\extendedStdComponents\BaseServiceRepository;

/**
 * Class ElementTypeServiceRepository
 * @package commonprj\components\core\entities\elementType
 */
class ElementTypeServiceRepository extends BaseServiceRepository implements IElementTypeRepository
{
    /**
     * @param array|null $condition
     * @return ElementType[]
     */
    public function find(array $condition = null)
    {
        $this->requestUri = 'common/element-type';
        $arModel = $this->getApiData();

        return $this->getArrayOfModels($arModel);
    }

    /**
     * @param int $condition
     * @param bool $byClass
     * @return BaseCrudModel
     * @throws \yii\web\HttpException
     */
    public function findOne($condition, $byClass = false)
    {
        $this->requestUri = 'common/element-type/' . $condition;
        $arModel = $this->getApiData();

        return $this->getOneModel($arModel);
    }

    /**
     * @param int $elementTypeId
     * @return ElementCategory[]
     */
    public function getElementCategoriesById(int $elementTypeId)
    {
        $this->requestUri = 'common/element-type/' . $elementTypeId . '/element-category';
        $arModel = $this->getApiData();

        return $this->getArrayOfModels($arModel);
    }

    /**
     * @param int $elementTypeId
     * @return BaseCrudModel
     */
    public function getElementClassById(int $elementTypeId)
    {
        $this->requestUri = 'common/element-type/' . $elementTypeId . '/element-class';
        $arModel = $this->getApiData();

        return $this->getOneModel($arModel);
    }

    /**
     * @param $elementTypeId
     * @return array|BaseCrudModel
     */
    public function getVariantById($elementTypeId)
    {
        $this->requestUri = 'common/element-type/' . $elementTypeId . '/variant';
        $arModel = $this->getApiData();

        return $this->getOneModel($arModel);
    }
}