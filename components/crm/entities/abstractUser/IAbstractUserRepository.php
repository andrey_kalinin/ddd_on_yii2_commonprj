<?php
/**
 * @package         FurniPrice
 * @subpackage      CRM
 * @category        Entity
 * @created         2018.01.22
 * @author          Vasiliy Konakov
 * @updated         2018.01.23
 * @updatedBy       Vasiliy Konakov
 */

namespace commonprj\components\crm\entities\abstractUser;

use yii\db\Exception;
use yii\web\HttpException;
use commonprj\components\crm\entities\accessSpecification\AccessSpecification;
use commonprj\components\crm\entities\address\Address;
use commonprj\components\crm\entities\userRole\UserRole;

/**
 * Class IAbstractUserRepository
 * @package commonprj\components\crm\entities\abstractUser
 */
interface IAbstractUserRepository
{

    /**
     * bindAddress
     * @param AbstractUser $element
     * @param int $addressId
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException|Exception
     */
    public function bindAddress(AbstractUser $element, int $addressId): bool;


    /**
     * unbindAddress
     * @param AbstractUser $element
     * @param int $addressId
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException|Exception
     */
    public function unbindAddress(AbstractUser $element, int $addressId): bool;

    /**
     * getAddresses
     * @param AbstractUser $element
     * @param int $addressTypeId
     * @return Address[]
     * @throws HttpException|Exception
     */
    public function getAddresses(AbstractUser $element, $addressTypeId): array;

    /**
     * getAccessSpecification
     */
    public function getAccessSpecification(): ?AccessSpecification;

    /**
     * bindUserRole
     * @param AbstractUser $element
     * @param int $userRole
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException|Exception
     */
    public function bindUserRole(AbstractUser $element, int $userRole): bool;

    /**
     * unbindUserRole
     * @param AbstractUser $element
     * @param int $userRole
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException|Exception
     */
    public function unbindUserRole(AbstractUser $element, int $userRole): bool;

    /**
     * getUserRole
     * @param AbstractUser $element
     * @return null|UserRole (domain layer object)
     * @throws HttpException|Exception
     */
    public function getUserRole(AbstractUser $element): ?UserRole;

}
