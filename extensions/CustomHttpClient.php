<?php

namespace commonprj\extensions;

use Yii;
use yii\httpclient\Client;
use yii\httpclient\Exception;

class CustomHttpClient extends Client
{
    /**
     * Максимальное количество попыток
     */
    const MAX_ATTEMPTS = 3;

    public function post($url, $data = null, $headers = [], $options = [])
    {
        return parent::post($url, $data ?? [], $headers, $options);
    }

    /**
     * @param \yii\httpclient\Request $request
     * @return \yii\httpclient\Response
     * @throws Exception
     */
    public function send($request)
    {

        for ($i = 1; $i < self::MAX_ATTEMPTS; $i++) {
            try {
                $response = parent::send($request);
                $exception = null;
                break;
            } catch (\Exception $e) {
                $exception = $e;
            }
        }

        if ($exception) {
            throw new Exception($e->getMessage(), $e->getCode(), $e);
        }

        return $response;
    }

    public function beforeSend($request)
    {
        parent::beforeSend($request);

        $sessionService = Yii::$app->storeService;

        $request->addHeaders([
            'Accept-Language' => $sessionService->getCurrentLanguageCode('eng-US'),
        ]);

    }


}