<?php
/**
 * @package         FurniPrice
 * @subpackage      Core
 * @category        DBRepository
 * @created         2017.12.20
 * @author          Vasiliy Konakov
 * @updated         2018.01.19
 * @updatedBy       Vasiliy Konakov
 */

namespace commonprj\components\core\entities\relationValue;

use commonprj\components\core\entities\propertyValueBigint\PropertyValueBigint;
use Yii;
use yii\db\Exception;

/**
 * Class RelationValue
 * @package commonprj\components\core\entities\relationValue
 */
class RelationValue extends PropertyValueBigint
{

    /** @var RelationValueDBRepository $repository */
    public $repository;

    /** @var bool $isParent */
    public $isParent;

    public $isRelation = true;

    /**
     * Присвоение свойству доменного слоя $repository - соответствующего компонента
     *
     * @inheritdoc
     */
    public function __construct($config = [])
    {
        parent::__construct($config);
        $this->repository = Yii::$app->relationValueRepository;
    }

    /**
     * Active record style validation rules for not-active record "Model"
     * @return array
     */
    public function rules(): array
    {
        return array_merge(
            [
                [['parent_element_id', 'relation_id', 'child_element_id'], 'required'],
                [['parent_element_id', 'relation_id', 'child_element_id'], 'integer'],
                [['parent_element_id', 'relation_id', 'child_element_id'], 'exist'],
                [['is_parent'], 'boolean'],
            ]
        );
    }

    /**
     * Save/update instance into storage
     * @return bool
     * @throws Exception
     */
    public function save():bool
    {
        return $this->repository->save($this);
    }

    /**
     * Delete instance in storage
     * @return bool
     * @throws Exception
     */
    public function delete():bool
    {
        return $this->repository->delete($this);
    }

}

