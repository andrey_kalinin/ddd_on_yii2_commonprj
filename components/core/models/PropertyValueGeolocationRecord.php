<?php

namespace commonprj\components\core\models;

use Yii;

/**
 * This is the model class for table "property_value_geolocation".
 *
 * @property integer $id
 * @property integer $property_id
 * @property integer $element_id
 * @property string $value
 *
 * @property PropertyRecord $property
 */
class PropertyValueGeolocationRecord extends AbstractPropertyValueRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'property_value_geolocation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['property_id', 'element_id', 'value'], 'required'],
            [['property_id', 'element_id'], 'integer'],
            [['value'], 'string'],
            [['property_id'], 'exist', 'skipOnError' => true, 'targetClass' => PropertyRecord::className(), 'targetAttribute' => ['property_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'property_id' => 'Property ID',
            'element_id' => 'Element ID',
            'value' => 'Value',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty()
    {
        return $this->hasOne(PropertyRecord::className(), ['id' => 'property_id']);
    }
}
