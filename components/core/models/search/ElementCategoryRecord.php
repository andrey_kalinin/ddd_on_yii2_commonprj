<?php

namespace commonprj\components\core\models\search;

use Yii;

/**
 * This is the model class for table "element_category".
 *
 * @property int $id
 * @property string $name
 * @property string $sysname
 * @property string $description
 * @property int $parent_id
 * @property bool $is_active
 * @property int $property_variant_id
 * @property int $sort_order
 * @property int $level
 * @property string $image_id
 * @property int $menu_id
 *
 * @property ElementCategoryRecord $parent
 * @property ElementCategoryRecord[] $elementCategoryRecords
 * @property Menu $menu
 * @property PropertyVariantRecord $propertyVariant
 */
class ElementCategoryRecord extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'element_category';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbSearch');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['parent_id', 'property_variant_id', 'sort_order', 'level', 'menu_id'], 'default', 'value' => null],
            [['parent_id', 'property_variant_id', 'sort_order', 'level', 'menu_id'], 'integer'],
            [['is_active'], 'boolean'],
            [['property_variant_id', 'name'], 'required'],
            [['sysname'], 'string', 'max' => 50],
            [['image_id'], 'string', 'max' => 32],
            [['name'], 'string', 'max' => 255],
            [['sysname'], 'unique'],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => ElementCategoryRecord::className(), 'targetAttribute' => ['parent_id' => 'id']],
            [['menu_id'], 'exist', 'skipOnError' => true, 'targetClass' => MenuRecord::className(), 'targetAttribute' => ['menu_id' => 'id']],
            [['property_variant_id'], 'exist', 'skipOnError' => true, 'targetClass' => PropertyVariantRecord::className(), 'targetAttribute' => ['property_variant_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'sysname' => 'Sysname',
            'description' => 'Description',
            'parent_id' => 'Parent ID',
            'is_active' => 'Is Active',
            'property_variant_id' => 'Property Variant ID',
            'sort_order' => 'Sort Order',
            'level' => 'Level',
            'image_id' => 'Image ID',
            'menu_id' => 'Menu ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(ElementCategoryRecord::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getElementCategoryRecords()
    {
        return $this->hasMany(ElementCategoryRecord::className(), ['parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenu()
    {
        return $this->hasOne(MenuRecord::className(), ['id' => 'menu_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertyVariant()
    {
        return $this->hasOne(PropertyVariantRecord::className(), ['id' => 'property_variant_id']);
    }
}
