<?php
/**
 * @package         FurniPrice
 * @subpackage      CRM
 * @category        Entity
 * @created         2018.01.22
 * @author          Vasiliy Konakov
 * @updated         2018.01.23
 * @updatedBy       Vasiliy Konakov
 */

namespace commonprj\components\crm\entities\employee;

use commonprj\components\crm\entities\company\Company;
use Yii;
use yii\db\Exception;
use yii\web\HttpException;
use commonprj\components\core\models\ElementRecord;
use commonprj\components\core\entities\property\Property;
use commonprj\components\crm\entities\user\UserDBRepository;
use commonprj\components\crm\entities\productOffer\ProductOffer;

class EmployeeDBRepository extends UserDBRepository implements IEmployeeRepository
{

    /**
     * bindCompany
     * @param Employee $element
     * @param int $companyId
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException|Exception
     */
    public function bindCompany(Employee $element, $companyId): bool
    {
        return $this->bindToParent($element, Yii::$app->propertyService->getPropertyIdByElementAndSysname($element,
            Property::PROPERTY_SYSNAME__EMPLOYEE2COMPANY), $companyId);
    }

    /**
     * unbindCompany
     * @param Employee $element
     * @param int $companyId
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException|Exception
     */
    public function unbindCompany(Employee $element, $companyId): bool
    {
        return $this->unbindChild($element, Yii::$app->propertyService->getPropertyIdByElementAndSysname($element,
            Property::PROPERTY_SYSNAME__EMPLOYEE2COMPANY), $companyId);
    }

    /**
     * @param Employee $element
     * @return Company|null
     * @throws HttpException
     */
    public function getCompany(Employee $element): ?Company
    {
        $item = $this->getParent($element, Yii::$app->propertyService->getPropertyIdByElementAndSysname($element,
            Property::PROPERTY_SYSNAME__EMPLOYEE2COMPANY));
        if ($item) {
            /** @var ompany $result */
            $result = self::instantiateByARAndClassName(
                ElementRecord::findOne(['id' => $item->id]), AbstractCompany::class);
            return $result;
        }
        return null;
    }

    /**
     * getProductOffers
     * @param Employee $element
     * @return ProductOffer[]
     * @throws HttpException|Exception
     */
    public function getProductOffers(Employee $element): array
    {
        $result = [];
        foreach ($this->getAssociatedEntities($element, Yii::$app->propertyService->getPropertyIdByElementAndSysname($element,
            Property::PROPERTY_SYSNAME__PRODUCT_OFFER2EMPLOYEE)) as $item) {
            $result[$item->id] = self::instantiateByARAndClassName(
                ElementRecord::findOne(['id' => $item->id]), ProductOffer::class);
        }
        return $result;
    }

}