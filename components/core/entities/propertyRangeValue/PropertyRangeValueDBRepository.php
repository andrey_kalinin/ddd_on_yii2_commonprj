<?php

namespace commonprj\components\core\entities\propertyRangeValue;

use commonprj\components\core\entities\abstractPropertyValue\AbstractPropertyValue;
use commonprj\components\core\entities\abstractPropertyValue\AbstractPropertyValueDBRepository;
use commonprj\components\core\models\PropertyRangeRecord;
use commonprj\services\SearchDBSynchService;
use Yii;
use yii\db\ActiveRecord;
use yii\web\HttpException;

/**
 * Class PropertyRangeDBRepository
 * @package commonprj\components\core\entities\propertyRangeValue
 */
class PropertyRangeValueDBRepository extends AbstractPropertyValueDBRepository
{
    public $activeRecordClass = PropertyRangeRecord::class;

    /**
     * @param AbstractPropertyValue $propertyValue
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\HttpException
     */
    public function delete(AbstractPropertyValue $propertyValue)
    {
        /**
         * @var ActiveRecord|null $record
         */
        $record = $propertyValue->repository->activeRecordClass::findOne($propertyValue->id);

        if (!$record) {
            throw new HttpException(404, basename(__FILE__, '.php') . __LINE__);
        }

        return $record->delete();
    }

    /**
     * @param AbstractPropertyValue $rangePropertyValue
     * @return bool|mixed|ActiveRecord
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function save(AbstractPropertyValue $rangePropertyValue)
    {
        if (empty($rangePropertyValue->fromValue) && empty($rangePropertyValue->toValue)) {
            throw new \LogicException('Cant`t save rangePropertyValue where `fromValue` or `toValue` are missing');
        }

        if ($rangePropertyValue->isNew()) {
            if ($this->hasElementPropertyValue($rangePropertyValue)) {
                throw new \Exception('Current Element already has current property value');
            }
        }

        $transaction = Yii::$app->getDb()->beginTransaction();

        $concretePropertyClass = self::getConcretePropertyValueClassByPropertyId($rangePropertyValue->propertyId);

        $values = [
            'fromValueId' => $rangePropertyValue->fromValue,
            'toValueId' => $rangePropertyValue->toValue,
        ];

        foreach ($values as $key => $value) {
            if ($value) {
                /**
                 * @var AbstractPropertyValue $propertyValue
                 */
                $propertyValue = new $concretePropertyClass([
                    'propertyId' => $rangePropertyValue->propertyId,
                    'value' => $value,
                    'elementId' => $rangePropertyValue->elementId,
                    'multiplicityId' => AbstractPropertyValue::MULTIPLICITY_RANGE,
                ]);

                $result = $propertyValue->save();

                if (!$result) {
                    return $propertyValue;
                } else {
                    $rangePropertyValue->{$key} = $propertyValue->id;
                }

            }
        }

        // save range value
        /**
         * @var ActiveRecord $propertyRangeRecord
         */
        $propertyRangeRecord = new $this->activeRecordClass();
        $propertyRangeRecord->setAttributes(self::arrayKeysCamelCase2Underscore($rangePropertyValue->getAttributes()));
        $result = $propertyRangeRecord->save();

        if (!$result) {
            return $propertyRangeRecord;
        }

        $searchDbSynch = new SearchDBSynchService($propertyRangeRecord, $rangePropertyValue->multiplicityId);
        $searchDbSynch->saveMultiplePropertyValue((new $concretePropertyClass)->repository->activeRecordClass, $values);

        $transaction->commit();
        return true;
    }
}