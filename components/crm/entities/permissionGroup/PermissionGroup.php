<?php

namespace commonprj\components\crm\entities\permissionGroup;

use commonprj\components\core\entities\elementClass\ElementClass;
use commonprj\components\crm\entities\permission\Permission;
use Yii;
use commonprj\components\core\entities\element\Element;

/**
 * Class PermissionGroup
 * @property IPermissionGroupRepository $repository
 * @package commonprj\components\crm\entities\permissionGroup
 */
class PermissionGroup extends Element
{
    /**
     * Присвоение свойству доменного слоя $repository - соответствующего компонента
     * @inheritdoc
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->repository = Yii::$app->permissionGroupRepository;
    }

    /**
     * @param $elementClassId
     * @return bool
     */
    public function bindElementClass($elementClassId): bool
    {
        return $this->repository->bindElementClass($this, $elementClassId);
    }

    /**
     * @param $elementClassId
     * @return bool
     */
    public function unbindElementClass($elementClassId): bool
    {
        return $this->repository->unbindElementClass($this, $elementClassId);
    }

    /**
     * @return ElementClass
     */
    public function getElementClass(): ElementClass
    {
        return $this->repository->getElementClass($this);
    }

    /**
     * @return Permission[]
     */
    public function getPermissions(): array
    {
        return $this->repository->getPermissions($this);
    }
}
