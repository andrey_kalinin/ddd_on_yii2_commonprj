<?php

namespace commonprj\services\search\filters;

use yii\db\Query;

class PropertyTextFilter extends AbstractFilter
{

    protected $table = 'property_value_text';
    protected $field = 'value';

    protected $operator;
    protected $propertyId;
    protected $values;
    protected $classId;

    public function renderQuery($operation, $values, $classId, $extendData = null)
    {

        if (strtolower($operation) == 'like') {
            $operation = 'ilike';
        }

        $table = $this->table;
        $field = $this->field;
        // был поиск в переводах и вернулся список id
        if ($extendData !== null) {
            $field = 'id';
            $values = reset($extendData);
            if ($operation == 'ilike') {
                $operation = 'IN';
            }
        }

        $condition = [
            $operation,
            "{$table}.{$field}",
            $values,
        ];

        $propertyId = $this->propertyId;

        $query = (new Query())
            ->select('e.element_id')
            ->from("{$table}")
            ->leftJoin('property_value2element e', "e.property_value_id = {$table}.id AND e.property_id = {$table}.property_id")
            ->where($condition)
            ->andWhere(["{$table}.property_id" => $propertyId]);

        return $query;
    }
}