<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 12.02.2018
 * Time: 11:03
 */

namespace commonprj\components\core\entities\propertyUnit;

use commonprj\components\core\entities\property\Property;
use commonprj\components\core\models\PropertyUnitRecord;
use commonprj\extendedStdComponents\BaseDBRepository;
use commonprj\extendedStdComponents\IBaseCrudModel;
use yii\web\HttpException;

/**
 * Class PropertyUnitDBRepository
 * @package commonprj\components\core\entities\propertyUnit
 */
class PropertyUnitDBRepository extends BaseDBRepository implements IPropertyUnitRepository
{
    /**
     * @var string
     */
    public $activeRecordClass = PropertyUnitRecord::class;

    /**
     * @param PropertyUnit $propertyUnit
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function save(IBaseCrudModel $propertyUnit, $runValidation = true, $attributes = null): bool
    {
        if (!$propertyUnitRecord = PropertyUnitRecord::findOne($propertyUnit->id)) {
            $propertyUnitRecord = new PropertyUnitRecord();
        }
        $propertyUnitRecord->setAttributes(self::arrayKeysCamelCase2Underscore($propertyUnit->attributes));

        $result = $propertyUnitRecord->save();

        if ($result) {
            $propertyUnit->setAttributes(self::arrayKeysUnderscore2CamelCase($propertyUnitRecord->attributes), false);
        }

        return $result;
    }

    /**
     * @param PropertyUnit $propertyUnit
     * @return bool
     * @throws HttpException
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function delete(IBaseCrudModel $propertyUnit): bool
    {
        $propertyUnitRecord = PropertyUnitRecord::findOne($propertyUnit->id);

        if (!$propertyUnitRecord) {
            throw new HttpException(404, basename(__FILE__, '.php') . __LINE__);
        }

        if ($propertyUnitRecord->getProperties()->one()) {
            throw new \LogicException('Не могу удалить PropertyUnit который используется');
        }

        return $propertyUnitRecord->delete();
    }

    /**
     * @return string[]
     */
    public static function primaryKey()
    {
        return PropertyUnitRecord::primaryKey();
    }

    /**
     * @param PropertyUnit $propertyUnit
     * @return Property[]
     * @throws HttpException
     */
    public function getProperties(PropertyUnit $propertyUnit): array
    {
        /**
         * @var PropertyUnitRecord $propertyUnitRecord
         */
        $propertyUnitRecord = PropertyUnitRecord::findOne($propertyUnit->id);

        if (!$propertyUnitRecord) {
            throw new HttpException(404, basename(__FILE__, '.php') . __LINE__);
        }

        $propertyRecords = $propertyUnitRecord->getProperties()->all();
        $result = [];

        foreach ((array)$propertyRecords as $propertyRecord) {
            $result[] = Property::populate($propertyRecord);
        }

        return $result;
    }
}