<?php

namespace commonprj\components\core\models\search;

use Yii;

/**
 * This is the model class for table "property_list_item".
 *
 * @property integer $id
 * @property integer $property_id
 * @property string $item
 * @property string $label
 *
 * @property PropertyRecord $property
 */
class PropertyListItemRecord extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'property_list_item';
    }

    /**
     * @return null|object|\yii\db\Connection
     */
    public static function getDb()
    {
        return Yii::$app->get('dbSearch');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['property_id', 'item', 'label'], 'required'],
            [['property_id', 'sort_order'], 'default', 'value' => null],
            [['property_id', 'sort_order'], 'integer'],
            [['item', 'label'], 'string', 'max' => 255],
            [['property_id', 'label'], 'unique', 'targetAttribute' => ['property_id', 'label']],
            [['property_id', 'item'], 'unique', 'targetAttribute' => ['property_id', 'item']],
            [['property_id'], 'exist', 'skipOnError' => true, 'targetClass' => PropertyRecord::className(), 'targetAttribute' => ['property_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'property_id' => 'Property ID',
            'item'        => 'Item',
            'label'       => 'Label',
            'sort_order' => 'Sort Order',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty()
    {
        return $this->hasOne(PropertyRecord::className(), ['id' => 'property_id']);
    }
}
