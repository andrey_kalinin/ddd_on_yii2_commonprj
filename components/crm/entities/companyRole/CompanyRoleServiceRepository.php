<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 1/19/2018
 * Time: 3:06 PM
 */

namespace commonprj\components\crm\entities\companyRole;


use commonprj\components\crm\entities\permissionGroup\PermissionGroup;

class CompanyRoleServiceRepository implements ICompanyRoleRepository
{
    /**
     * @param CompanyRole $companyRole
     * @return array
     */
    public function getUserRoles(CompanyRole $companyRole): array
    {
        // TODO: Implement getUserRoles() method.
    }

    /**
     * @param CompanyRole $companyRole
     * @param int $permissionGroupId
     * @return bool
     */
    public function bindPermissionGroup(CompanyRole $companyRole, int $permissionGroupId): bool
    {
        // TODO: Implement bindPermissionGroup() method.
    }

    /**
     * @param CompanyRole $companyRole
     * @param int $permissionGroupId
     * @return bool
     */
    public function unbindPermissionGroup(CompanyRole $companyRole, int $permissionGroupId): bool
    {
        // TODO: Implement unbindPermissionGroup() method.
    }


    /**
     * @param CompanyRole $companyRole
     * @return PermissionGroup[]
     */
    public function getPermissionGroups(CompanyRole $companyRole): array
    {
        // TODO: Implement getPermissionGroups() method.
    }

}