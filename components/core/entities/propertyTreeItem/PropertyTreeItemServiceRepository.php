<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 03.04.2018
 * Time: 19:07
 */

namespace commonprj\components\core\entities\PropertyTreeItem;

use commonprj\extendedStdComponents\BaseServiceRepository;
use Yii;

class PropertyTreeItemServiceRepository extends BaseServiceRepository
{
    /**
     * @var string
     */
    protected $baseUri = 'core/property-tree-item/';

    /**
     * @var array
     */
    protected $forbiddenMethods = ['delete'];

    /**
     * PropertyServiceRepository constructor.
     */
    public function __construct()
    {
        $this->restServer = Yii::$app->catalogManager;
    }
}