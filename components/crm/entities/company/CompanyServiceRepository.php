<?php

namespace commonprj\components\crm\entities\Company;

use commonprj\components\core\entities\element\ElementServiceRepository;
use commonprj\components\crm\entities\address\Address;
use commonprj\components\crm\entities\companyRole\CompanyRole;
use commonprj\components\crm\entities\companyState\CompanyState;
use commonprj\components\crm\entities\employee\Employee;
use Yii;
use yii\httpclient\Client;

/**
 * Class CompanyServiceRepository
 * @package commonprj\components\crm\entities\Company
 */
class CompanyServiceRepository extends ElementServiceRepository
{
    /**
     * @var Client
     */
    protected $restServer;

    /**
     * @var string
     */
    protected $baseUri = 'company/';

    /**
     * CompanyServiceRepository constructor.
     */
    public function __construct()
    {
        $this->restServer = Yii::$app->catalogManager;
    }

    /**
     * @param Company $company
     * @param int $addressId
     * @return bool
     */
    public function bindAddress(Company $company, int $addressId)
    {
        $uri = $this->getBaseUri($company->id . '/address/' . $addressId);
        $response = $this->restServer->post($uri)->send();
        return $response->getIsOk();
    }

    /**
     * @param Company $company
     * @param int $addressId
     * @return bool
     */
    public function unbindAddress(Company $company, int $addressId)
    {
        $uri = $this->getBaseUri($company->id . '/address/' . $addressId);
        $response = $this->restServer->delete($uri)->send();
        return $response->getIsOk();
    }

    /**
     * @param Company $company
     * @return array|\commonprj\extendedStdComponents\BaseCrudModel[]|null
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\HttpException
     */
    public function getAddresses(Company $company)
    {
        $this->requestUri = $this->getBaseUri($company->id . '/addresses');
        $this->requestParams = [];
        $content = $this->getApiData();

        if (!empty($content)) {
            $result = $this->getArrayOfModels($content, Address::className());
        }

        return $result ?? null;
    }

    /**
     * @param Company $company
     * @param int $companyRoleId
     * @return bool
     */
    public function bindCompanyRole(Company $company, int $companyRoleId)
    {
        $uri = $this->getBaseUri($company->id . '/company-role/' . $companyRoleId);
        $response = $this->restServer->post($uri)->send();
        return $response->getIsOk();
    }

    /**
     * @param Company $company
     * @param int $companyRoleId
     * @return bool
     */
    public function unbindCompanyRole(Company $company, int $companyRoleId)
    {
        $uri = $this->getBaseUri($company->id . '/company-role/' . $companyRoleId);
        $response = $this->restServer->delete($uri)->send();
        return $response->getIsOk();
    }

    /**
     * @param Company $company
     * @return \commonprj\extendedStdComponents\BaseCrudModel|null
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\HttpException
     */
    public function getCompanyRole(Company $company)
    {
        $this->requestUri = $this->getBaseUri($company->id . '/company-role');
        $this->requestParams = [];
        $content = $this->getApiData();

        if (!empty($content)) {
            $result = $this->getOneModel($content, CompanyRole::className());
        }

        return $result ?? null;
    }

    /**
     * @param Company $company
     * @param int $companyStateId
     * @return bool
     */
    public function bindCompanyState(Company $company, int $companyStateId)
    {
        $uri = $this->getBaseUri($company->id . '/company-state/' . $companyStateId);
        $response = $this->restServer->post($uri)->send();
        return $response->getIsOk();
    }

    /**
     * @param Company $company
     * @param int $companyStateId
     * @return bool
     */
    public function unbindCompanyState(Company $company, int $companyStateId)
    {
        $uri = $this->getBaseUri($company->id . '/company-state/' . $companyStateId);
        $response = $this->restServer->delete($uri)->send();
        return $response->getIsOk();
    }

    /**
     * @param Company $company
     * @return \commonprj\extendedStdComponents\BaseCrudModel|null
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\HttpException
     */
    public function getCompanyState(Company $company)
    {
        $this->requestUri = $this->getBaseUri($company->id . '/company-state');
        $this->requestParams = [];
        $content = $this->getApiData();

        if (!empty($content)) {
            $result = $this->getOneModel($content, CompanyState::className());
        }

        return $result ?? null;
    }

    /**
     * @param Company $company
     * @return array|\commonprj\extendedStdComponents\BaseCrudModel[]|null
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\HttpException
     */
    public function getEmployees(Company $company)
    {
        $this->requestUri = $this->getBaseUri($company->id . '/employees');
        $this->requestParams = [];
        $content = $this->getApiData();

        if (!empty($content)) {
            $result = $this->getArrayOfModels($content, Employee::className());
        }

        return $result ?? null;
    }

    /**
     * @param Company $company
     * @param int $employeeId
     * @return bool
     */
    public function bindEmployee(Company $company, int $employeeId)
    {
        $uri = $this->getBaseUri($company->id . '/employee/' . $employeeId);
        $response = $this->restServer->post($uri)->send();
        return $response->getIsOk();
    }

    /**
     * @param Company $company
     * @param int $employeeId
     * @return bool
     */
    public function unbindEmployee(Company $company, int $employeeId)
    {
        $uri = $this->getBaseUri($company->id . '/employee/' . $employeeId);
        $response = $this->restServer->delete($uri)->send();
        return $response->getIsOk();
    }

}