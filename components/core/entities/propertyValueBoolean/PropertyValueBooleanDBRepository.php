<?php

namespace commonprj\components\core\entities\propertyValueBoolean;

use commonprj\components\core\entities\abstractPropertyValue\AbstractPropertyValueDBRepository;
use commonprj\components\core\models\PropertyValueBooleanRecord;

/**
 * Class propertyValueBooleanDBRepository
 * @package commonprj\components\core\entities\booleanProperty
 */
class PropertyValueBooleanDBRepository extends AbstractPropertyValueDBRepository
{
    public $activeRecordClass = PropertyValueBooleanRecord::class;
}