<?php
/**
 * @package         FurniPrice
 * @subpackage      Catalog
 * @category        Entity
 * @created         2018.01.22
 * @author          Vasiliy Konakov
 * @updated         2018.01.23
 * @updatedBy       Vasiliy Konakov
 */

namespace commonprj\components\catalog\entities\materialTemplate;

use commonprj\components\core\entities\element\Element;
use Yii;
use yii\db\Exception;
use yii\web\HttpException;
use commonprj\components\core\entities\property\Property;
use commonprj\components\core\entities\element\ElementDBRepository;
use commonprj\components\catalog\entities\material\Material;

/**
 * Class MaterialTemplateDBRepository
 * @package commonprj\components\catalog\entities\materialTemplate
 */
class MaterialTemplateDBRepository extends ElementDBRepository implements IMaterialTemplateRepository
{

    /**
     * getMaterialCollections
     * @param MaterialTemplate $element
     * @return Material[]
     * @throws HttpException|Exception
     */
    public function getMaterials(MaterialTemplate $element): array
    {
        $result = [];
        foreach ($this->getAssociatedEntities($element,
            Yii::$app->propertyService->getPropertyIdByElementAndSysname($element,
                Property::PROPERTY_SYSNAME__MATERIAL2MATERIAL_TEMPLATE)) as $item) {
            $result[$item->id] = Material::findOne($item->id);
        }
        return $result;
    }

    /**
     * @param Element $entity
     */
    public function populateElementProperties(Element $entity)
    {
        if (is_null($entity->properties)) {
            parent::populateElementProperties($entity);
        }

        foreach ($entity->properties as $key => $property) {
            if ($property->sysname == 'properties') {
                $populatedProperties = $this->populateJsonProperties($property);
            }
        }

        $entity->relatedProperties = $populatedProperties ?? [];
    }

    /**
     * @param $propertyJson
     * @return mixed
     */
    private function populateJsonProperties($propertyJson)
    {
        $properties = [];
        foreach ($propertyJson->value as $propertyId) {
            $property = Property::findOne($propertyId);
            $properties[] = $property;
        }

        return $properties;
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return array_merge(parent::attributes(), ['relatedProperties']);
    }


}