<?php

namespace commonprj\components\catalog\entities\productModel;

/**
 * @package         FurniPrice
 * @subpackage      Catalog
 * @category        Entity
 * @created         2018.01.22
 * @author          Vasiliy Konakov
 * @updated         2018.01.23
 * @updatedBy       Vasiliy Konakov
 */
use commonprj\components\catalog\entities\component\Component;
use commonprj\components\catalog\entities\material\Material;
use commonprj\components\catalog\entities\materialCollection\MaterialCollection;
use commonprj\components\catalog\entities\product\Product;
use commonprj\components\catalog\entities\variation\Variant;
use commonprj\components\core\entities\element\ElementDBRepository;
use commonprj\components\core\entities\property\Property;
use commonprj\components\core\models\ElementRecord;
use commonprj\components\crm\entities\manufacturer\Manufacturer;
use Yii;
use yii\web\HttpException;

/**
 * Class ProductModelDBRepository
 * @package commonprj\components\core\entities\productModel
 */
class ProductModelDBRepository extends ElementDBRepository implements IProductModelRepository
{

    /**
     * bindManufacturer
     * @param ProductModel $element
     * @param int $manufacturerId
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException|Exception
     */
    public function bindManufacturer(ProductModel $element, int $manufacturerId): bool
    {
        return $this->bindAssociatedEntity($element,
            Yii::$app->propertyService->getPropertyIdByElementAndSysname($element, Property::PROPERTY_SYSNAME__PRODUCT_MODEL2MANUFACTURER), $manufacturerId);
    }

    /**
     * unbindManufacturer
     * @param ProductModel $element
     * @param int $manufacturerId
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException|Exception
     */
    public function unbindManufacturer(ProductModel $element, int $manufacturerId): bool
    {
        return $this->unbindAssociatedEntity($element,
            Yii::$app->propertyService->getPropertyIdByElementAndSysname($element, Property::PROPERTY_SYSNAME__PRODUCT_MODEL2MANUFACTURER), $manufacturerId);
    }

    /**
     * getManufacturer
     * @param ProductModel $element
     * @return null|Manufacturer (domain layer object)
     * @throws HttpException|Exception
     */
    public function getManufacturer(ProductModel $element): ?Manufacturer
    {
        $item = $this->getAssociatedEntity($element,
            Yii::$app->propertyService->getPropertyIdByElementAndSysname($element, Property::PROPERTY_SYSNAME__PRODUCT_MODEL2MANUFACTURER));
        if ($item) {
            /** @var Manufacturer $result */
            $result = self::instantiateByARAndClassName(
                            ElementRecord::findOne(['id' => $item->id]), Manufacturer::class);
            return $result;
        }
        return null;
    }

    /**
     * @param ProductModel $element
     * @param array $conditions
     * @return array
     * @throws Exception
     * @throws HttpException
     * @throws Exception
     */
    public function getSellers(ProductModel $element, array $conditions = []): array
    {
        $sellers = $element->getManufacturer()->getSellers();

        if (count($conditions) == 0) {
            return $sellers;
        }

        $result = [];

        foreach ($sellers as $seller) {
            if (Yii::$app->searchService->isSatisfied($seller, $conditions)) {
                $result[] = $seller;
            }
        }

        return $sellers;
    }

    /**
     * bindProductMaterial
     * @param ProductModel $element
     * @param int $productMaterialId
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException|Exception
     */
    public function bindProductMaterial(ProductModel $element, int $productMaterialId): bool
    {
        $relationId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($element, Property::PROPERTY_SYSNAME__PRODUCT_MODEL2PRODUCT_MATERIAL);
        return $this->bindChild($element, $relationId, $productMaterialId);
    }

    /**
     * unbindProductMaterial
     * @param ProductModel $element
     * @param int $productMaterialId
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException|Exception
     */
    public function unbindProductMaterial(ProductModel $element, int $productMaterialId): bool
    {
        return $this->unbindChild($element, Yii::$app->propertyService->getPropertyIdByElementAndSysname($element, Property::PROPERTY_SYSNAME__PRODUCT_MODEL2PRODUCT_MATERIAL), $productMaterialId);
    }

    /**
     * getProductMaterials
     * @param ProductModel $element
     * @return MaterialCollection[]
     * @throws HttpException|Exception
     */
    public function getProductMaterials(ProductModel $element): array
    {
        $result = [];
        foreach ($this->getChildren($element,
            Yii::$app->propertyService->getPropertyIdByElementAndSysname($element, Property::PROPERTY_SYSNAME__PRODUCT_MODEL2PRODUCT_MATERIAL)) as $item) {
            $result[$item->id] = self::instantiateByARAndClassName(
                ElementRecord::findOne(['id' => $item->id]), ProductModel::class);
}
        return $result;
    }

    /**
     * bindMaterial
     * @param ProductModel $element
     * @param int $materialId
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException|Exception
     */
    public function bindMaterial(ProductModel $element, int $materialId): bool
    {
        return $this->bindChild($element,
            Yii::$app->propertyService->getPropertyIdByElementAndSysname($element, Property::PROPERTY_SYSNAME__MATERIAL2PRODUCT_MODEL), $materialId);
    }

    /**
     * unbindMaterial
     * @param ProductModel $element
     * @param int $materialId
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException|Exception
     */
    public function unbindMaterial(ProductModel $element, int $materialId): bool
    {
        return $this->unbindChild($element,
            Yii::$app->propertyService->getPropertyIdByElementAndSysname($element, Property::PROPERTY_SYSNAME__MATERIAL2PRODUCT_MODEL), $materialId);
    }

    /**
     * getMaterials
     * @param ProductModel $element
     * @return Material[]
     * @throws HttpException|Exception
     */
    public function getMaterials(ProductModel $element): array
    {
        $result = [];
        foreach ($this->getChildren($element,
            Yii::$app->propertyService->getPropertyIdByElementAndSysname($element, Property::PROPERTY_SYSNAME__MATERIAL2PRODUCT_MODEL)) as $item) {
            $result[$item->id] = Material::findOne($item->id);
        }
        return $result;
    }

    /**
     * bindConstructionElement
     * @param ProductModel $element
     * @param int $constructionElementId
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException|Exception
     */
    public function bindConstructionElement(ProductModel $element, int $constructionElementId): bool
    {
        return $this->bindChild($element,
            Yii::$app->propertyService->getPropertyIdByElementAndSysname($element, Property::PROPERTY_SYSNAME__COMPONENT2PRODUCT_MODEL_CONSTRUCTION_ELEMENT), $constructionElementId);
    }

    /**
     * unbindConstructionElement
     * @param ProductModel $element
     * @param int $constructionElementId
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException|Exception
     */
    public function unbindConstructionElement(ProductModel $element, int $constructionElementId): bool
    {
        return $this->unbindChild($element,
            Yii::$app->propertyService->getPropertyIdByElementAndSysname($element, Property::PROPERTY_SYSNAME__COMPONENT2PRODUCT_MODEL_CONSTRUCTION_ELEMENT), $constructionElementId);
    }

    /**
     * getConstructionElements
     * @param ProductModel $element
     * @return Component[]
     * @throws HttpException|Exception
     */
    public function getConstructionElements(ProductModel $element): array
    {
        $result = [];
        foreach ($this->getChildren($element,
            Yii::$app->propertyService->getPropertyIdByElementAndSysname($element, Property::PROPERTY_SYSNAME__COMPONENT2PRODUCT_MODEL_CONSTRUCTION_ELEMENT)) as $item) {
            $result[$item->id] = self::instantiateByARAndClassName(
                ElementRecord::findOne(['id' => $item->id]), Component::class);
        }
        return $result;
    }

    /**
     * bindModularComponent
     * @param ProductModel $element
     * @param int $modularComponentId
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException|Exception
     */
    public function bindModularComponent(ProductModel $element, int $modularComponentId): bool
    {
        return $this->bindChild($element,
            Yii::$app->propertyService->getPropertyIdByElementAndSysname($element, Property::PROPERTY_SYSNAME__COMPONENT2PRODUCT_MODEL_MODULAR_COMPONENT), $modularComponentId);
    }

    /**
     * unbindModularComponent
     * @param ProductModel $element
     * @param int $modularComponentId
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException|Exception
     */
    public function unbindModularComponent(ProductModel $element, int $modularComponentId): bool
    {
        return $this->unbindChild($element,
            Yii::$app->propertyService->getPropertyIdByElementAndSysname($element, Property::PROPERTY_SYSNAME__COMPONENT2PRODUCT_MODEL_MODULAR_COMPONENT), $modularComponentId);
    }

    /**
     * getModularComponents
     * @param ProductModel $element
     * @return Component[]
     * @throws HttpException|Exception
     */
    public function getModularComponents(ProductModel $element): array
    {
        $result = [];
        foreach ($this->getChildren($element,
            Yii::$app->propertyService->getPropertyIdByElementAndSysname($element, Property::PROPERTY_SYSNAME__COMPONENT2PRODUCT_MODEL_MODULAR_COMPONENT)) as $item) {
            $result[$item->id] = self::instantiateByARAndClassName(
                ElementRecord::findOne(['id' => $item->id]), Component::class);
        }
        return $result;
    }

    /**
     * getProducts
     * @param ProductModel $element
     * @return Product[]
     * @throws HttpException|Exception
     */
    public function getProducts(ProductModel $element): array
    {
        $result = [];
        foreach ($this->getAssociatedEntities($element,
            Yii::$app->propertyService->getPropertyIdByElementAndSysname($element, Property::PROPERTY_SYSNAME__PRODUCT2PRODUCT_MODEL)) as $item) {
            $result[$item->id] = self::instantiateByARAndClassName(
                ElementRecord::findOne(['id' => $item->id]), Product::class);
        }
        return $result;
    }

}
