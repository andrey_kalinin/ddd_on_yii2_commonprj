<?php
/**
 * Created by daSilva.Rodrigues
 * Date: 18.08.2016
 */

namespace commonprj\components\core\entities\relationClass;

use commonprj\components\core\models\ElementClassRecord;
use commonprj\components\core\models\RelationClassRecord;
use commonprj\components\core\models\RelationGroupRecord;
use commonprj\extendedStdComponents\BaseCrudModel;
use commonprj\extendedStdComponents\BaseDBRepository;
use Yii;
use yii\web\HttpException;
use yii\web\ServerErrorHttpException;

/**
 * Class RelationClassDBRepository
 * @package commonprj\components\core\entities\relationClass
 */
class RelationClassDBRepository extends BaseDBRepository implements IRelationClassRepository
{
    public $activeRecordClass = 'commonprj\components\core\models\RelationClassRecord';

    /**
     * @param $condition
     * @param $isRoot
     * @return array
     * @throws HttpException
     */
    public function getElementClassesById($condition, $isRoot)
    {
        if (!$relationClassRecord = RelationClassRecord::findOne($condition)) {
            throw new HttpException(404, basename(__FILE__, '.php') . __LINE__);
        }

        $elementClass2relationClasses = $relationClassRecord->getElementClass2relationClasses()->select(['element_class_id'])->where(['is_root' => $isRoot])->all();

        $elementClassIds = [];
        foreach ($elementClass2relationClasses as $elementClass2relationClass) {
            $elementClassIds[] = $elementClass2relationClass->getAttribute('element_class_id');
        }

        if (!$elementClassIds) {
            return [];
        }

        $elementClassRecord = ElementClassRecord::find()->where(['id' => $elementClassIds])->one();

        return static::instantiateByARAndClassName($elementClassRecord);
    }

    /**
     * @param RelationClass $relationClass
     * @return bool
     */
    public function save(RelationClass $relationClass)
    {
        if (!$relationClassRecord = RelationClassRecord::findOne($relationClass->id)) {
            $relationClassRecord = new RelationClassRecord();
        }

        $relationClassRecord->setAttributes(self::arrayKeysCamelCase2Underscore($relationClass->attributes));
        $result = $relationClassRecord->save();

        if ($result) {
            $relationClass->setAttributes(self::arrayKeysUnderscore2CamelCase($relationClassRecord->attributes), false);
        } else {
            $relationClass->addErrors($relationClassRecord->getErrors());
        }

        return $result;
    }

    /**
     * @param $id
     * @return bool
     * @throws HttpException
     * @throws ServerErrorHttpException
     */
    public function deleteRelationClassById($id)
    {
        $model = RelationClassRecord::findOne($id);

        if ($model === null) {
            throw new HttpException(404, basename(__FILE__, '.php') . __LINE__);
        }

        Yii::$app->getDb()->beginTransaction();

        try {
            $relationGroups = $model->getRelationGroups()->all();
            /** @var RelationGroupRecord $relationGroup */
            foreach ($relationGroups as $relationGroup) {
                BaseCrudModel::deleteRows($relationGroup->getRelationHierarchies()->all());
                BaseCrudModel::deleteRows($relationGroup->getRelationInclusions()->all());
            }
            BaseCrudModel::deleteRows($relationGroups);
            BaseCrudModel::deleteRows($model->getElementClass2relationClasses()->all());
        } catch (ServerErrorHttpException $e) {
            Yii::$app->getDb()->getTransaction()->rollBack();

            return false;
        }

        if (!$model->delete()) {
            Yii::$app->getDb()->getTransaction()->rollBack();

            return false;
        }

        Yii::$app->getDb()->getTransaction()->commit();

        return true;
    }

    /**
     * @return string[]
     */
    public function primaryKey()
    {
        return RelationClassRecord::primaryKey();
    }
}