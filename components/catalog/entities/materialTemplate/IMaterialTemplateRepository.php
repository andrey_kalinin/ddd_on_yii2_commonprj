<?php
/**
 * @package         FurniPrice
 * @subpackage      Catalog
 * @category        Entity
 * @created         2018.01.22
 * @author          Vasiliy Konakov
 * @updated         2018.01.23
 * @updatedBy       Vasiliy Konakov
 */

namespace commonprj\components\catalog\entities\materialTemplate;

use yii\db\Exception;
use yii\web\HttpException;
use commonprj\components\catalog\entities\material\Material;

/**
 * Class IMaterialTemplateRepository
 * @package commonprj\components\catalog\entities\materialTemplate
 */
interface IMaterialTemplateRepository
{

    /**
     * getMaterials
     * @param MaterialTemplate $element
     * @return Material[]
     * @throws HttpException|Exception
     */
    public function getMaterials(MaterialTemplate $element): array;

}