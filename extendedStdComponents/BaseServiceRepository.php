<?php

namespace commonprj\extendedStdComponents;

use ReflectionClass;
use Yii;
use yii\base\InvalidParamException;
use yii\helpers\ArrayHelper;
use yii\helpers\BaseInflector;
use yii\httpclient\Response;
use yii\web\HttpException;
use yii\httpclient\Client;
use yii\web\NotFoundHttpException;

/**
 * Class BaseServiceRepository
 * @package commonprj\extendedStdComponents
 * Класс - родитель, для всех ServiceRepository
 */
class BaseServiceRepository implements IBaseRepository
{
    /**
     * @var Client
     */
    protected $restServer;

    /**
     * @var string
     * Параметр отвечающий за базовый API URI конкретной сущности
     */
    protected $baseUri = null;

    /**
     * @var string
     * API URI, обязательно нужно определять в любом методе дочернего класса
     */
    protected $requestUri = null;

    /**
     * @var array
     * Параметры добавляемые к API calls, если нужно передать в API дополнительные параметры,
     * нужно переопределить данный параметр перед вызовом любого метода данного класса
     */
    protected $requestParams = [];

    /**
     * Массив запрещенных методов CRUD
     * @var array
     */
    protected $forbiddenMethods = [];

    const CORE_ENTITIES_NAMESPACE = 'commonprj\components\core\entities';

    /**
     * @return string
     */
    protected function getRequestUri()
    {
        return $this->requestUri;
    }

    /**
     * @param string $uriPart
     * @return string
     */
    protected function getBaseUri($uriPart = '')
    {
        return trim($this->baseUri . $uriPart, '/');
    }

    /**
     * Метод совершает запрос к АПИ, получает JSON, преобразует его в массив и возвращает, в случае ошибки или вернет
     * пустой массив, или запишет ошибку в лог и выбросит 404 ошибку, за это отвечает параметр $throwException
     * @param array|null $queryParams
     * @return array
     * @throws HttpException
     * @throws \yii\httpclient\Exception
     */
    protected function getApiData(array $queryParams = [])
    {
        if (empty($this->requestUri) || !is_string($this->requestUri)) {
            throw new InvalidParamException('RequestUri cannot be blank!');
        }

        $response = $this->restServer->get($this->getRequestUri(), $queryParams)->send();
        $content = $response->getData();

        if (!$response->getIsOk()) {
            $this->throwRestServerExceptionMessage($response);
        }

        return $content;
    }

    /**
     * @param array $data
     * @param string $modelClassName
     * @return BaseCrudModel Метод мапит одну доменную модель
     * Метод мапит одну доменную модель
     */
    protected function getOneModel(array $data, string $modelClassName = null)
    {
        $modelClassName = $modelClassName ?? $data['entity'] ?? null;

        if (!$modelClassName) {
            throw new InvalidParamException('Can\'t map model, class name is empty! ' . __FILE__ . __LINE__);
        }

        $model = $this->createEntityFromData($data, $modelClassName);

        return $model;
    }

    /**
     * @param array $data
     * @param string $modelClassName
     * @return array|BaseCrudModel[] Метод мапит массив доменных моделей
     * Метод мапит массив доменных моделей
     */
    protected function getArrayOfModels(array $data, string $modelClassName = null): array
    {
        $modelClassName = $modelClassName ?? $data['entity'] ?? null;

        if (!$modelClassName) {
            throw new InvalidParamException('Can\'t map model, class name is empty! ' . __FILE__ . __LINE__);
        }

        $result = $this->createEntitiesFromArray($data, $modelClassName);

        return $result;
    }

    /**
     * @param array $arModel
     * @param string $jsonBoolKey
     * @return bool Метод возвращает boolean результат
     * Метод возвращает boolean результат
     * @internal param string $requestUri
     */
    protected function getBooleanResult(array $arModel, string $jsonBoolKey): bool
    {
        $result = false;

        if ($arModel) {
            $result = $arModel[$jsonBoolKey];
        }

        return $result;
    }

    /**
     * @param $entity
     * @param array|null $condition
     * @return array|mixed
     * @throws HttpException
     * @throws NotFoundHttpException
     * @throws \yii\httpclient\Exception
     */
    public function findAll(IBaseCrudModel $entity, $condition = null)
    {
        $this->checkForbiddenCRUDMethod(__FUNCTION__);
        $uri = $this->getBaseUri();

        $response = $this->restServer->get($uri, $condition)->send();

        if (!$response->getIsOk()) {
            $this->throwRestServerExceptionMessage($response);
        }

        $content = $response->getData();

        if (isset($content['items'])) {
            $items = $content['items'];
            // создаем сущности на основе полученного json
            $content['items'] = $this->createEntitiesFromArray($items, get_class($entity));
        } elseif (is_array($content)) {
            // если нет пагинации
            $content = $this->createEntitiesFromArray($content, get_class($entity));
        }

        return $content;
    }

    /**
     * @param IBaseCrudModel $entity
     * @param $id
     * @return IBaseCrudModel
     * @throws HttpException
     * @throws NotFoundHttpException
     */
    public function findOne(IBaseCrudModel $entity, $id)
    {
        if (ArrayHelper::isAssociative($id)) {
            $id = $id['id'] ?? null;
        }

        $this->checkForbiddenCRUDMethod(__FUNCTION__);
        $uri = $this->getBaseUri($id);

        $response = $this->restServer->get($uri)->send();

        if (!$response->getIsOk()) {
            throw new HttpException(404, 'Not found');
        }

        $content = $response->getData();

        $model = $this->createEntityFromData($content, get_class($entity));

        return $model;
    }

    /**
     * Заполняет сущность данными
     * @param BaseCrudModel $entity
     * @param array $data
     */
    public function populateRecord(BaseCrudModel $entity, $data)
    {
        foreach ($entity->attributes() as $name) {
            $value = $data[$name] ?? null;

            if (is_null($value)) {
                continue;
            }

            $entity->setOldAttribute($name, $value);
            $entity->{$name} = $value;
        }

    }

    public function createEntityFromData($data, $entityClass)
    {
        $model = $entityClass::instantiate();
        $entityClass::populateRecord($model, $data);

        return $model;
    }

    public function createEntitiesFromArray($data, $entityClass)
    {

        foreach ((array)$data as $datum) {
            $result[] = $this->createEntityFromData($datum, $entityClass);
        }

        return $result ?? [];
    }

    /**
     * @param IBaseCrudModel $entity
     * @param bool $runValidation
     * @param null $attributes
     * @return bool
     * @throws HttpException
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function save(IBaseCrudModel $entity, $runValidation = true, $attributes = null): bool
    {
        $this->checkForbiddenCRUDMethod('save');

        if ($this->isNew($entity)) {
            return $this->insert($entity, $runValidation, $attributes);
        }

        return $this->update($entity, $runValidation, $attributes);
    }

    /**
     * @param $entity
     * @return bool
     * @throws HttpException
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function update(IBaseCrudModel $entity): bool
    {
        $this->checkForbiddenCRUDMethod('update');

        $params = $entity->getAttributes();
        unset($params['entity']);
        unset($params['id']);

        $response = $this->restServer->put($this->getBaseUri($entity->id), $params)->send();

        if (!$response->getIsOk()) {
            $this->throwRestServerExceptionMessage($response);
        }

        $entity->id = $response->headers['id'];
        return true;
    }

    /**
     * @param IBaseCrudModel $entity
     * @param bool $runValidation
     * @param null $attributes
     * @return bool
     * @throws HttpException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function insert(IBaseCrudModel $entity, $runValidation = true, $attributes = null): bool
    {
        $params = $entity->getAttributes();
        unset($params['entity']);
        unset($params['id']);

        $response = $this->restServer->post($this->getBaseUri(), $params)->send();

        if (!$response->getIsOk()) {
            $this->throwRestServerExceptionMessage($response);
        }

        $entity->id = $response->headers['id'];
        return true;
    }

    /**
     * @param $entity
     * @return bool
     * @throws NotFoundHttpException
     */
    public function delete(IBaseCrudModel $entity): bool
    {
        $this->checkForbiddenCRUDMethod(__FUNCTION__);
        $uri = $this->getBaseUri($entity->id);
        $response = $this->restServer->delete($uri)->send();

        return $response->getIsOk();
    }

    /**
     * @param string $method
     * @throws NotFoundHttpException
     */
    protected function checkForbiddenCRUDMethod(string $method)
    {
        if (in_array($method, $this->forbiddenMethods)) {
            throw new NotFoundHttpException('Метод не существует для данного ресурса');
        }
    }

    /**
     * @param $entity
     * @param $values
     * @param bool $safeOnly
     */
    public function setAttributes($entity, $values, $safeOnly = true)
    {
        foreach ($entity->attributes() as $key) {
            $value = $values[$key] ?? null;

            if ($value !== null) {
                $entity->{$key} = $value;
            }

        }

    }

    /**
     * @param $entity
     * @return array
     */
    public function getAttributes($entity)
    {
        foreach ($entity->attributes() as $key) {
            $values[$key] = $entity->{$key};
        }

        return $values ?? [];
    }

    /**
     * @param $entity
     * @return array
     * @throws \ReflectionException
     */
    public function attributes($entity)
    {
        $skipFileds = [
            'repository',
        ];
        $class = new ReflectionClass($entity);
        $names = [];
        foreach ($class->getProperties(\ReflectionProperty::IS_PUBLIC) as $property) {
            if (!$property->isStatic() && !in_array($property->getName(), $skipFileds)) {
                $names[] = $property->getName();
            }
        }

        return $names;
    }

    /**
     * @param Response $response
     * @throws HttpException
     * @throws \yii\httpclient\Exception
     */
    protected function throwRestServerExceptionMessage(Response $response)
    {
        $content = $response->getData();
        $message = (!empty($content)) ? ($content['message'] ?? '') : '';
        throw new HttpException($response->getStatusCode(), 'restServer '. $message);
    }

    /**
     * @param $entity
     * @return bool
     */
    protected function isNew($entity)
    {
        return (isset($entity->id)) ? false : true;
    }


    /**
     * Преобразует массив условий в строку запроса
     * @param $conditions
     * @return string
     */
    public function buildQueryString($conditions)
    {
        $result = [];

        foreach ($conditions as $key => $condition) {

            if (is_string($key)) {
                $field = $key;
                $operation = in_array($key, ['page', 'perPage']) ? null : 'eq';
                $value = $condition;
            } else {
                list($operation, $field, $value) = $condition;
            }

            $queryKey = $operation ? "{$field}__{$operation}" : $field;
            $queryValue = $value;

            $result[$queryKey] = $queryValue;
        }

        return http_build_query($result);
    }

    /**
     * Преобразует строку запроса в массив условий
     * @param $queryString
     * @return array
     */
    public function buildConditions($queryString)
    {
        parse_str($queryString, $result);
        $conditions = [];

        foreach ($result as $key => $value) {
            @list($field, $operation) = explode('__', $key);

            if ($operation && strtolower($operation) !== 'eq') {
                $conditions[] = [$operation, $field, $value];
            } else {
                $conditions[$field] = $value;
            }
        }

        return $conditions;
    }

    /**
     * Возвращает массив переводов
     * @param BaseCrudModel $entity
     * @return array
     * @throws HttpException
     * @throws \yii\httpclient\Exception
     */
    public function getTranslations(BaseCrudModel $entity)
    {
        $baseUri = ($this->getBaseUri($entity->id . '/translation'));
        $response = $this->restServer->get($baseUri)->send();

        if (!$response->getIsOk()) {
            $this->throwRestServerExceptionMessage($response);
        }

        $result = $response->getData();

        return $result;
    }
    
    /**
     * @param string $className
     * @param array $condition
     * @return int|null
     * @throws HttpException
     */
    public static function getIdByEntityClassAndCondition(string $className, array $condition = []): ?int
    {
        $errorMessage = "Не удалось получить entity `{$className}`";

        $response = $className::findAll($condition);

        try {
            $entity = reset($response['items']);
        } catch (\Exception $e) {
            throw new HttpException(500, $errorMessage);
        }

        if ($entity instanceof $className) {
            return $entity->id;
        }

        throw new HttpException(500, $errorMessage);
    }
}
