<?php

namespace commonprj\components\catalog\entities\priceCategory;

use commonprj\components\core\entities\element\ElementDBRepository;
use commonprj\components\core\entities\property\Property;
use commonprj\components\crm\entities\manufacturer\Manufacturer;
use Yii;

class PriceCategoryDBRepository extends ElementDBRepository implements IPriceCategoryRepository
{

    public function bindManufacturer(PriceCategory $priceCategory, int $manufacturerId): bool
    {
        $propertyPriceCategory2ManufacturerId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($priceCategory, Property::PROPERTY_SYSNAME__PRICE_CATEGORY2MANUFACTURER);

        return $priceCategory->bindAssociatedEntity($propertyPriceCategory2ManufacturerId, $manufacturerId);
    }

    public function unbindManufacturer(PriceCategory $priceCategory, int $manufacturerId): bool
    {
        $propertyPriceCategory2ManufacturerId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($priceCategory, Property::PROPERTY_SYSNAME__PRICE_CATEGORY2MANUFACTURER);

        return $priceCategory->unbindAssociatedEntity($propertyPriceCategory2ManufacturerId, $manufacturerId);
    }

    public function getManufacturer(PriceCategory $priceCategory): ?Manufacturer
    {
        $propertyPriceCategory2ManufacturerId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($priceCategory, Property::PROPERTY_SYSNAME__PRICE_CATEGORY2MANUFACTURER);

        $manufacturer = $priceCategory->getAssociatedEntity($propertyPriceCategory2ManufacturerId);
        if ($manufacturer) {
            $manufacturer->properties = $manufacturer->getProperties();
        }

        return $manufacturer;
    }

}
