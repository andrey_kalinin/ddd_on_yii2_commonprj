<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 15.12.2017
 * Time: 12:05
 */

namespace commonprj\components\core\entities\propertyArrayValue;

use commonprj\components\core\entities\abstractPropertyValue\AbstractPropertyValue;
use commonprj\components\core\models\ElementRecord;
use commonprj\components\core\models\PropertyRecord;
use commonprj\extendedStdComponents\BaseCrudModel;
use yii\db\ActiveRecord;

class PropertyArrayValue extends AbstractPropertyValue
{
    public $name;
    public $values = [];
    public $valueIds;
    public $searchdbPropertyValueId;

    protected static $propertyValueClass;

    /**
     * PropertyRangeValue constructor.
     * @param array $config
     * @param ActiveRecord|null $activeRecord
     */
    public function __construct(array $config = [], ActiveRecord $activeRecord = null)
    {
        parent::__construct($config);
        $this->repository = \Yii::$app->propertyArrayValueRepository;
        $this->multiplicityId = AbstractPropertyValue::MULTIPLICITY_ARRAY;
    }

    /**
     * @param ActiveRecord $propertyRecord
     * @return array
     */
    public function getValues(ActiveRecord $propertyRecord): array
    {
        $propertyTypeId = $propertyRecord->getPropertyType()->one()->id;
        static::$propertyValueClass = $this->repository::getPropertyValueClassByTypeId($propertyTypeId);

        $propertyArrayRecords = $propertyRecord->getPropertyArrays()->all();
        $result = [];

        foreach ($propertyArrayRecords as $propertyArrayRecord) {

            $propertyArrayValue = (new PropertyArrayValue())->findOne([
                'condition' => ['id' => $propertyArrayRecord->getAttribute('id')],
            ]);

            $result[] = $propertyArrayValue;
        }

        return $result;
    }

    /**
     * @param string $valueIds
     * @return array
     */
    public function valueIds2array(string $valueIds): array
    {
        return $this->repository->valueIds2array($valueIds);
    }

    /**
     * @return string
     */
    static function getPropertyValuesMethod()
    {
        return 'getPropertyArrays';
    }

    /**
     * @return bool
     */
    public function delete(): bool
    {
        return $this->repository->delete($this);
    }

    /**
     * Active record style validation for not-active record Model
     * @return array
     */
    public function rules(): array
    {
        return [
            [['id', 'element_id', 'property_id', 'valueIds', 'name'], 'required'],
            [['id', 'element_id', 'property_id'], 'integer'],
            [['name', 'valueIds'], 'string'],
            [['element_id'], 'exist', 'skipOnError' => true, 'targetClass' => ElementRecord::className(), 'targetAttribute' => ['element_id' => 'id']],
            [['property_id'], 'exist', 'skipOnError' => true, 'targetClass' => PropertyRecord::className(), 'targetAttribute' => ['property_id' => 'id']],
        ];
    }

}