<?php

namespace commonprj\components\catalog\entities\component;

use commonprj\components\catalog\entities\component\Component;
use commonprj\components\catalog\entities\material\Material;

class ComponentServiceRepository implements IComponentRepository
{

    /**
     * @param Component $component
     * @param int $materialId
     * @return bool
     */
    public function bindMaterial(Component $component, int $materialId): bool
    {
        // TODO: Implement bindMaterial() method.
    }

    /**
     * @param Component $component
     * @param int $materialId
     * @return bool
     */
    public function unbindMaterial(Component $component, int $materialId): bool
    {
        // TODO: Implement unbindMaterial() method.
    }

    /**
     * @param Component $component
     * @return Material[]
     */
    public function getMaterials(Component $component): array
    {
        // TODO: Implement getMaterials() method.
    }

}
