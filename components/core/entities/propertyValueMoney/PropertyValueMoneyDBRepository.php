<?php

namespace commonprj\components\core\entities\propertyValueMoney;

use commonprj\components\core\entities\abstractPropertyValue\AbstractPropertyValueDBRepository;
use commonprj\components\core\models\PropertyValueMoneyRecord;
use commonprj\extendedStdComponents\IBaseCrudModel;
use Yii;
use yii\db\Expression;
use yii\db\Query;

/**
 * Class PropertyValueMoneyDBRepository
 * @package commonprj\components\core\entities\propertyValueMoney
 */
class PropertyValueMoneyDBRepository extends AbstractPropertyValueDBRepository
{
    public $activeRecordClass = PropertyValueMoneyRecord::class;

    /**
     * @param IBaseCrudModel $entity
     * @param bool $runValidation
     * @param null $attributes
     * @return bool
     */
    public function save(IBaseCrudModel $entity, $runValidation = true, $attributes = null): bool
    {
        $currencyService = $this->getCurrencyService();
        $response = $currencyService->getCurrencyData($entity->value);

        $entity->value = $response['value'];
        $entity->currencyListItemId = $response['currencyListItemId'];

        return parent::save($entity, $runValidation, $attributes);
    }

    protected function getCurrencyService()
    {
        return Yii::$app->currency;
    }

    /**
     * Возвращает строку для UNION запроса для получения свойств со значениями элемента
     * @param int $elementId
     * @return Query
     */
    public function getValueQueryBuilder(int $elementId)
    {
        $table = $this->activeRecordClass::tableName();

        $queryBuilder = (new Query())
            ->select([
                "{$table}.id",
                'value' => "concat_ws(',', \"{$table}\".value, \"property_list_item\".item)",
                "{$table}.property_id",
                "{$table}.element_id",
                'prop_class' => new Expression("'" . $this->activeRecordClass . "'"),
            ])
            ->from($table)
            ->leftJoin('property_list_item', "{$table}.currency_list_item_id = property_list_item.id")
            ->where(['element_id' => $elementId]);

        $queryBuilder->addSelect(['searchdb_property_value_id' => new Expression(0)]);

        return $queryBuilder;
    }
}