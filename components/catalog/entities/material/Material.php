<?php
/**
 * @package         FurniPrice
 * @subpackage      Catalog
 * @category        Entity
 * @created         2018.01.22
 * @author          Vasiliy Konakov
 * @updated         2018.01.23
 * @updatedBy       Vasiliy Konakov
 */

namespace commonprj\components\catalog\entities\material;

use Yii;
use yii\db\Exception;
use yii\web\HttpException;
use commonprj\components\core\entities\element\Element;
use commonprj\components\catalog\entities\component\Component;
use commonprj\components\catalog\entities\materialCollection\MaterialCollection;
use commonprj\components\catalog\entities\materialGroup\MaterialGroup;
use commonprj\components\catalog\entities\materialTemplate\MaterialTemplate;
use commonprj\components\catalog\entities\productModel\ProductModel;

class Material extends Element
{

    /**
     * @var MaterialDBRepository $repository
     */
    public $repository;

    /**
     * @var int $materialGroupId
     */
    public $materialGroupId;

    /**
     * @var int $materialTemplateId
     */
    public $materialTemplateId;

    public $hierarchyChildren;

    /**
     * РџСЂРёСЃРІРѕРµРЅРёРµ СЃРІРѕР№СЃС‚РІСѓ РґРѕРјРµРЅРЅРѕРіРѕ СЃР»РѕСЏ $repository - СЃРѕРѕС‚РІРµС‚СЃС‚РІСѓСЋС‰РµРіРѕ РєРѕРјРїРѕРЅРµРЅС‚Р°
     * @inheritdoc
     */

    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->repository = Yii::$app->materialRepository;
    }

    /**
     * bindMaterialGroup
     * @param int $materialGroupId
     * @return bool
     * @throws HttpException|Exception
     */
    public function bindMaterialGroup(int $materialGroupId): bool
    {
        return $this->repository->bindMaterialGroup($this, $materialGroupId);
    }

    /**
     * unbindMaterialGroup
     * @param int $materialGroupId
     * @return bool
     * @throws HttpException|Exception
     */
    public function unbindMaterialGroup(int $materialGroupId): bool
    {
        return $this->repository->unbindMaterialGroup($this, $materialGroupId);
    }

    /**
     * getMaterialGroups
     * @return null|MaterialGroup
     * @throws HttpException|Exception
     */
    public function getMaterialGroups(): array
    {
        return $this->repository->getMaterialGroups($this);
    }

    /**
     * bindMaterialTemplate
     * @param int $materialTemplateId
     * @return bool
     * @throws HttpException|Exception
     */
    public function bindMaterialTemplate(int $materialTemplateId): bool
    {
        return $this->repository->bindMaterialTemplate($this, $materialTemplateId);
    }

    /**
     * unbindMaterialTemplate
     * @param int $materialTemplateId
     * @return bool
     * @throws HttpException|Exception
     */
    public function unbindMaterialTemplate(int $materialTemplateId): bool
    {
        return $this->repository->unbindMaterialTemplate($this, $materialTemplateId);
    }

    /**
     * @param array $queryParams
     * @return MaterialTemplate|null
     * @throws Exception
     * @throws HttpException
     */
    public function getMaterialTemplate(array $queryParams = []): ?MaterialTemplate
    {
        return $this->repository->getMaterialTemplate($this, $queryParams);
    }

    /**
     * getComponents
     * @return Component[]
     * @throws HttpException|Exception
     */
    public function getComponents(): array
    {
        return $this->repository->getComponents($this);
    }

    /**
     * getProductModels
     * @return ProductModel[]
     * @throws HttpException|Exception
     */
    public function getProductModels(): array
    {
        return $this->repository->getProductModels($this);
    }

    /**
     * bindCompositionChild
     * bind curent material to the parent material with ID = $compositionMaterialId
     * In the process of use, we will look and, perhaps, change the direction.
     * @param int $compositionMaterialId
     * @return bool
     * @throws HttpException|Exception
     */
    public function bindCompositionChild(int $compositionChildId): bool
    {
        return $this->repository->bindCompositionChild($this, $compositionChildId);
    }

    /**
     * unbindCompositionChild
     * unbind from curent parent material child one with ID = $compositionMaterialId
     * In the process of use, we will look and, perhaps, change the direction.
     * @param int $compositionMaterialId
     * @return bool
     * @throws HttpException|Exception
     */
    public function unbindCompositionChild(int $compositionChildId): bool
    {
        return $this->repository->unbindCompositionChild($this, $compositionChildId);
    }

    /**
     * getCompositionChildren
     * @return array Material[] (domain layer objects)
     * @throws HttpException|Exception
     */
    public function getCompositionChildren(): array
    {
        return $this->repository->getCompositionChildren($this);
    }


    /**
     * @return Material[]
     */
    public function getCompositionParents(): array
    {
        return $this->repository->getCompositionParents($this);
    }

    /**
     * @param int $childMaterialId
     * @return bool
     */
    public function bindHierarchyChild(int $childMaterialId): bool
    {
        return $this->repository->bindHierarchyChild($this, $childMaterialId);
    }

    /**
     * @param int $childMaterialId
     * @return bool
     */
    public function unbindHierarchyChild(int $childMaterialId): bool
    {
        return $this->repository->unbindHierarchyChild($this, $childMaterialId);
    }

    /**
     * @return Material[]
     */
    public function getHierarchyChildren(): array
    {
        return $this->repository->getHierarchyChildren($this);
    }

    /**
     * @return null|Material
     * @throws HttpException
     */
    public function getHierarchyParent(): ?Material
    {
        return $this->repository->getHierarchyParent($this);
    }

    /**
     * @param bool $isReturnFullObjects
     * @param bool $isReturnLinearArray
     * @return array
     * @throws Exception
     * @throws HttpException
     */
    public function getHierarchyDescendants($isReturnFullObjects = true, $isReturnLinearArray = false)
    {
        return $this->repository->getHierarchyDescendants($this, $isReturnFullObjects, $isReturnLinearArray);
    }

    /**
     * @param bool $isReturnFullObjects
     * @param bool $isReturnLinearArray
     * @return mixed
     * @throws Exception
     * @throws HttpException
     */
    public function getFullListInHierarchy($isReturnFullObjects = true, $isReturnLinearArray = false)
    {
        return $this->repository->getFullListInHierarchy($this, $isReturnFullObjects, $isReturnLinearArray);
    }

    public function getMaterialTemplateInHierarchy()
    {
        return $this->repository->getMaterialTemplateInHierarchy($this);
    }
}