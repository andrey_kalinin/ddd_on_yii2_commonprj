<?php

namespace commonprj\components\core\entities\propertyRangeValue;

use commonprj\components\core\entities\abstractPropertyValue\AbstractPropertyValue;
use commonprj\components\core\models\ElementRecord;
use commonprj\components\core\models\PropertyRecord;
use yii\db\ActiveRecord;

/**
 * Class PropertyRange
 * @package commonprj\components\core\entities\propertyRangeValue
 */
class PropertyRangeValue extends AbstractPropertyValue
{
    public $id;
    public $name;
    public $propertyId;
    public $fromValueId;
    public $toValueId;
    public $fromValue;
    public $toValue;
    public $multiplicityId = AbstractPropertyValue::MULTIPLICITY_RANGE;

    protected static $propertyValueClass;

    /**
     * PropertyRangeValue constructor.
     * @param array $config
     * @param ActiveRecord|null $activeRecord
     */
    public function __construct(array $config = [], ActiveRecord $activeRecord = null)
    {
        parent::__construct($config);
        $this->repository = \Yii::$app->propertyRangeValueRepository;
    }

    /**
     * @param ActiveRecord $propertyRecord
     * @return array
     */
    public function getValues(ActiveRecord $propertyRecord): array
    {
        $propertyTypeId = $propertyRecord->getPropertyType()->one()->id;
        static::$propertyValueClass = $this->repository::getPropertyValueClassByTypeId($propertyTypeId);

        return parent::getValues($propertyRecord);
    }

    /**
     * @param ActiveRecord $propertyValueRecord
     * @return mixed
     */
    public function setValue(ActiveRecord $propertyValueRecord)
    {
        $value = parent::setValue($propertyValueRecord);

        $value->fromValue = (new static::$propertyValueClass())->findOne([
            'condition' => ['id' => $value->fromValueId]
        ])->value;

        $value->toValue = (new static::$propertyValueClass())->findOne([
            'condition' => ['id' => $value->toValueId]
        ])->value;

        $value->multiplicityId = 2;

        return $value;

    }

    /**
     * @return bool
     */
    public function delete(): bool
    {
        return $this->repository->delete($this);
    }

    /**
     * Active record style validation for not-active record Model
     * @return array
     */
    public function rules(): array
    {
        return [
            [['id', 'element_id', 'property_id', 'name'], 'required'],
            [['id', 'element_id', 'property_id', 'from_value_id', 'to_value_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['element_id'], 'exist', 'skipOnError' => true, 'targetClass' => ElementRecord::className(), 'targetAttribute' => ['element_id' => 'id']],
            [['property_id'], 'exist', 'skipOnError' => true, 'targetClass' => PropertyRecord::className(), 'targetAttribute' => ['property_id' => 'id']],
        ];
    }
}
