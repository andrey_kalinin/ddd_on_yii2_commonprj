<?php
/**
 * @package         FurniPrice
 * @subpackage      Core
 * @category        DBRepository
 * @created         2017.12.20
 * @author          Vasiliy Konakov
 * @updated         2018.01.19
 * @updatedBy       Vasiliy Konakov
 */

namespace commonprj\components\core\entities\relationValue;

use commonprj\components\core\entities\abstractPropertyValue\AbstractPropertyValue;
use commonprj\extendedStdComponents\IBaseCrudModel;
use Yii;
use yii\db\Exception;
use yii\db\ActiveRecord;
use commonprj\components\core\models\PropertyValueBigintRecord;
use commonprj\components\core\entities\propertyValueBigint\PropertyValueBigintDBRepository;

/**
 * Class RelationValueDBRepository
 * @package commonprj\components\core\entities\relationValue
 */
class RelationValueDBRepository extends PropertyValueBigintDBRepository implements IRelationValueRepository
{
    /**
     * @param AbstractPropertyValue $propertyValue
     * @return ActiveRecord[]
     */
    public function getRecords(AbstractPropertyValue $propertyValue)
    {
        return PropertyValueBigintRecord::find()->where([
            'element_id'  => $propertyValue->elementId,
            'property_id' => $propertyValue->propertyId,
            'value'       => $propertyValue->value,
        ])->orWhere([
            'element_id'  => $propertyValue->value,
            'property_id' => $propertyValue->propertyId,
            'value'       => $propertyValue->elementId,
        ])->all();
    }

    /**
     * @param IBaseCrudModel $propertyValue
     * @param bool $runValidation
     * @param null $attributes
     * @return bool
     * @throws Exception
     * @throws \Exception
     * @throws \Throwable
     */
    public function save(IBaseCrudModel $propertyValue, $runValidation = true, $attributes = null): bool
    {
        $reversePropertyValue = $this->createReverseRecord($propertyValue);
        $transaction = Yii::$app->getDb()->beginTransaction();

        try {
            $res1 = parent::save($propertyValue);
            $res2 = parent::save($reversePropertyValue);

            if (!$res1 || !$res2) {
                $transaction->rollBack();
                return false;
            }

        } catch (Exception $exception) {
            $transaction->rollBack();
            return false;
        }

        $transaction->commit();
        return true;
    }

    /**
     * @param RelationValue $propertyValue
     * @return bool
     * @throws Exception
     */
    public function delete(IBaseCrudModel $propertyValue): bool
    {
        $records = $this->getRecords($propertyValue);
        $transaction = Yii::$app->getDb()->beginTransaction();
        foreach ((array)$records as $record) {
            if (!$record->delete()) {
                $record->addErrors($record->getErrors());
                $transaction = Yii::$app->getDb()->getTransaction();
                $transaction->rollBack();
                return false;
            }
        }
        $transaction->commit();
        return true;
    }

    protected function createReverseRecord(AbstractPropertyValue $propertyValue)
    {
        $reversePropertyValue = clone $propertyValue;

        $reversePropertyValue->elementId = $propertyValue->value;
        $reversePropertyValue->value = $propertyValue->elementId;
        $reversePropertyValue->isParent = !$propertyValue->isParent;

        return $reversePropertyValue;
    }

}
