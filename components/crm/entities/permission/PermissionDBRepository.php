<?php

namespace commonprj\components\crm\entities\permission;

use Yii;
use commonprj\components\core\entities\element\Element;
use commonprj\components\core\entities\element\ElementDBRepository;
use commonprj\components\core\entities\property\Property;
use commonprj\components\crm\entities\permissionGroup\PermissionGroup;

class PermissionDBRepository extends ElementDBRepository implements IPermissionRepository
{
    /**
     * @param Permission $permission
     * @param int $permissionGroupId
     * @return bool
     */
    public function bindPermissionGroup(Permission $permission, int $permissionGroupId): bool
    {
        $propertyId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($permission,
            Property::PROPERTY_SYSNAME__PERMISSION2PERMISSION_GROUP);

        return $permission->bindAssociatedEntity($propertyId, $propertyId);
    }


    /**
     * @param Permission $permission
     * @param int $permissionGroupId
     * @return bool
     */
    public function unbindPermissionGroup(Permission $permission, int $permissionGroupId): bool
    {
        $propertyId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($permission,
            Property::PROPERTY_SYSNAME__PERMISSION2PERMISSION_GROUP);

        return $permission->unbindAssociatedEntity($propertyId, $propertyId);
    }

    /**
     * @param Permission $permission
     * @return PermissionGroup
     * @throws \yii\web\HttpException
     */
    public function getPermissionGroup(Permission $permission): PermissionGroup
    {
        $propertyId = Yii::$app->propertyService->getPropertyIdByElementAndSysname($permission,
            Property::PROPERTY_SYSNAME__PERMISSION2PERMISSION_GROUP);

        $elements = $permission->getAssociatedEntities($propertyId);

        foreach ($elements as &$element) {
            /** @var Element $element */
            $element->properties = $element->getProperties();
        }

        return $elements;
    }

}