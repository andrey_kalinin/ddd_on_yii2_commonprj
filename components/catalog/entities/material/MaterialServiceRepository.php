<?php

namespace commonprj\components\catalog\entities\material;

use commonprj\components\catalog\entities\component\Component;
use commonprj\components\catalog\entities\materialGroup\MaterialGroup;
use commonprj\components\catalog\entities\materialTemplate\MaterialTemplate;
use commonprj\components\catalog\entities\productModel\ProductModel;
use commonprj\components\core\entities\element\ElementServiceRepository;
use Yii;
use yii\httpclient\Client;

/**
 * Class MaterialServiceRepository
 * @package commonprj\components\catalog\entities\Material
 */
class MaterialServiceRepository extends ElementServiceRepository
{
    /**
     * @var Client
     */
    protected $restServer;

    /**
     * @var string
     */
    protected $baseUri = 'material/';

    /**
     * MaterialServiceRepository constructor.
     */
    public function __construct()
    {
        $this->restServer = Yii::$app->catalogManager;
    }

    /**
     * @param Material $material
     * @param int $materialGroupId
     * @return bool
     */
    public function bindMaterialGroup(Material $material, int $materialGroupId)
    {
        $uri = $this->getBaseUri($material->id . '/material-group/' . $materialGroupId);
        $response = $this->restServer->post($uri)->send();
        return $response->getIsOk();
    }

    /**
     * @param Material $material
     * @param int $materialGroupId
     * @return bool
     */
    public function unbindMaterialGroup(Material $material, int $materialGroupId)
    {
        $uri = $this->getBaseUri($material->id . '/material-group/' . $materialGroupId);
        $response = $this->restServer->delete($uri)->send();
        return $response->getIsOk();
    }

    /**
     * @param Material $material
     * @return array|\commonprj\extendedStdComponents\BaseCrudModel[]
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\HttpException
     */
    public function getMaterialGroups(Material $material)
    {
        $this->requestUri = $this->getBaseUri($material->id . '/material-groups');
        $content = $this->getApiData();

        if (!empty($content)) {
            $result = $this->getArrayOfModels($content, MaterialGroup::className());
        }

        return $result ?? [];
    }

    /**
     * @param Material $material
     * @param int $materialTemplateId
     * @return bool
     */
    public function bindMaterialTemplate(Material $material, int $materialTemplateId)
    {
        $uri = $this->getBaseUri($material->id . '/material-template/' . $materialTemplateId);
        $response = $this->restServer->post($uri)->send();
        return $response->getIsOk();
    }

    /**
     * @param Material $material
     * @param int $materialTemplateId
     * @return bool
     */
    public function unbindMaterialTemplate(Material $material, int $materialTemplateId)
    {
        $uri = $this->getBaseUri($material->id . '/material-template/' . $materialTemplateId);
        $response = $this->restServer->delete($uri)->send();
        return $response->getIsOk();
    }

    /**
     * @param Material $material
     * @param array $queryParams
     * @return \commonprj\extendedStdComponents\BaseCrudModel|null
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\HttpException
     */
    public function getMaterialTemplate(Material $material, array $queryParams = [])
    {
        $this->requestUri = $this->getBaseUri($material->id . '/material-template');
        $content = $this->getApiData($queryParams);

        if (!empty($content)) {
            $result = $this->getOneModel($content, MaterialTemplate::className());
        }

        return $result ?? null;
    }

    /**
     * @param Material $material
     * @return \commonprj\extendedStdComponents\BaseCrudModel|null
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\HttpException
     */
    public function getComponents(Material $material)
    {
        $this->requestUri = $this->getBaseUri($material->id . '/components');
        $content = $this->getApiData();

        if (!empty($content)) {
            $result = $this->getOneModel($content, Component::className());
        }

        return $result ?? null;
    }

    /**
     * @param Material $material
     * @return \commonprj\extendedStdComponents\BaseCrudModel|null
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\HttpException
     */
    public function getProductModels(Material $material)
    {
        $this->requestUri = $this->getBaseUri($material->id . '/product-models');
        $content = $this->getApiData();

        if (!empty($content)) {
            $result = $this->getOneModel($content, ProductModel::className());
        }

        return $result ?? null;
    }

    /**
     * @param Material $material
     * @param int $compositionChildId
     * @return bool
     */
    public function bindCompositionChild(Material $material, int $compositionChildId)
    {
        $uri = $this->getBaseUri($material->id . '/composition/' . $compositionChildId);
        $response = $this->restServer->post($uri)->send();
        return $response->getIsOk();
    }

    /**
     * @param Material $material
     * @param int $compositionChildId
     * @return bool
     */
    public function unbindCompositionChild(Material $material, int $compositionChildId)
    {
        $uri = $this->getBaseUri($material->id . '/composition/' . $compositionChildId);
        $response = $this->restServer->delete($uri)->send();
        return $response->getIsOk();
    }

    /**
     * @param Material $material
     * @return \commonprj\extendedStdComponents\BaseCrudModel|null
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\HttpException
     */
    public function getCompositionChildren(Material $material)
    {
        $this->requestUri = $this->getBaseUri($material->id . '/composition/children');
        $content = $this->getApiData();

        if (!empty($content)) {
            $result = $this->getOneModel($content, CompositionMaterial::className());
        }

        return $result ?? null;
    }

    /**
     * @param Material $material
     * @return \commonprj\extendedStdComponents\BaseCrudModel|null
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\HttpException
     */
    public function getCompositionParents(Material $material)
    {
        $this->requestUri = $this->getBaseUri($material->id . '/composition/parents');
        $content = $this->getApiData();

        if (!empty($content)) {
            $result = $this->getOneModel($content, CompositionParent::className());
        }

        return $result ?? null;
    }

    /**
     * @param Material $material
     * @param int $hierarchyChildId
     * @return bool
     */
    public function bindHierarchyChild(Material $material, int $hierarchyChildId)
    {
        $uri = $this->getBaseUri($material->id . '/hierarchy/' . $hierarchyChildId);
        $response = $this->restServer->post($uri)->send();
        return $response->getIsOk();
    }

    /**
     * @param Material $material
     * @param int $hierarchyChildId
     * @return bool
     */
    public function unbindHierarchyChild(Material $material, int $hierarchyChildId)
    {
        $uri = $this->getBaseUri($material->id . '/hierarchy/' . $hierarchyChildId);
        $response = $this->restServer->delete($uri)->send();
        return $response->getIsOk();
    }

    /**
     * @param Material $material
     * @return array|\commonprj\extendedStdComponents\BaseCrudModel[]
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\HttpException
     */
    public function getHierarchyChildren(Material $material)
    {
        $this->requestUri = $this->getBaseUri($material->id . '/hierarchy/children');
        $content = $this->getApiData();

        if (!empty($content)) {
            $result = $this->getArrayOfModels($content, Material::className());
        }

        return $result ?? [];
    }

    /**
     * @param Material $material
     * @return \commonprj\extendedStdComponents\BaseCrudModel|null
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\HttpException
     */
    public function getHierarchyParent(Material $material)
    {
        $this->requestUri = $this->getBaseUri($material->id . '/hierarchy/parent');
        $content = $this->getApiData();

        if (!empty($content)) {
            $result = $this->getOneModel($content, Material::className());
        }

        return $result ?? null;
    }

    /**
     * @param Material $material
     * @return array|\commonprj\extendedStdComponents\BaseCrudModel
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\HttpException
     */
    public function getHierarchyDescendants(Material $material)
    {
        $this->requestUri = $this->getBaseUri($material->id . '/hierarchy/descendants');
        $content = $this->getApiData();

        if (!empty($content)) {
            $result = $this->getOneModel($content, Material::className());
        }

        return $result ?? [];
    }

    /**
     * @param Material $material
     * @return array|\commonprj\extendedStdComponents\BaseCrudModel
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\HttpException
     */
    public function getFullListInHierarchy(Material $material)
    {
        $this->requestUri = $this->getBaseUri($material->id . '/full-list-in-hierarchy');
        $content = $this->getApiData();

        if (!empty($content)) {
            $result = $this->getOneModel($content, Material::className());
        }

        return $result ?? [];
    }

}