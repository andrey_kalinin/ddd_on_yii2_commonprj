<?php

namespace commonprj\components\core\models\search;

use Yii;

/**
 * This is the model class for table "property".
 *
 * @property integer $id
 * @property string $name
 * @property string $sysname
 * @property string $description
 * @property integer $property_unit_id
 * @property boolean $is_specific
 * @property integer $property_type_id
 * @property integer $property_group_id
 * @property integer $sort_order
 *
 * @property PropertyGroupRecord $propertyGroup
 * @property PropertyTypeRecord $propertyType
 * @property PropertyUnitRecord $propertyUnit
 * @property Property2elementClassRecord[] $property2elementClasses
 * @property ElementClassRecord[] $elementClasses
 * @property PropertyArrayRecord[] $propertyArrays
 * @property PropertyListItemRecord[] $propertyListItems
 * @property PropertyRangeRecord[] $propertyRanges
 * @property PropertyValueBigintRecord[] $propertyValueBigints
 * @property PropertyValueBooleanRecord[] $propertyValueBooleans
 * @property PropertyValueDateRecord[] $propertyValueDates
 * @property PropertyValueFloatRecord[] $propertyValueFloats
 * @property PropertyValueGeolocationRecord[] $propertyValueGeolocations
 * @property PropertyValueIntRecord[] $propertyValueInts
 * @property PropertyValueStringRecord[] $propertyValueStrings
 * @property PropertyValueTextRecord[] $propertyValueTexts
 * @property PropertyValueTimestampRecord[] $propertyValueTimestamps
 * @property PropertyVariantRecord[] $propertyVariants
 */
class PropertyRecord extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'property';
    }

    /**
     * @return null|object|\yii\db\Connection
     */
    public static function getDb()
    {
        return Yii::$app->get('dbSearch');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'sysname', 'property_type_id'], 'required'],
            [['description'], 'string'],
            [['property_unit_id', 'property_type_id', 'property_group_id', 'sort_order'], 'integer'],
            [['is_specific'], 'boolean'],
            [['name'], 'string', 'max' => 255],
            [['sysname'], 'string', 'max' => 50],
            [['name'], 'unique'],
            [['name'], 'unique'],
            [['sysname'], 'unique'],
            [['property_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => PropertyGroupRecord::className(), 'targetAttribute' => ['property_group_id' => 'id']],
            [['property_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => PropertyTypeRecord::className(), 'targetAttribute' => ['property_type_id' => 'id']],
            [['property_unit_id'], 'exist', 'skipOnError' => true, 'targetClass' => PropertyUnitRecord::className(), 'targetAttribute' => ['property_unit_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'sysname' => 'Sysname',
            'description' => 'Description',
            'property_unit_id' => 'Property Unit ID',
            'is_specific' => 'Is Specific',
            'property_type_id' => 'Property Type ID',
            'property_group_id' => 'Property Group ID',
            'sort_order' => 'Sort Order',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertyGroup()
    {
        return $this->hasOne(PropertyGroupRecord::className(), ['id' => 'property_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertyType()
    {
        return $this->hasOne(PropertyTypeRecord::className(), ['id' => 'property_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertyUnit()
    {
        return $this->hasOne(PropertyUnitRecord::className(), ['id' => 'property_unit_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty2elementClasses()
    {
        return $this->hasMany(Property2elementClassRecord::className(), ['property_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getElementClasses()
    {
        return $this->hasMany(ElementClassRecord::className(), ['id' => 'element_class_id'])->viaTable('property2element_class', ['property_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertyArrays()
    {
        return $this->hasMany(PropertyArrayRecord::className(), ['property_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertyListItems()
    {
        return $this->hasMany(PropertyListItemRecord::className(), ['property_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertyRanges()
    {
        return $this->hasMany(PropertyRangeRecord::className(), ['property_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertyValueBigints()
    {
        return $this->hasMany(PropertyValueBigintRecord::className(), ['property_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertyValueBooleans()
    {
        return $this->hasMany(PropertyValueBooleanRecord::className(), ['property_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertyValueDates()
    {
        return $this->hasMany(PropertyValueDateRecord::className(), ['property_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertyValueFloats()
    {
        return $this->hasMany(PropertyValueFloatRecord::className(), ['property_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertyValueGeolocations()
    {
        return $this->hasMany(PropertyValueGeolocationRecord::className(), ['property_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertyValueInts()
    {
        return $this->hasMany(PropertyValueIntRecord::className(), ['property_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertyValueStrings()
    {
        return $this->hasMany(PropertyValueStringRecord::className(), ['property_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertyValueTexts()
    {
        return $this->hasMany(PropertyValueTextRecord::className(), ['property_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertyValueTimestamps()
    {
        return $this->hasMany(PropertyValueTimestampRecord::className(), ['property_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertyVariants()
    {
        return $this->hasMany(PropertyVariantRecord::className(), ['property_id' => 'id']);
    }
}
