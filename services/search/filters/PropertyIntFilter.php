<?php

namespace commonprj\services\search\filters;

use yii\db\Query;

class PropertyIntFilter extends AbstractFilter
{

    protected $table = 'property_value_int';
    protected $field = 'value';

    protected $operator;
    protected $propertyId;
    protected $values;
    protected $classId;


    protected function renderQuery($operation, $values, $classId, $extendData = null)
    {
        $table = $this->table;
        $propertyId = $this->propertyId;

        $condition = [
            $operation,
            "$table." . $this->field,
        ];

        if ($operation == 'BETWEEN') {
            $condition[] = $values[0];
            $condition[] = $values[1];
        } else {
            $condition[] = $values;
        }

        $query = (new Query())
            ->select('e.element_id')
            ->from("{$table}")
            ->leftJoin('property_value2element e', "e.property_value_id = {$table}.id AND e.property_id = {$table}.property_id")
            ->leftJoin('element2element_class', 'element2element_class.element_id = e.element_id')
            ->where([
                'e.property_id'                          => $propertyId,
                'element2element_class.element_class_id' => $classId,
            ])
            ->andWhere($condition);

        return $query;
    }
}