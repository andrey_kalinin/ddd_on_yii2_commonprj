<?php

namespace commonprj\services;

use commonprj\components\core\entities\element\Element;
use commonprj\components\core\entities\elementClass\ElementClass;
use commonprj\components\core\entities\property\Property;
use commonprj\components\core\entities\propertyType\PropertyType;
use commonprj\components\core\helpers\ClassAndContextHelper;
use commonprj\components\core\models\PropertyRecord;
use Yii;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;


class PropertyService
{
    private $propertyClassNameMap;
    private $propertyMap;
    private $propertyTypeMap;

    /**
     * @param Element $element
     * @param string $propertySysname
     * @return int|null
     * @throws HttpException
     */
    public function getPropertyIdByElementAndSysname(Element $element, string $propertySysname)
    {
        $elementClasses = $element->getElementClasses();
        foreach ($elementClasses as $elementClassId => $_) {
            $propertyId = $this->getPropertyIdByElementClassIdAndSysname($elementClassId, $propertySysname);
            if ($propertyId) {
                return $propertyId;
            }
        }


        throw new NotFoundHttpException("Property Relation Not Found / propertySysname:{$propertySysname} " . __FILE__ . __LINE__);
    }

    /**
     * @param int $elementClassId
     * @param string $propertySysname
     * @return int|null
     */
    public function getPropertyIdByElementClassIdAndSysname(int $elementClassId, string $propertySysname)
    {
        $this->loadPropertyMap();

        return $this->propertyClassNameMap[$elementClassId][$propertySysname] ?? null;
    }

    public function getSysnameById($currentPropertyId)
    {
        $this->loadPropertyMap();

        foreach ($this->propertyMap as $propertySysname => $propertyId) {
            if ($currentPropertyId == $propertyId) {
                return $propertySysname;
            }
        }
        throw new NotFoundHttpException("Property Relation Not Found currentPropertyId:{$currentPropertyId} " . __FILE__ . __LINE__);
    }

    public function getIdBySysname($sysname)
    {
        $this->loadPropertyMap();

        return $this->propertyMap[$sysname] ?? null;
    }

    /**
     * Возвращает класс свойства по его типу
     * @param int $typeId
     * @return bool
     */
    public function getPropertyValueClassByTypeId(int $typeId)
    {
        $this->loadPropertyTypeMap();
        return $this->propertyTypeMap[$typeId] ?? false;
    }

    /**
     *
     */
    private function loadPropertyMap()
    {
        $this->loadPropertyTypeMap();
        $propertiesRecords = PropertyRecord::getDb()->cache(function ($db) {
            return PropertyRecord::find()->all();
        },3600);

        if (!$this->propertyMap) {
            $propertiesRecords = PropertyRecord::find()->all();

            foreach ((array)$propertiesRecords as $propertiesRecord) {
                $this->propertyMap[$propertiesRecord->sysname] = $propertiesRecord->id;
            }
        }

        if (!$this->propertyClassNameMap) {
            $propertiesWithClasses = Yii::$app->propertyRepository->getProperty2elementClass();
            //TODO: переделать на метод find WITH когда заработает WITH
            foreach ($propertiesWithClasses as $property) {
                $this->propertyClassNameMap[$property['element_class_id']][$property['propertySysname']] = $property['property_id'];
            }
        }

    }

    /**
     *
     */
    public function loadPropertyTypeMap()
    {
        if (!$this->propertyTypeMap) {

            $propertyTypes =  PropertyType::find()
                    ->translate(false)
                    ->where(['parent_id' => 1])
                    ->all();

            $elementClasses = ElementClass::find()
                    ->translate(false)
                    ->indexBy('id')
                    ->all();

            foreach ($propertyTypes as $propertyType) {
                $elementClass = $elementClasses[$propertyType->elementClassId];
                $this->propertyTypeMap[$propertyType->id] = ClassAndContextHelper::getFullClassNameByModelClass($elementClass->name);
            }

        }

    }
}




