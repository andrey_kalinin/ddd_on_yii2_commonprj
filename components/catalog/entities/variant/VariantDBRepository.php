<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 02.02.2018
 * Time: 13:07
 */

namespace commonprj\components\catalog\entities\variant;

use commonprj\components\catalog\entities\material\Material;
use commonprj\components\catalog\entities\option\Option;
use commonprj\components\catalog\entities\priceCategory\PriceCategory;
use commonprj\components\catalog\entities\productMaterial\ProductMaterial;
use commonprj\components\core\entities\abstractPropertyValue\AbstractPropertyValue;
use commonprj\components\core\entities\element\ElementDBRepository;
use commonprj\components\core\entities\property\Property;
use commonprj\extendedStdComponents\BaseCrudModel;
use commonprj\extendedStdComponents\BaseDBRepository;
use Yii;
use yii\web\ServerErrorHttpException;

/**
 * Class VariationDBRepository
 * @package commonprj\components\core\entities\variation
 */
class VariantDBRepository extends ElementDBRepository implements IVariantRepository
{
    /**
     * @param Variant $element
     * @param int $optionId
     * @return bool
     * @throws \yii\db\Exception
     * @throws \yii\web\HttpException
     */
    public function bindOption(Variant $element, int $optionId): bool
    {
        return $this->bindAssociatedEntity($element, Yii::$app->propertyService->getPropertyIdByElementAndSysname($element,
            Property::PROPERTY_SYSNAME__VARIANT2OPTION), $optionId);
    }

    /**
     * @param Variant $element
     * @param int $optionId
     * @return bool
     * @throws \yii\db\Exception
     * @throws \yii\web\HttpException
     */
    public function unbindOption(Variant $element, int $optionId): bool
    {
        return $this->unbindAssociatedEntity($element, Yii::$app->propertyService->getPropertyIdByElementAndSysname($element,
            Property::PROPERTY_SYSNAME__VARIANT2OPTION), $optionId);
    }

    /**
     * @param Variant $element
     * @return Option|null
     * @throws \yii\web\HttpException
     */
    public function getOption(Variant $element): ?Option
    {
        $item = $this->getAssociatedEntity($element, Yii::$app->propertyService->getPropertyIdByElementAndSysname($element,
            Property::PROPERTY_SYSNAME__VARIANT2OPTION));
        if ($item) {
            /** @var Option $result */
            $result = Option::findOne($item->id);
            return $result;
        }
        return null;
    }

    /**
     * @param BaseCrudModel $entity
     * @param int $entityId
     * @throws \Exception
     * @throws \commonprj\components\core\entities\element\NotFoundHttpException
     */
    public function populateRecord(BaseCrudModel $entity, $entityId)
    {
        parent::populateRecord($entity, $entityId);

        $option = $this->getOption($entity);

        if (!$option) {
            return;
        }

        switch (reset($option->typeOfOption)) {
            case 'PriceCategory':
                $entity->priceCategory = PriceCategory::findOne($entity->configuration->priceCategoryId);
                $entity->productMaterials = ProductMaterial::find()
                    ->where(['id' => $entity->configuration->productMaterialIds])->all();
                break;
            case 'Material':
                $entity->materials = Material::find()
                    ->where(['id' => $entity->configuration->materialIds])->all();
                break;
            case 'Property':
                    $propertyValueId = $entity->configuration->propertyValueId;
                    $propertyTypeId = $entity->getOption()->getProperty()->propertyTypeId;
                    /**
                     * @var AbstractPropertyValue $propertyValueClass
                     */
                    $propertyValueClass = Yii::$app->propertyService->getPropertyValueClassByTypeId($propertyTypeId);
                    $propertyValue = $propertyValueClass::findOne($propertyValueId);
                    $entity->propertyValue = $propertyValue;
                break;
            default:
                throw new \Exception("Не известный typeOfOption");
                break;
        }
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return array_merge(parent::attributes(), ['productMaterials', 'priceCategory', 'materials', 'propertyValue']);
    }
}