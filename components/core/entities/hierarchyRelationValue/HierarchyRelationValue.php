<?php
/**
 * @package         FurniPrice
 * @subpackage      Core
 * @category        DBRepository
 * @created         2017.12.20
 * @author          Vasiliy Konakov
 * @updated         2018.01.19
 * @updatedBy       Vasiliy Konakov
 */

namespace commonprj\components\core\entities\hierarchyRelationValue;

use yii;
use yii\db\Exception;
use commonprj\components\core\entities\relationValue\RelationValue;

/**
 * Class HierarchyRelationValue
 * @package commonprj\components\core\entities\hierarchyRelationValue
 */
class HierarchyRelationValue extends RelationValue
{

    /** @var HierarchyRelationValueDBRepository $repository */
    public $repository;

    /**
     * Присвоение свойству доменного слоя $repository - соответствующего компонента
     * @inheritdoc
     */
    public function __construct($config = [])
    {
        parent::__construct($config);
        $this->repository = Yii::$app->hierarchyRelationValueRepository;
    }

    /**
     * Save/update instance into storage
     * @return bool
     * @throws Exception
     */
    public function save(): bool
    {
        return $this->repository->save($this);
    }

    /**
     * Delete instance in storage
     * @return bool
     * @throws Exception
     */
    public function delete(): bool
    {
        return $this->repository->delete($this);
    }

}
