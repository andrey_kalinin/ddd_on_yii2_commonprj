<?php

namespace commonprj\components\core\entities\propertyValueInt;

use commonprj\components\core\entities\abstractPropertyValue\AbstractPropertyValueDBRepository;
use commonprj\components\core\models\PropertyValueIntRecord;

/**
 * Class IntPropertyDBRepository
 * @package commonprj\components\core\entities\intProperty
 */
class PropertyValueIntDBRepository extends AbstractPropertyValueDBRepository
{
    public $activeRecordClass = PropertyValueIntRecord::class;
}