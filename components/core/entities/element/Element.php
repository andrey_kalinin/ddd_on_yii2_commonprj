<?php

namespace commonprj\components\core\entities\element;

use commonprj\components\core\entities\elementClass\ElementClass;
use commonprj\components\core\entities\property\Property;
use commonprj\components\core\helpers\ClassAndContextHelper;
use commonprj\extendedStdComponents\BaseCrudModel;
use commonprj\extendedStdComponents\ElementQuery;
use Yii;
use yii\web\HttpException;


/**
 * Class Element
 * @property IElementRepository $repository
 * @package commonprj\components\core\entities\element
 */
class Element extends BaseCrudModel
{
    const ALLOWED_WHERE_PARAMS = [
        'id',
        'isActive',
    ];

    public $repository;
    public $id;
    public $isActive = 0;
    public $elementClasses;
    public $properties;
    public $parents;
    public $children;
    public $root;
    public $sortOrder = 1000;

    /**
     * Массив хранящий знание о том какие свойства кем являются (значение, массив или диппазон)
     * @var array
     */
    protected $propertyMultiplicitySysname = [];
    protected $propertyMultiplicity = [];

    /**
     * @var array|null old attribute values indexed by attribute names.
     * This is `null` if the record [[isNewRecord|is new]].
     */
    protected $_oldProperties;

    private $_elementClasses = [];
    private $_isParent = [];
    private $_associatedEntities = [];
    private $_haveSearchedAllAssociatedEntities = false;
    private $_children = [];
    private $_haveSearchedAllChildren = false;
    private $_parents = [];
    private $_haveSearchedAllParents = false;
    private $_properties = [];

    protected static $attributes = null;
    protected static $attributesProperties = [];
    protected static $attributesPropertiesClass = null;

    /**
     * Присвоение свойству доменного слоя $repository - соответствующего компонента
     * @inheritdoc
     */
    public function __construct($config = [])
    {
        parent::__construct($config);
        $this->repository = Yii::$app->elementRepository;
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function __get($name)
    {
        foreach ($this->properties as $property) {
            if (isset($property['sysname']) && $property['sysname'] == $name) {
                return $property['value'];
            }
        }

        return null;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['isActive'], 'boolean'],
        ];
    }

    // Extends Methods

    /**
     * Метод возвращает классы к которым принадлежит текущий элемент.
     * @return ElementClass[]
     * @throws HttpException
     */
    public function getElementClasses(): array
    {

        if (empty($this->_elementClasses)) {
            $this->_elementClasses = $this->repository->getElementClasses($this);
        }
        return $this->_elementClasses;
    }

    /**
     * Метод возвращает свойства текущего элемента.
     * @return Property[]
     * @throws HttpException
     */
    public function getProperties()
    {

        if (empty($this->properties)) {
            $this->properties = $this->repository->getProperties($this);
        }
        return $this->properties;
    }

    /**
     * @param $propertyId
     * @return \commonprj\components\core\entities\abstractPropertyValue\AbstractPropertyValue
     */
    public function getPropertyValue($propertyId)
    {

        if (!empty($this->_properties[$propertyId])) {
            return $this->_properties[$propertyId]->value;
        }
        return $this->repository->getPropertyValue($this, $propertyId);
    }

    #-------------------------------------------------------------------------------------------------------------------
    # /** BEGIN this part of code created by Vasiliy Konakov 2018.01.19 as a part of feature/FP-62 // @bklv */
    #-------------------------------------------------------------------------------------------------------------------

    /**
     * Find parent element related to basic element by property type "aggregation|hierarchy"
     * @param int $propertyId PropertyRecord id
     * @return null|Element (domain layer object)
     * @throws Exception
     */
    public function getParent(int $propertyId): Element
    {
        if (!empty($this->_parents[$propertyId])) {
            return reset($this->_parents[$propertyId]);
        }
        return $this->repository->getParent($this, $propertyId);
    }

    /**
     * Find all parent elements related to basic element by property type "aggregation"
     * @param int|null $propertyId [optional] PropertyRecord id
     * @return array Element[] (domain layer objects)
     * @throws HttpException
     */
    public function getParents(int $propertyId = null): array
    {

        if (empty($propertyId)) {

            if (!$this->_haveSearchedAllParents) {
                $this->_parents = $this->repository->getParents($this);
                $this->_haveSearchedAllParents = true;
            }
            return $this->_parents;
        } else {

            if (empty($this->_parents[$propertyId]) && !$this->_haveSearchedAllParents) {
                $this->_parents[$propertyId] = $this->repository->getParents($this, $propertyId);
            }
        }

        return $this->_parents[$propertyId];
    }


    /**
     * Find if there is s child element related to basic element by property type "aggregation|hierarchy"
     * @param int $propertyId PropertyRecord id
     * @return boolean SUCCESS (has a parent) OR FAIL (no parents)
     * @throws HttpException
     */
    public function getIsParent(int $propertyId): bool
    {

        if (empty($this->_isParent)) {
            $this->_isParent = $this->repository->getIsParent($this, $propertyId);
        }
        return $this->_isParent;
    }

    /**
     * Find all children elements related to basic element by property type "aggregation|hierarchy"
     * @param int|null $propertyId [optional] PropertyRecord id
     * @return array Element[] (domain layer objects)
     * @throws HttpException
     */
    public function getChildren(int $propertyId = NULL): array
    {

        if (empty($propertyId)) {

            if (!$this->_haveSearchedAllChildren) {
                $this->_children = $this->repository->getChildren($this);
                $this->_haveSearchedAllChildren = true;
            }
            return $this->_children;
        } else {

            if (empty($this->_children[$propertyId]) && !$this->_haveSearchedAllChildren) {
                $this->_children[$propertyId] = $this->repository->getChildren($this, $propertyId);
            }
        }

        return $this->_children[$propertyId];
    }

    /**
     * Find all associated elements related to basic element by property type "association"
     * @param int $propertyId PropertyRecord id
     * @return null|Element
     * @throws HttpException
     */
    public function getAssociatedEntity(int $propertyId): ?Element
    {
        if (!empty($this->_associatedEntities[$propertyId])) {
            return reset($this->_associatedEntities[$propertyId]);
        }
        return $this->repository->getAssociatedEntity($this, $propertyId);
    }

    /**
     * Find all associated elements related to basic element by property type "association"
     * @param int $propertyId [optional] ID of property
     * @return array Element[] (domain layer objects)
     * @throws HttpException
     */
    public function getAssociatedEntities(int $propertyId = NULL): array
    {

        if (empty($propertyId)) {

            if (!$this->_haveSearchedAllAssociatedEntities) {
                $this->_associatedEntities = $this->repository->getAssociatedEntities($this);
                $this->_haveSearchedAllAssociatedEntities = true;
            }
            return $this->_associatedEntities;
        } else {

            if (empty($this->_associatedEntities[$propertyId]) && !$this->_haveSearchedAllAssociatedEntities) {
                $this->_associatedEntities[$propertyId] = $this->repository->getAssociatedEntities($this, $propertyId);
            }
        }

        return $this->_associatedEntities[$propertyId];
    }

    /**
     * Binding a child to an element by relation property type "aggregation|hierarchy"
     * @param int $propertyId PropertyRecord id
     * @param int $childElementId child ElementRecord id
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException
     */
    public function bindChild(int $propertyId, int $childElementId): bool
    {
        $this->clearChildren();
        return $this->repository->bindChild($this, $propertyId, $childElementId);
    }

    /**
     * Binding an element to parent by relation property type "aggregation|hierarchy"
     * @param int $propertyId PropertyRecord id
     * @param int $parentElementId Parent ElementRecord id
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException|Exception
     */
    public function bindToParent(int $propertyId, int $parentElementId): bool
    {
        $this->clearParents();
        return $this->repository->bindToParent($this, $propertyId, $parentElementId);
    }

    /**
     * Binding children to an element by relation property type "aggregation|hierarchy"
     * @param int $propertyId
     * @param array $childElementIds
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException
     */
    public function bindChildren(int $propertyId, array $childElementIds): bool
    {
        $this->clearChildren();
        return $this->repository->bindChildren($this, $propertyId, $childElementIds);
    }

    /**
     * Binding a related element by relation property type "association"
     * @param int $propertyId PropertyRecord id
     * @param int $associatedElementId child ElementRecord id
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException|Exception
     */
    public function bindAssociatedEntity(int $propertyId, int $associatedElementId): bool
    {
        $this->clearAssociatedEntities();
        return $this->repository->bindAssociatedEntity($this, $propertyId, $associatedElementId);
    }

    /**
     * Binding several related elements by relation property type "association"
     * @param int $propertyId
     * @param array $associatedElementIds
     * @return boolean SUCCESS OR FAIL
     * @throws Exception|HttpException
     */
    public function bindAssociatedEntities(int $propertyId, array $associatedElementIds): bool
    {
        $this->clearAssociatedEntities();
        return $this->repository->bindAssociatedEntities($this, $propertyId, $associatedElementIds);
    }

    /**
     * @param int $propertyId
     * @param bool $isReturnFullObjects
     * @param bool $isReturnLinearArray
     * @return Element[]
     */
    public function getDescendants(int $propertyId, $isReturnFullObjects = false, $isReturnLinearArray = false): array
    {
        return $this->repository->getDescendants($this, $propertyId, $isReturnFullObjects, $isReturnLinearArray);
    }


    public function saveProperties(array $propertyValues)
    {
        return $this->repository->saveProperties($this, $propertyValues);
    }

    public function getPropertyMultiplicityData()
    {
        return array_merge($this->propertyMultiplicity, $this->propertyMultiplicitySysname);
    }

    /**
     * Cleans the corresponding temporary private variables
     * return void
     */
    private function clearAssociatedEntities(): void
    {
        $this->_associatedEntities = [];
        $this->_haveSearchedAllAssociatedEntities = false;
    }

    /**
     * Cleans the corresponding temporary private variables
     * return void
     */
    private function clearParents(): void
    {
        $this->_parents = [];
        $this->_haveSearchedAllParents = false;
    }

    /**
     * Cleans the corresponding temporary private variables
     * return void
     */
    private function clearChildren(): void
    {
        $this->_children = [];
        $this->_haveSearchedAllChildren = false;
    }


    /**
     * Delete relation between this and related CHILD element identified by property "aggregation|hierarchy"
     * @param int $propertyId PropertyRecord id
     * @param int $childElementId child ElementRecord id
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException
     */
    public function unbindChild(int $propertyId, int $childElementId): bool
    {
        $this->clearChildren();
        return $this->repository->unbindChild($this, $propertyId, $childElementId);
    }

    /**
     * Delete relations between this and ALL RELATED CHILD element identified by property "aggregation|hierarchy"
     * @param int $propertyId PropertyRecord id
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException
     */
    public function unbindAllChildren(int $propertyId): bool
    {
        $this->clearChildren();
        return $this->repository->unbindAllChildren($this, $propertyId);
    }


    /**
     * Delete relation between this and related element identified by property "association"
     * @param int $propertyId PropertyRecord id
     * @param int $associatedElementId child ElementRecord id
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException|Exception
     */
    public function unbindAssociatedEntity(int $propertyId, $associatedElementId): bool
    {
        $this->clearAssociatedEntities();
        return $this->repository->unbindAssociatedEntity($this, $propertyId, $associatedElementId);
    }

    /**
     * Delete relation between this and ALL RELATED ELEMENTS identified by property "association"
     * @param int $propertyId PropertyRecord id
     * @return boolean SUCCESS OR FAIL
     * @throws HttpException
     */
    public function unbindAllAssociatedEntities(int $propertyId): bool
    {
        $this->clearAssociatedEntities();
        return $this->repository->unbindAllAssociatedEntities($this, $propertyId);
    }

    public function propertiesAttributes()
    {
        $result = ClassAndContextHelper::getContextAndClassName(get_called_class());
        $className = $result['className'];
        if (!self::$attributesProperties || self::$attributesPropertiesClass != $className) {
            self::$attributesProperties = [];
            self::$attributesPropertiesClass = $className;
            $elementClassId = ClassAndContextHelper::getElementClassIdByClassAndContextName($className);
            $elementClass = ElementClass::findOne(['id' => $elementClassId]);

            $properties = $elementClass->getProperties();

            foreach ($properties as $property) {
                self::$attributesProperties[$property->id] = $property->sysname;
            }
        }

        return self::$attributesProperties;
    }

    public function fields()
    {
        return parent::attributes();
    }

    /**
     * @inheritdoc
     * @return ElementQuery
     */
    public static function find()
    {
        return Yii::createObject(ElementQuery::className(), [get_called_class()]);
    }

    public function setOldProperties($key, $property)
    {
        $this->_oldProperties[$key] = clone $property;
    }

    public function getOldProperties($key = null)
    {
        if ($key) {
            return $this->_oldProperties[$key] ?? null;
        } else {
            return $this->_oldProperties ?? [];
        }
    }

    public static function populateElementProperties($entity)
    {
        return $entity->repository->populateElementProperties($entity);
    }

    /**
     * Sets the attribute values in a massive way.
     * @param array $values attribute values (name => value) to be assigned to the model.
     * @param bool $safeOnly whether the assignments should only be done to the safe attributes.
     * A safe attribute is one that is associated with a validation rule in the current [[scenario]].
     * @see safeAttributes()
     * @see attributes()
     */
    public function setAttributes($values, $safeOnly = true)
    {
        return $this->repository->setAttributes($this, $values, $safeOnly);
    }

    /**
     * @param string $sysname
     * @return null
     */
    public function getPropertyFromPropertiesBySysname(string $sysname)
    {
        if (!$this->properties) {
            return null;
        }

        foreach ($this->properties as $property) {

            if (is_array($property)) {
                $property = (object)$property;
            }

            if ($property->sysname == $sysname) {
                return $property;
            }
        }
    }
}
