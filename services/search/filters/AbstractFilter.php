<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 3/1/2018
 * Time: 12:46 PM
 */

namespace commonprj\services\search\filters;


use commonprj\services\CurrencyService;
use Yii;
use yii\db\Query;

class AbstractFilter
{
    protected $propertyId;
    protected $table;
    protected $field;


    public function __construct($propertyId = null)
    {
        $this->propertyId = $propertyId;
    }

    public function getTable()
    {
        return $this->table;
    }

    public function getPropertyId()
    {
        return $this->propertyId;
    }

    public function getField()
    {
        return $this->field;
    }

    public function getQuery($operation, $values, $classId, $extendData = null)
    {
        $values = $this->beforeQueryForValues($values);

        return $this->renderQuery($operation, $values, $classId, $extendData);
    }

    public function beforeQueryForValues($values)
    {
        return $values;
    }

    protected function renderQuery($operation, $values, $classId, $extendData = null)
    {
        return new Query();
    }

    /**
     * @return CurrencyService
     */
    protected function getCurrencyService()
    {
        return Yii::$app->currency;
    }
}