<?php

namespace commonprj\components\core\models\search;

use Yii;

/**
 * This is the model class for table "property_variant".
 *
 * @property int $id
 * @property int $property_id
 * @property int $property_multiplicity_id
 * @property int $property_value_id
 *
 * @property ElementCategoryRecord[] $elementCategories
 * @property PropertyValidationRuleRecord[] $propertyValidationRules
 * @property PropertyRecord $property
 * @property TemplateRecord[] $templates
 */
class PropertyVariantRecord extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'property_variant';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbSearch');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['property_id', 'property_multiplicity_id', 'property_value_id'], 'required'],
            [['property_id', 'property_multiplicity_id', 'property_value_id'], 'default', 'value' => null],
            [['property_id', 'property_multiplicity_id', 'property_value_id'], 'integer'],
            [['property_id'], 'exist', 'skipOnError' => true, 'targetClass' => PropertyRecord::className(), 'targetAttribute' => ['property_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'property_id' => 'Property ID',
            'property_multiplicity_id' => 'Property Multiplicity ID',
            'property_value_id' => 'Property Value ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getElementCategories()
    {
        return $this->hasMany(ElementCategoryRecord::className(), ['property_variant_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertyValidationRules()
    {
        return $this->hasMany(PropertyValidationRuleRecord::className(), ['property_variant_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty()
    {
        return $this->hasOne(PropertyRecord::className(), ['id' => 'property_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemplates()
    {
        return $this->hasMany(TemplateRecord::className(), ['property_variant_id' => 'id']);
    }
}
