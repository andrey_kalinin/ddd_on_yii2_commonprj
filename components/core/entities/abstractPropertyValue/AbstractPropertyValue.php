<?php
/**
 * @package         FurniPrice
 * @subpackage      Core
 * @category        DBRepository
 * @created         17.06.2016
 * @author          daSilva.Rodrigues
 * @refactoredBy    Vasiliy Konakov
 * @updated         13.12.2017
 */

namespace commonprj\components\core\entities\abstractPropertyValue;

use commonprj\extendedStdComponents\BaseCrudModel;
use commonprj\extendedStdComponents\BaseDBRepository;
use commonprj\components\core\models\ElementRecord;
use commonprj\components\core\models\PropertyRecord;
use yii\db\ActiveRecord;
use yii\web\HttpException;

/**
 * Class AbstractPropertyValue
 * @property IAbstractPropertyValueRepository $repository
 * @package commonprj\components\core\entities\abstractPropertyValue
 */
abstract class AbstractPropertyValue extends BaseCrudModel
{
    const MULTIPLICITY_SCALAR = 1;
    const MULTIPLICITY_RANGE = 2;
    const MULTIPLICITY_ARRAY = 3;

    /**
     * @var integer $id Real type bigint! "*_property_value" table primary key
     */
    public $id;
    public $elementId;
    public $propertyId;
    public $value;
    public $multiplicityId = self::MULTIPLICITY_SCALAR;
    public $isRelation = false;

    protected $currentClassName;

    /**
     * AbstractPropertyValue constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->repository = \Yii::$app->abstractPropertyValueRepository;
        $this->currentClassName = static::class;
    }

    /**
     * @param ActiveRecord $propertyRecord
     * @return array
     */
    public function getValues(ActiveRecord $propertyRecord): array
    {
        $result = [];

        foreach ($propertyRecord->getPropertyValuesQuery()->all() as $propertyValueRecord) {
            $result[] = (new $this->currentClassName())->getValue($propertyValueRecord);
        }

        return $result;
    }

    /**
     * @param ActiveRecord $propertyValueRecord
     * @return mixed
     */
    public function getValue(ActiveRecord $propertyValueRecord)
    {
        return $this->repository->getValue($this, $propertyValueRecord);
    }

    /**
     * Active record style validation for not-active record Model
     * @return array
     */
    public function rules(): array
    {
        return [
            [['id', 'element_id', 'property_id', 'value'], 'required'],
            [['id', 'element_id', 'property_id'], 'integer'],
            [['element_id'], 'exist', 'skipOnError' => true, 'targetClass' => ElementRecord::className(), 'targetAttribute' => ['element_id' => 'id']],
            [['property_id'], 'exist', 'skipOnError' => true, 'targetClass' => PropertyRecord::className(), 'targetAttribute' => ['property_id' => 'id']],
        ];
    }

    /**
     * @return bool
     */
    public function isNew()
    {
        return empty($this->id);
    }
}
