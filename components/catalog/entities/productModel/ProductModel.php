<?php
/**
 * @package         FurniPrice
 * @subpackage      Catalog
 * @category        Entity
 * @created         2018.01.22
 * @author          Vasiliy Konakov
 * @updated         2018.01.23
 * @updatedBy       Vasiliy Konakov
 */

namespace commonprj\components\catalog\entities\productModel;

use commonprj\components\catalog\entities\component\Component;
use commonprj\components\catalog\entities\material\Material;
use commonprj\components\catalog\entities\materialCollection\MaterialCollection;
use commonprj\components\catalog\entities\product\Product;
use commonprj\components\core\entities\abstractPropertyValue\AbstractPropertyValue;
use commonprj\components\core\entities\element\Element;
use commonprj\components\catalog\entities\variation\Variant;
use commonprj\components\crm\entities\manufacturer\Manufacturer;
use commonprj\extendedStdComponents\BaseCrudModel;
use Yii;
use yii\db\Exception;
use yii\web\HttpException;

/**
 * Class ProductModel
 * @package commonprj\components\core\entities\productModel
 */
class ProductModel extends Element
{
    /**
     * @var ProductModelDBRepository $repository
     */
    public $repository;

    /**
     * @var string $name
     */
    public $name;

    protected $propertyMultiplicitySysname = [
        'image' => AbstractPropertyValue::MULTIPLICITY_ARRAY,
    ];


    /**
     * Присвоение свойству доменного слоя $repository - соответствующего компонента
     * @inheritdoc
     */
    public function __construct(array $config = [])
    {
//        parent::__construct($config);
        $this->repository = Yii::$app->productModelRepository;
    }

    /**
     * bindManufacturer
     * @param int $manufacturerId
     * @return bool
     * @throws HttpException|Exception
     */
    public function bindManufacturer(int $manufacturerId): bool
    {
        return $this->repository->bindManufacturer($this, $manufacturerId);
    }

    /**
     * unbindManufacturer
     * @param int $manufacturerId
     * @return bool
     * @throws HttpException|Exception
     */
    public function unbindManufacturer(int $manufacturerId): bool
    {
        return $this->repository->unbindManufacturer($this, $manufacturerId);
    }

    /**
     * getManufacturer
     * @return null|Manufacturer
     * @throws HttpException|Exception
     */
    public function getManufacturer(): ?Manufacturer
    {
        return $this->repository->getManufacturer($this);
    }

    /**
     * getSellers
     * @param array $condition
     * @return array
     * @throws Exception
     * @throws HttpException
     */
    public function getSellers(array $condition = []): array
    {
        return $this->repository->getSellers($this, $condition);
    }

    /**
     * bindMaterialCollection
     * @param int $materialCollectionId
     * @return bool
     * @throws HttpException|Exception
     */
    public function bindProductMaterial(int $productMaterialId): bool
    {
        return $this->repository->bindProductMaterial($this, $productMaterialId);
    }

    /**
     * unbindProductMaterial
     * @param int $productMaterialId
     * @return bool
     * @throws HttpException|Exception
     */
    public function unbindProductMaterial(int $productMaterialId): bool
    {
        return $this->repository->unbindProductMaterial($this, $productMaterialId);
    }

    /**
     * getProductMaterials
     * @return MaterialCollection[]
     * @throws HttpException|Exception
     */
    public function getProductMaterials(): array
    {
        return $this->repository->getProductMaterials($this);
    }

    /**
     * bindMaterial
     * @param int $materialId
     * @return bool
     * @throws HttpException|Exception
     */
    public function bindMaterial(int $materialId): bool
    {
        return $this->repository->bindMaterial($this, $materialId);
    }

    /**
     * unbindMaterial
     * @param int $materialId
     * @return bool
     * @throws HttpException|Exception
     */
    public function unbindMaterial(int $materialId): bool
    {
        return $this->repository->unbindMaterial($this, $materialId);
    }

    /**
     * getMaterials
     * @return Material[]
     * @throws HttpException|Exception
     */
    public function getMaterials(): array
    {
        return $this->repository->getMaterials($this);
    }

    /**
     * bindConstructionElement
     * @param int $constructionElementId
     * @return bool
     * @throws HttpException|Exception
     */
    public function bindConstructionElement(int $constructionElementId): bool
    {
        return $this->repository->bindConstructionElement($this, $constructionElementId);
    }

    /**
     * unbindConstructionElement
     * @param int $constructionElementId
     * @return bool
     * @throws HttpException|Exception
     */
    public function unbindConstructionElement(int $constructionElementId): bool
    {
        return $this->repository->unbindConstructionElement($this, $constructionElementId);
    }

    /**
     * getConstructionElements
     * @return Component[]
     * @throws HttpException|Exception
     */
    public function getConstructionElements(): array
    {
        return $this->repository->getConstructionElements($this);
    }

    /**
     * bindModularComponent
     * @param int $modularComponentId
     * @return bool
     * @throws HttpException|Exception
     */
    public function bindModularComponent(int $modularComponentId): bool
    {
        return $this->repository->bindModularComponent($this, $modularComponentId);
    }

    /**
     * unbindModularComponent
     * @param int $modularComponentId
     * @return bool
     * @throws HttpException|Exception
     */
    public function unbindModularComponent(int $modularComponentId): bool
    {
        return $this->repository->unbindModularComponent($this, $modularComponentId);
    }

    /**
     * getModularComponents
     * @return Component[]
     * @throws HttpException|Exception
     */
    public function getModularComponents(): array
    {
        return $this->repository->getModularComponents($this);
    }

    /**
     * getProducts
     * @return Product[]
     * @throws HttpException|Exception
     */
    public function getProducts(): array
    {
        return $this->repository->getProducts($this);
    }

}
