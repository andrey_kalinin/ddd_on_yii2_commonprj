<?php

namespace commonprj\components\catalog\entities\productMaterial;

use commonprj\components\catalog\entities\material\Material;
use commonprj\components\catalog\entities\materialCollection\MaterialCollection;
use commonprj\components\catalog\entities\materialGroup\MaterialGroup;
use commonprj\components\catalog\entities\priceCategory\PriceCategory;
use commonprj\components\crm\entities\manufacturer\Manufacturer;

interface IProductMaterialRepository
{

    /**
     * @param ProductMaterial $productMaterial
     * @param int $manufacturerId
     * @return boolean
     */
    public function bindManufacturer(ProductMaterial $productMaterial, int $manufacturerId): bool;

    /**
     * @param ProductMaterial $productMaterial
     * @param int $manufacturerId
     * @return boolean
     */
    public function unbindManufacturer(ProductMaterial $productMaterial, int $manufacturerId): bool;

    /**
     * @param ProductMaterial $productMaterial
     * @return Manufacturer
     */
    public function getManufacturer(ProductMaterial $productMaterial): Manufacturer;

    /**
     *
     * @param ProductMaterial $productMaterial
     * @param int $materialId
     * @return boolean
     */
    public function bindMaterial(ProductMaterial $productMaterial, int $materialId): bool;

    /**
     *
     * @param ProductMaterial $productMaterial
     * @param int $materialId
     * @return boolean
     */
    public function unbindMaterial(ProductMaterial $productMaterial, int $materialId): bool;

    /**
     * @param ProductMaterial $productMaterial
     * @return Material
     */
    public function getMaterial(ProductMaterial $productMaterial): Material;

    /**
     *
     * @param ProductMaterial $productMaterial
     * @param int $materialCollectionId
     * @return boolean
     */
    public function bindMaterialCollection(ProductMaterial $productMaterial, int $materialCollectionId): bool;

    /**
     *
     * @param ProductMaterial $productMaterial
     * @param int $materialCollectionId
     * @return boolean
     */
    public function unbindMaterialCollection(ProductMaterial $productMaterial, int $materialCollectionId): bool;

    /**
     *
     * @param ProductMaterial $productMaterial
     * @return MaterialCollection
     */
    public function getMaterialCollection(ProductMaterial $productMaterial): MaterialCollection;

    /**
     *
     * @param ProductMaterial $productMaterial
     * @param int $priceCategoryId
     * @return boolean
     */
    public function bindPriceCategory(ProductMaterial $productMaterial, int $priceCategoryId): bool;

    /**
     *
     * @param ProductMaterial $productMaterial
     * @param int $priceCategoryId
     * @return boolean
     */
    public function unbindPriceCategory(ProductMaterial $productMaterial, int $priceCategoryId): bool;

    /**
     *
     * @param ProductMaterial $productMaterial
     * @return PriceCategory
     */
    public function getPriceCategory(ProductMaterial $productMaterial): PriceCategory;

    /**
     * @param ProductMaterial $productMaterial
     * @return MaterialGroup
     */
    public function getMaterialGroup(ProductMaterial $productMaterial): MaterialGroup;

    /**
     * @param \commonprj\components\catalog\entities\productMaterial\ProductMaterial $productMaterial
     * @param int $materialGroupId
     * @return bool
     */
    public function bindMaterialGroup(ProductMaterial $productMaterial, int $materialGroupId): bool;

    /**
     * @param \commonprj\components\catalog\entities\productMaterial\ProductMaterial $productMaterial
     * @param int $materialGroupId
     * @return bool
     */
    public function unbindMaterialGroup(ProductMaterial $productMaterial, int $materialGroupId): bool;
}
