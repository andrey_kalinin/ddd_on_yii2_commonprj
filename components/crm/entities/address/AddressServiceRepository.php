<?php

namespace commonprj\components\crm\entities\Address;

use commonprj\components\core\entities\element\ElementServiceRepository;
use Yii;
use yii\httpclient\Client;

/**
 * Class AddressServiceRepository
 * @package commonprj\components\crm\entities\Address */
class AddressServiceRepository extends ElementServiceRepository
{
    /**
     * @var Client
     */
    protected $restServer;

    /**
     * @var string
     */
    protected $baseUri = 'address/';

    /**
     * AddressServiceRepository constructor.
     */
    public function __construct()
    {
        $this->restServer = Yii::$app->crmRestServer;
    }

}