<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 22.03.2018
 * Time: 13:04
 */

namespace commonprj\components\catalog\entities\option;

use commonprj\components\catalog\entities\materialGroup\MaterialGroup;
use commonprj\components\catalog\entities\variant\Variant;
use commonprj\components\core\entities\element\ElementDBRepository;
use commonprj\components\core\entities\property\Property;
use commonprj\extendedStdComponents\BaseCrudModel;
use Yii;
use yii\web\NotFoundHttpException;

class OptionDBRepository extends ElementDBRepository implements IOptionRepository
{
    /**
     * @param Option $element
     * @return Property
     * @throws NotFoundHttpException
     */
    public function getProperty(Option $element): ?Property
    {
        if (!$element->propertyId) {
            return null;
            throw new NotFoundHttpException("Не установлен атрибут propertyId OptionId:{$element->id}", 404);
        }

        return Property::findOne($element->propertyId);
    }

    /**
     * @param Option $element
     * @return array
     * @throws \yii\web\HttpException
     */
    public function getVariants(Option $element): array
    {
        $result = [];
        foreach ($this->getAssociatedEntities($element,
            Yii::$app->propertyService->getPropertyIdByElementAndSysname($element,
                Property::PROPERTY_SYSNAME__VARIANT2OPTION)) as $item) {
            $result[$item->id] = Variant::findOne($item->id);
        }
        return $result;
    }

    /**
     * @param BaseCrudModel $entity
     * @param array|int $entityId
     * @throws \commonprj\components\core\entities\element\NotFoundHttpException
     */
    public function populateRecord(BaseCrudModel $entity, $entityId)
    {
        parent::populateRecord($entity, $entityId);
        $entity->propertyId = $entity->getPropertyFromPropertiesBySysname('propertyId')->value ?? null;
        $entity->property = $entity->getProperty();
        $entity->typeOfOption = $entity->getPropertyFromPropertiesBySysname('typeOfOption')->value ?? null;

        $materialGroupProperty = $entity->getPropertyFromPropertiesBySysname('materialGroupId');
        $entity->materialGroup = (is_null($materialGroupProperty) ? null : MaterialGroup::findOne($materialGroupProperty->value));
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return array_merge(parent::attributes(), ['propertyId', 'property', 'typeOfOption', 'materialGroup']);
    }

}