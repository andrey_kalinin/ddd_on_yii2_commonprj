<?php
/**
 * Created by daSilva.Rodrigues
 * Date: 22.08.2016
 */

namespace commonprj\components\core\entities\propertyValueBoolean;

use commonprj\components\core\entities\abstractPropertyValue\AbstractPropertyValue;
use yii\db\ActiveRecord;

/**
 * Class BooleanProperty
 * @package commonprj\components\core\entities\booleanProperty
 */
class PropertyValueBoolean extends AbstractPropertyValue
{
    public function __construct(array $config = [], ActiveRecord $activeRecord = null)
    {
        parent::__construct($config);
        $this->repository = \Yii::$app->propertyValueBooleanRepository;
    }

    /**
     * @return bool
     */
    public function save(): bool
    {
        $this->value = filter_var($this->value, FILTER_VALIDATE_BOOLEAN);
        return parent::save();
    }


}