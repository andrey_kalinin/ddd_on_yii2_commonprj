<?php

namespace commonprj\components\core\entities\propertyValueListItem;

use commonprj\components\core\entities\abstractPropertyValue\AbstractPropertyValue;
use commonprj\components\core\entities\abstractPropertyValue\AbstractPropertyValueDBRepository;
use commonprj\components\core\models\PropertyListItemRecord;
use commonprj\components\core\models\PropertyValueListItemRecord;
use commonprj\extendedStdComponents\IBaseCrudModel;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\db\Query;

/**
 * Class ListItemPropertyDBRepository
 * @package commonprj\components\core\entities\listItemProperty
 */
class PropertyValueListItemDBRepository extends AbstractPropertyValueDBRepository
{
    /**
     * @var ActiveRecord::class
     */
    public $activeRecordClass = PropertyValueListItemRecord::class;

    /**
     * @param AbstractPropertyValue $propertyValue
     * @param ActiveRecord $propertyRecord
     * @return AbstractPropertyValue
     */
    public function setValue(AbstractPropertyValue $propertyValue, ActiveRecord $propertyRecord): AbstractPropertyValue
    {
        $listItemProperty = $propertyRecord->getPropertyListItem()->one();

        $propertyValueListItem = parent::setValue($propertyValue, $propertyRecord);
        $propertyValueListItem->label = $listItemProperty->getAttribute('label');
        $propertyValueListItem->value = $listItemProperty->getAttribute('value');
        $propertyValueListItem->multiplicityId = AbstractPropertyValue::MULTIPLICITY_SCALAR;

        return $propertyValueListItem;
    }

    /**
     * Возвращает данные для UNION запроса для получения свойств и значений элемента
     * @param $elementId
     * @return string
     */
    public function getValueQueryBuilder(int $elementId)
    {
        $table = $this->getListItemTable();

        $queryBuilder = (new Query())
            ->select([
                'pv.id',
                'value' => "cast({$table}.item as text)",
                'pv.property_id',
                'pv.element_id',
                'prop_class' => new Expression("'" . $this->activeRecordClass . "'"),
            ])
            ->from(['pv' => $this->activeRecordClass::tableName()])
            ->leftJoin([$table], "{$table}.id = pv.value")
            ->where(['element_id' => $elementId]);

        $queryBuilder->addSelect(['searchdb_property_value_id' => new Expression(0)]);
        return $queryBuilder;
    }

    /**
     * @param AbstractPropertyValue $propertyValue
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function save(IBaseCrudModel $propertyValue, $runValidation = true, $attributes = null): bool
    {
        $params = [
            'item'        => $propertyValue->value,
            'property_id' => $propertyValue->propertyId,
        ];

        $listItemClass = $this->getListItemClass();
        $propertyListItem = $listItemClass::find()->where($params)->one();

        if (!$propertyListItem) {
            throw new \Exception(sprintf('List Item Property {id:%s} not found', $propertyValue->value));
        }

        $searchParams = [
            'property_id' => $propertyValue->propertyId,
            'value'       => $propertyListItem->id,
            'element_id'  => $propertyValue->elementId,
        ];
        $propertyValue->value = $propertyListItem->id;

        $existingValue = $propertyValue::find()
            ->where($searchParams)
            ->one(\Yii::$app->getDb());

        if ($existingValue) {
            $propertyValue->id = $existingValue->id;
            return true;
        }

        return parent::save($propertyValue);
    }

    /**
     * @param IBaseCrudModel $propertyValue
     * @return bool
     * @throws \Exception
     */
    public function delete(IBaseCrudModel $propertyValue): bool
    {
        $params = [
            'item'        => $propertyValue->value,
            'property_id' => $propertyValue->propertyId,
        ];

        $listItemClass = $this->getListItemClass();
        $propertyListItem = $listItemClass::find()->where($params)->one();

        if (!$propertyListItem) {
            throw new \Exception(sprintf('List Item Property {id:%s} not found', $propertyValue->value));
        }

        $searchParams = [
            'property_id' => $propertyValue->propertyId,
            'value'       => $propertyListItem->id,
            'element_id'  => $propertyValue->elementId,
        ];
        $propertyValue->value = $propertyListItem->id;

        $existingValue = $propertyValue::find()
            ->where($searchParams)
            ->one(\Yii::$app->getDb());

        if ($existingValue) {
            return parent::delete($existingValue);
        }

        throw new \Exception(sprintf('List Item Value Property {id:%s} not found', $propertyValue->value));
    }


    protected function getListItemClass()
    {
        return PropertyListItemRecord::class;
    }

    protected function getListItemTable()
    {
        $listItemClass = $this->getListItemClass();

        return $listItemClass::tableName();
    }

    public function getListItemByParams($params)
    {
        $listItemClass = $this->getListItemClass();

        return $listItemClass::find()->where($params)->one(\Yii::$app->getDb());
    }
}
