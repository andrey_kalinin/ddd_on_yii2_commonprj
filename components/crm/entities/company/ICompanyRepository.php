<?php

namespace commonprj\components\crm\entities\company;

use commonprj\components\crm\entities\companyRole\CompanyRole;
use commonprj\components\crm\entities\companyState\CompanyState;

interface ICompanyRepository
{

    /**
     * @param Company $company
     * @param int $addressId
     * @return bool
     */
    public function bindAddress(Company $company, int $addressId): bool;

    /**
     * @param Company $company
     * @param int $addressId
     * @return bool
     */
    public function unbindAddress(Company $company, int $addressId): bool;

    /**
     * @param Company $company
     * @param int|null $typeId
     * @return array
     */
    public function getAddresses(Company $company, int $typeId = null): array;

    /**
     * @param Company $company
     * @param int $companyRoleId
     * @return boolean
     */
    public function bindCompanyRole(Company $company, int $companyRoleId): bool;

    /**
     * @param Company $company
     * @param int $companyRoleId
     * @return bool
     */
    public function unbindCompanyRole(Company $company, int $companyRoleId): bool;

    /**
     * @param Company $company
     * @return CompanyRole
     */
    public function getCompanyRole(Company $company): CompanyRole;

    /**
     * @param Company $company
     * @param int $companyStateId
     * @return boolean
     */
    public function bindCompanyState(Company $company, int $companyStateId): bool;

    /**
     * @param Company $company
     * @param int $companyStateId
     * @return bool
     */
    public function unbindCompanyState(Company $company, int $companyStateId): bool;

    /**
     * @param Company $company
     * @return CompanyState
     */
    public function getCompanyState(Company $company): CompanyState;

    /**
     * @param Company $company
     * @return Employee[]
     */
    public function getEmployees(Company $company): array;

    /**
     * @param Company $company
     * @param int $employeeId
     * @return boolean
     */
    public function bindEmployee(Company $company, int $employeeId): bool;

    /**
     * @param Company $company
     * @param int $employeeId
     * @return boolean
     */
    public function unbindEmployee(Company $company, int $employeeId): bool;


}
