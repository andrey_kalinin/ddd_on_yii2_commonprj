<?php

namespace commonprj\components\core\entities\element;

use commonprj\components\core\entities\elementClass\ElementClass;
use commonprj\components\core\entities\elementType\ElementType;
use commonprj\components\core\entities\model\Model;
use commonprj\components\core\entities\property\Property;
use commonprj\components\core\entities\relationClass\RelationClass;
use commonprj\components\core\entities\relationGroup\RelationGroup;
use commonprj\extendedStdComponents\BaseCrudModel;
use commonprj\extendedStdComponents\BaseServiceRepository;

/**
 * Class ElementServiceRepository
 * @package commonprj\components\core\entities\element
 */
class ElementServiceRepository extends BaseServiceRepository
{
    public function populateRecord(BaseCrudModel $entity, $data)
    {
        parent::populateRecord($entity, $data);



        $this->populateElementProperties($entity);

    }

    public function populateElementProperties(Element $entity)
    {

        foreach ((array)$entity->properties as $propertyData) {
            $property = Property::instantiate();
            Property::populateRecord($property, $propertyData);
            $properties[] = $property;
        }

        $entity->properties = $properties ?? [];
    }

}