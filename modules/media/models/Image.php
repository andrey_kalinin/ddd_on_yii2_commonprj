<?php

namespace commonprj\modules\media\models;

use Yii;
use yii\base\Model;
use yii\httpclient\Client;

/**
 * Image is the model behind the upload form.
 */
class Image extends Model
{

    /**
     * @var UploadedFile file attribute
     */
    public $file;
    public $mediaServiceUrl;
    public $recognizeColors;
    public $maxSize;
    public $extensions;

    /**
     * @return array the validation rules.
     */
    public function rules() {
        return [
            [['file'], 'file', 'extensions' => $this->extensions, 'maxSize' => $this->maxSize],
            ['recognizeColors', 'integer', 'skipOnEmpty' => true, 'integerOnly' => true, 'min' => 3, 'max' => 20],
        ];
    }

    public function upload($params) {
        if ($this->validate()) {
            $client = new Client([
                'baseUrl' => $this->mediaServiceUrl . (!empty($params) ? '?' . http_build_query($params) : ''),
            ]);

            $response = $client->createRequest()
                    ->setMethod('post')
                    ->addFile('image', $this->file->tempName)
                    ->send();

            if ($response->isOk) {
                return $response->data;
            } else {
                return ['status' => 'error', 'message' => 'fail response'];
            }
        } else {
            return $this->errors;
        }
        return ['status' => 'error', 'message' => 'unknown error'];
    }

    public function delete($id) {
        $data = false;
        if ($this->validate()) {
            $client = new Client([
                'baseUrl' => $this->mediaServiceUrl . '/' . $id,
            ]);

            $response = $client->createRequest()
                    ->setMethod('delete')
                    ->send();

            if ($response->isOk) {
                $data = $response->data;
            }
        }
        return $data;
    }

}
