<?php

namespace commonprj\components\core\entities\propertyValueFloat;

use commonprj\components\core\entities\abstractPropertyValue\AbstractPropertyValue;
use yii\db\ActiveRecord;

/**
 * Class FloatProperty
 * @package commonprj\components\core\entities\propertyValueFloat
 */
class PropertyValueFloat extends AbstractPropertyValue
{
    public function __construct(array $config = [], ActiveRecord $activeRecord = null)
    {
        parent::__construct($config);
        $this->repository = \Yii::$app->propertyValueFloatRepository;
    }
}