<?php
/**
 * Created by daSilva.Rodrigues
 * Date: 02.08.2016
 */

namespace commonprj\extendedStdComponents;

use commonprj\components\core\helpers\ClassAndContextHelper;
use commonprj\components\core\models\PropertyTypeRecord;
use commonprj\services\LocalizationService;
use Yii;
use yii\data\Sort;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\BaseInflector;
use yii\helpers\StringHelper;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\Request;
use commonprj\components\catalog\entities as CatalogEntities;
use commonprj\components\core\entities as CoreEntities;
use commonprj\components\crm\entities as CrmEntities;
use commonprj\components\core\models;

/**
 * Class BaseDBRepository
 * @package commonprj\extendedStdComponents
 */
class BaseDBRepository implements IBaseRepository, IBaseBDRepository
{
    const ENTITY_TO_RECORD = [
        // Core

        CoreEntities\element\Element::class                                   => models\ElementRecord::class,
        CoreEntities\elementCategory\ElementCategory::class                   => models\ElementCategoryRecord::class,
        CoreEntities\elementClass\ElementClass::class                         => models\ElementClassRecord::class,
        CoreEntities\property\Property::class                                 => models\PropertyRecord::class,
        CoreEntities\propertyVariant\PropertyVariant::class                   => models\PropertyVariantRecord::class,
        CoreEntities\propertyValueBoolean\PropertyValueBoolean::class         => models\PropertyValueBooleanRecord::class,
        CoreEntities\propertyValueDate\PropertyValueDate::class               => models\PropertyValueDateRecord::class,
        CoreEntities\propertyValueFloat\PropertyValueFloat::class             => models\PropertyValueFloatRecord::class,
        CoreEntities\propertyValueInt\PropertyValueInt::class                 => models\PropertyValueIntRecord::class,
        CoreEntities\propertyValueGeolocation\PropertyValueGeolocation::class => models\PropertyValueGeolocationRecord::class,
        CoreEntities\propertyValueListItem\PropertyValueListItem::class       => models\PropertyValueListItemRecord::class,
        CoreEntities\propertyValueString\PropertyValueString::class           => models\PropertyValueStringRecord::class,
        CoreEntities\propertyValueText\PropertyValueText::class               => models\PropertyValueTextRecord::class,
        CoreEntities\propertyValueTimeStamp\PropertyValueTimeStamp::class     => models\PropertyValueTimestampRecord::class,
        CoreEntities\propertyValueColor\PropertyValueColor::class             => models\PropertyValueColorRecord::class,
        CoreEntities\propertyValueJson\PropertyValueJson::class               => models\PropertyValueJsonRecord::class,
        CoreEntities\propertyRangeValue\PropertyRangeValue::class             => models\PropertyRangeRecord::class,
        CoreEntities\propertyArrayValue\PropertyArrayValue::class             => models\PropertyArrayRecord::class,
        CoreEntities\propertyGroup\PropertyGroup::class                       => models\PropertyGroupRecord::class,
        CoreEntities\propertyUnit\PropertyUnit::class                         => models\PropertyUnitRecord::class,
        CoreEntities\propertyType\PropertyType::class                         => PropertyTypeRecord::class,

        // Catalog
        CatalogEntities\material\Material::class                              => models\ElementRecord::class,
        CatalogEntities\productMaterial\ProductMaterial::class                => models\ElementRecord::class,
        CatalogEntities\materialCollection\MaterialCollection::class          => models\ElementRecord::class,
        CatalogEntities\priceCategory\PriceCategory::class                    => models\ElementRecord::class,
        CatalogEntities\productModel\ProductModel::class                      => models\ElementRecord::class,

        // Crm
        CrmEntities\address\Address::class                                    => models\ElementRecord::class,
        CrmEntities\seller\Seller::class                                      => models\ElementRecord::class,
        CrmEntities\manufacturer\Manufacturer::class                          => models\ElementRecord::class,
    ];

    /**
     * @var ActiveRecord
     */
    public $activeRecordClass;

    /**
     * Метод конвертирует названия ключей пришедшего массива из uppercase в lowercase
     * @param array $attributes
     * @return array
     */
    public static function arrayKeysUpper2Lower(array $attributes, $excludeKeys = []): array
    {
        $result = [];

        foreach ($attributes as $key => $attribute) {
            if (!in_array($key, $excludeKeys)) {
                $result[strtolower($key)] = $attribute;
            } else {
                $result[$key] = $attribute;
            }
        }

        return $result;
    }

    /**
     * @param $entity
     * @param $values
     * @param bool $safeOnly
     */
    public function setAttributes($entity, $values, $safeOnly = true)
    {
        foreach ($entity->attributes() as $attribute) {
            $newValue = $values[$attribute] ?? null;
            if ($newValue !== null && $entity->getOldAttributes($attribute) !== $newValue) {
                $entity->{$attribute} = $newValue;
            }
        }
    }

    /**
     * @param IBaseCrudModel $entity
     * @param bool $runValidation
     * @param null $attributes
     * @return bool
     */
    public function save(IBaseCrudModel $entity, $runValidation = true, $attributes = null): bool
    {
        if ($entity->isNewRecord()) {
            return $this->insert($entity, $runValidation, $attributes);
        }

        return $this->updateInternal($entity, $attributes) !== false;
    }

    /**
     * @param IBaseCrudModel $entity
     * @param bool $runValidation
     * @param null $attributes
     * @return bool
     */
    public function update(IBaseCrudModel $entity, $runValidation = true, $attributes = null): bool
    {
        return $this->save($entity, $runValidation, $attributes);
    }


    /**
     * @param IBaseCrudModel $entity
     * @param bool $runValidation
     * @param null $attributes
     * @return bool
     */
    public function insert(IBaseCrudModel $entity, $runValidation = true, $attributes = null): bool
    {
        /** @var ActiveRecord $record */
        $record = new $this->activeRecordClass();

        foreach ($record->attributes() as $attribute) {
            if ($attribute == 'id') {
                continue;
            }
            $record->setAttribute($attribute, $entity->{lcfirst(BaseInflector::camelize($attribute))});
        }

        $result = $record->save();

        if ($result) {
            $entity->id = $record->id;
        } else {
            $entity->addErrors($record->getErrors());
        }

        if ($record->hasAttribute('searchdb_property_value_id')) {
            $entity->searchdbPropertyValueId = $record->searchdb_property_value_id;
        }

        return $result;
    }

    /**
     * @param $entity
     * @param null $names
     * @param array $except
     * @return array
     */
    public function getAttributes($entity, $names = null, $except = [])
    {
        $values = [];

        foreach ($this->attributes() as $attribute) {
            $values[$attribute] = $entity->{$attribute};
        }

        return $values;
    }

    /**
     * @return array
     */
    public function attributes()
    {
        $values = [];
        $record = new $this->activeRecordClass();

        foreach ($record->attributes() as $attribute) {
            $values[] = lcfirst(BaseInflector::camelize($attribute));
        }

        return $values;
    }

    /**
     * @param IBaseCrudModel $entity
     * @param null $attributes
     * @return bool
     */
    protected function updateInternal(IBaseCrudModel $entity, $attributes = null)
    {
        /** @var ActiveRecord $record */
        $record = $this->activeRecordClass::findOne($entity->id);
        foreach ($record->attributes() as $attribute) {
            if ($attribute == 'id') {
                continue;
            }
            $record->setAttribute($attribute, $entity->{lcfirst(BaseInflector::camelize($attribute))});
        }

        $result = $record->save();

        if ($result) {
            $entity->id = $record->id;
        } else {
            $entity->addErrors($record->getErrors());
        }

        return $result;
    }

    /**
     * @param IBaseCrudModel $entity
     * @return bool
     */
    public function delete(IBaseCrudModel $entity): bool
    {
        $record = $this->activeRecordClass::findOne($entity->id);

        return $record->delete();
    }

    /**
     * @inheritdoc
     * @return EntityQuery
     */
    public function find(IBaseCrudModel $entity)
    {
        return Yii::createObject(EntityQuery::className(), [get_class($entity)]);
    }

    /**
     * @inheritdoc
     * @return static|null BaseCrudModel instance matching the condition, or `null` if nothing matches.
     */
    public function findOne(IBaseCrudModel $entity, $condition)
    {
        return $this->findByCondition($entity, $condition)->one();
    }


    /**
     * @inheritdoc
     * @return static[] an array of BaseCrudModel instances, or an empty array if nothing matches.
     */
    public function findAll(IBaseCrudModel $entity, $condition)
    {
        return $this->findByCondition($entity, $condition)->all();
    }


    /**
     * Finds BaseCrudModel instance(s) by the given condition.
     * This method is internally called by [[findOne()]] and [[findAll()]].
     * @param mixed $condition please refer to [[findOne()]] for the explanation of this parameter
     * @return ActiveQueryInterface the newly created [[ActiveQueryInterface|ActiveQuery]] instance.
     * @throws InvalidConfigException if there is no primary key defined
     * @internal
     */
    protected static function findByCondition(IBaseCrudModel $entity, $condition)
    {
        $query = $entity::find();

        if ($condition && !ArrayHelper::isAssociative($condition)) {
            // query by primary key
            $primaryKey = $entity::primaryKey();

            if (isset($primaryKey[0])) {
                $condition = [$primaryKey[0] => $condition];
            } else {
                throw new InvalidConfigException('"' . get_called_class() . '" must have a primary key.');
            }
        }
        return $query->andWhere($condition);
    }


//
//    /**
//     * @param array|null $condition
//     * @return array
//     * @throws HttpException
//     * @throws \Exception
//     */
//    public function find(array $condition = null)
//    {
//        // TODO: Требуется рефакторинг
//        $parentClass = $this->getParentClass(static::class);
//
//        $ucFirst = ucfirst((str_replace('DBRepository', '', StringHelper::basename($parentClass))));
//        $classNameRecord = (new $parentClass)->activeRecordClass;
//
////        if (!empty($condition['byClassId'])) {
////            $idsByClass = $this->idsByClassId($condition['byClassId']);
////            $ids = [];
////
////            if (empty($idsByClass)) {
////                return [];
////            } else {
////                foreach ($idsByClass as $id) {
////                    $currentEntity = lcFirst($ucFirst);
////                    $ids[] = $id["{$currentEntity}_id"];
////                }
////
////                $condition['condition']['id'] = $ids;
////                $result = [];
////                unset($condition['byClassId']);
////                $entities = $this->find($condition);
////
////                foreach ($entities as $entity) {
////                    $result[$entity['id']] = $entity;
////                }
////
////                return $result;
////            }
////        }
//
//        /** @var ActiveQuery $query */
//        $query = call_user_func("{$classNameRecord}::find");
//        $table = $classNameRecord::tableName();
//
//        if (!empty($condition['byClassId'])) {
//            $query
//                ->select("{$table}.*")
//                ->leftJoin('element2element_class e2ec', "e2ec.element_id = {$table}.id")
//                ->where(['e2ec.element_class_id' => $condition['byClassId']]);
//        }
//
//        // ORDER BY field
//        if (isset($condition['orderBy'])) {
//            $query->orderBy($condition['orderBy']);
//        }
//
//        // ORDER BY PROPERTY VALUE
//        if (isset($condition['sort'])) {
//            $sort = $condition['sort'];
//            $orderBy = sprintf('%s.value %s', $sort['table'], $sort['direction']);
//            $query
//                ->leftJoin("{$sort['table']}", "{$sort['table']}.element_id = {$table}.id")
//                ->orderBy($orderBy);
//
//            // для случая с сортировкой условия в для WHERE в формате таблица.колонка
//            if (isset($condition['condition'])) {
//                $newCondition = [];
//                foreach ($condition['condition'] as $key => $value) {
//                    $newCondition["{$table}.$key"] = $value;
//                }
//                $condition['condition'] = $newCondition;
//            }
//        }
//
//        // WHERE
//        if (isset($condition['condition']) && ArrayHelper::isAssociative($condition['condition'])) {
//            $query = $query->andWhere($condition['condition']);
//        }
//
//
//        if (isset($condition['pagination'])) {
//            $limit = $condition['pagination']['limit'] ?? 50;
//            $offset = $condition['pagination']['offset'] ?? 0;
//            $query = $query->offset($offset)->limit($limit);
//        }
//
//        $needSpecialRelation = null;
//
////        if (!empty($params['with'])) {
////            list($params, $needSpecialRelation, $query) = $this->checkAndSetWithParams($params, $request, $query);
////        }
//
//        $records = $query->all();
//        $result = [];
//
//        if (!empty($condition['groupBy'])) {
//            foreach ($records as $record) {
//                $groupVar = BaseInflector::underscore($condition['groupBy']);
//                $key = $record->$groupVar;
//
//                if ($key === null) {
//                    $key = 'null';
//                } elseif ($key === false) {
//                    $key = 0;
//                }
//
//                $entity = self::instantiateByARAndClassName($record, null, $needSpecialRelation);
//
//                if (!empty($needSpecialRelation)) {
//                    self::checkSpecialRelations($needSpecialRelation, $entity);
//                }
//
//                $result["{$key}"][] = $entity;
//            }
//        } else {
//            foreach ($records as $record) {
//
//                if (!isset($entityInstance)) {
//                    $entityClassName = $this->getEntityClassByAR($record);
//                    $entityInstance = new $entityClassName;
//                }
//
//                if (method_exists(new $entityInstance, 'populate')) {
//                    $entity = $entityClassName::populate($record);
//                } else {
//                    $entity = self::instantiateByARAndClassName($record, null, $needSpecialRelation);
//                }
//
//                if (!empty($needSpecialRelation)) {
//                    self::checkSpecialRelations($needSpecialRelation, $entity);
//                }
//
//                $result["{$record->id}"] = $entity;
//            }
//        }
//
//        return $result;
//    }

    /**
     * Находит родительский класс который можно инстанциировать
     * @param string $class
     * @return string
     */
    private function getParentClass($class)
    {
        $parentClass = get_parent_class($class);

        if (!$parentClass || $parentClass == 'commonprj\extendedStdComponents\BaseDBRepository') {
            $parentClass = static::class;
        }

        $testClass = new \ReflectionClass($parentClass);

        return $testClass->isAbstract() ? $this->getParentClass($parentClass) : $parentClass;
    }

    /**
     * @param $params
     * @param $query
     * @return ActiveQuery
     */
    private function checkSortParams($params, $query): ActiveQuery
    {
        $sortParams = explode(',', $params['sort']);

        foreach ($sortParams as &$sortParam) {
            if (strncmp($sortParam, '-', 1) === 0) {
                $sortParam = substr($sortParam, 1);
            }

            if (!empty(static::SORT_PARAMS[$sortParam])) {
                $resultSortParams[$sortParam] = static::SORT_PARAMS[$sortParam];
            }
        }

        if (!empty($resultSortParams)) {
            $sort = new Sort([
                'attributes'      => $resultSortParams,
                'enableMultiSort' => 1,
            ]);

            /** @var ActiveQuery $query */
            $query = $query->orderBy($sort->orders);

            return $query;
        }

        return $query;
    }

    /**
     * @param $params
     * @param $request
     * @param $query
     * @return array
     */
    private function checkAndSetWithParams($params, $request, ActiveQuery $query): array
    {
        if (strpos($params['with'], 'propertyValues') !== false) {
            if (strpos($request->url, '/common/property') !== false) {
                $params['with'] = preg_replace('/propertyValues,|,propertyValues|propertyValues/', '', $params['with']);

                if (!empty($params['with'])) {
                    $params['with'] .= ',';
                }

                $params['with'] .= 'bigintPropertyValues,booleanPropertyValues,datePropertyValues,floatPropertyValues,geolocationPropertyValues,intPropertyValues,listItemPropertyValues,stringPropertyValues,textPropertyValues,timestampPropertyValues';
            } else {
                if (strpos($params['with'], 'properties.propertyValues') !== false) {
                    $params['with'] = preg_replace('/properties\.propertyValues/', '', $params['with']);

                    if (!empty($params['with'])) {
                        $params['with'] .= ',';
                    }

                    $params['with'] .= 'properties.bigintPropertyValues,properties.booleanPropertyValues,properties.datePropertyValues,properties.floatPropertyValues,properties.geolocationPropertyValues,properties.intPropertyValues,properties.listItemPropertyValues,properties.stringPropertyValues,properties.textPropertyValues,properties.timestampPropertyValues';
                }
            }
        }

        $needSpecialRelation['propertyValue'] = strpos($params['with'], 'properties.propertyValue') !== false;
        $needSpecialRelation['root'] = strpos($params['with'], 'root') !== false;

        if (!empty($needSpecialRelation['propertyValue'])) {
            $params['with'] = preg_replace('/\.propertyValue/', '', $params['with']);
        }

        if (!empty($needSpecialRelation['root'])) {
            $params['with'] = preg_replace('/root/', '', $params['with']);
        }

        if (!empty($params['with'])) {
            $query = $query->with(explode(',', $params['with']));
        }

        return [$params, $needSpecialRelation, $query];
    }

    /**
     * Фабрика для создания объекта доменной модели.
     * @param        $activeRecord - Данные из БД, которые будут мапиться в объект доменного слоя.
     * @param string $fullClassName - Имя класса доменной модели или ее DB репозитория (включая namespace), который
     *                              нужно инстанциировать.
     * @return BaseCrudModel|ActiveRecord - Возвращает объект доменного слоя или ActiveRecord.
     * @throws HttpException
     */
    public static function instantiateByARAndClassName(
        $activeRecord,
        string $fullClassName = null,
        $needSpecialRelation = null
    )
    {
        $instanceofActiveRecord = $activeRecord instanceof ActiveRecord;

        if (is_null($fullClassName)) {
            if (!$instanceofActiveRecord) {
                throw new HttpException(500, basename(__FILE__, '.php') . __LINE__);
            }

            $entityClassName = array_keys(self::ENTITY_TO_RECORD, get_class($activeRecord));

            if (empty($entityClassName) || count($entityClassName) > 1) {
                /** @var ElementRecord $activeRecord */
                try {
                    $allClasses = $activeRecord->getElementClasses()->all();
                } catch (\Exception $e) {
                    //todo в последствии логировать
                    return $activeRecord;
                }

                if (count($allClasses) === 1) {
                    $entityClassName[0] = ClassAndContextHelper::getFullClassNameByModelClass($allClasses[0]->getAttribute('name'));
                } else {
                    //todo сделать учет для нескольких классов
                    $contextName = preg_replace('/\\\\\w+/', '', $allClasses[0]->getAttribute('name'));
                    $className = ucfirst(preg_replace('/\w+\\\\/', '', $allClasses[0]->getAttribute('name')));
                    $entityClassName[0] = "commonprj\\components\\core\\entities\{$contextName}\\" . lcfirst($className) . "\\$className";
                }
            }

            $entityClassName = $entityClassName[0];


        } else {
            $entityClassName = preg_replace('/DBRepository|ServiceRepository/', '', $fullClassName);
        }

        if (!class_exists($entityClassName)) {
            return $activeRecord;
        }

        /** @var BaseCrudModel $resultElement */
        $resultElement = new $entityClassName();

        // Пока не перевели все свойства в camelcase, потом убрать за ненадобностью.
        if ($instanceofActiveRecord) {
            $resultElement->setAttributes(self::arrayKeysUnderscore2CamelCase($activeRecord->getAttributes()), false);

            /** @var ActiveRecord $activeRecord */
            if (!empty($activeRecord->getRelatedRecords())) {
                foreach ($activeRecord->getRelatedRecords() as $propertyName => $relatedRecordArray) {
                    $resultArray = [];
                    if (is_array($relatedRecordArray)) {
                        foreach ($relatedRecordArray as $relatedRecord) {

                            $entity = self::instantiateByARAndClassName($relatedRecord, null, $needSpecialRelation);

                            if (!empty($needSpecialRelation)) {
                                self::checkSpecialRelations($needSpecialRelation, $entity);
                            }

                            $resultArray[$relatedRecord['id']] = $entity;
                        }

                        $resultElement->{$propertyName} = $resultArray;
                    } else {
                        if ($relatedRecordArray instanceof ActiveRecord) {
                            $entity = self::instantiateByARAndClassName($relatedRecordArray, null,
                                $needSpecialRelation);

                            if (!is_null($needSpecialRelation)) {
                                self::checkSpecialRelations($needSpecialRelation, $entity);
                            }

                            $resultElement->$propertyName = $entity;
                        } else {
                            $resultElement->$propertyName = [];
                        }
                    }
                }
            }
            /** @var ActiveRecord $activeRecord */
            if ($activeRecord->hasErrors()) {
                $resultElement->addErrors($activeRecord->getErrors());
            }
        } else {
            // Елси вместо activeRecord был передан массив из camelCase свойств указанного в fullClassName класса
            $resultElement->setAttributes(self::arrayKeysUnderscore2CamelCase($activeRecord), false);
        }

        $resultElement->entity = $entityClassName;

        if (!empty($needSpecialRelation)) {
            self::checkSpecialRelations($needSpecialRelation, $resultElement);
        }

        return $resultElement;
    }


    /**
     * @param $activeRecord
     * @return models\ElementRecord
     * @throws HttpException
     */
    private static function getEntityClassNameByAR($activeRecord)
    {
        $instanceofActiveRecord = $activeRecord instanceof ActiveRecord;

        if (!$instanceofActiveRecord) {
            throw new HttpException(500, basename(__FILE__, '.php') . __LINE__);
        }

        $entityClassName = array_keys(self::ENTITY_TO_RECORD, get_class($activeRecord));

        if (empty($entityClassName) || count($entityClassName) > 1) {
            /** @var models\ElementRecord $activeRecord */
            try {
                $allClasses = $activeRecord->getElementClasses()->all();
            } catch (\Exception $e) {
                //todo в последствии логировать
                return $activeRecord;
            }

            if (count($allClasses) === 1) {
                $entityClassName[0] = ClassAndContextHelper::getFullClassNameByModelClass($allClasses[0]->getAttribute('name'));
            } else {
                //todo сделать учет для нескольких классов
                $contextName = preg_replace('/\\\\\w+/', '', $allClasses[0]->getAttribute('name'));
                $className = ucfirst(preg_replace('/\w+\\\\/', '', $allClasses[0]->getAttribute('name')));
                $entityClassName[0] = "commonprj\\components\\core\\entities\{$contextName}\\" . lcfirst($className) . "\\$className";
            }
        }

        return $entityClassName[0];
    }

    /**
     * Метод конвертирует названия ключей пришедшего массива из underscore в camelCase
     * @param array|object $attributes
     * @return array
     */
    public static function arrayKeysUnderscore2CamelCase(array $attributes, $excludeKeys = []): array
    {
        $result = [];

        foreach ($attributes as $key => $attribute) {
            if (!in_array($key, $excludeKeys)) {
                $result[lcfirst(BaseInflector::camelize($key))] = $attribute;
            } else {
                $result[$key] = $attribute;
            }
        }

        return $result;
    }

    /**
     * Общий для ядра метод для поиска записи по primary key. Так же возможен поиск по дополнительным условиям.
     * @param int|string|array $condition - Условия посика. Должен содерать primary key, остальные уловия опциональны.
     *                                    Если true - вернет элемент только если он принадлежит обратившемуся по api
     *                                    классу.
     * @return BaseCrudModel - Возвращает объект доменного слоя.
     * @throws HttpException
     */
//    public function findOne($condition)
//    {
//        if (!class_exists($this->activeRecordClass)) {
//            throw new HttpException(500, basename(__FILE__, '.php') . __LINE__);
//        }
//
//        $request = Yii::$app->getRequest();
//        $params = $request instanceof Request ? $request->getQueryParams() : [];
//        /** @var ActiveQuery $entity */
//        $entity = ($this->activeRecordClass)::find();
//
//        if (!empty($condition['condition'])) {
//            $entity = $entity->where($condition['condition']);
//        }
//
//        $needSpecialRelation = null;
//
//        if (!empty($params['with'])) {
//            list(, $needSpecialRelation, $entity) = $this->checkAndSetWithParams($params, $request, $entity);
//        }
//
//        $entity = $entity->one();
//
//        if (!$entity) {
//            throw new HttpException(404, basename(__FILE__, '.php') . __LINE__);
//        }
//
//
//        $calledEntity = str_replace('DBRepository', '', get_called_class());
//
//
//        if (new $calledEntity() instanceof CoreEntities\element\Element) {
//            $condition['byClass'] = true;
//        }
//
//        if (!empty($condition['byClass'])) {
//            $className = ClassAndContextHelper::getContextAndClassName(get_called_class())['className'];
//
//            if ($className != 'core\ElementDBRepository') {
//                $classId = ClassAndContextHelper::getClassId(get_called_class());
//
//                // фильтруем все классы к которым принадлежит данный элемент
//                $elementClassRecordArray = $entity->getElementClasses()->all();
//
//                // определяем id обратившегося по апи класса
//                /* проверяем есть ли среди всех классов к которым принадлежит данный элемент - обратившийся класс, если да
//                то возвращаем результат, в противном случае говорим что ничего не нашли*/
//                foreach ($elementClassRecordArray as $elementClassRecord) {
//                    if ($elementClassRecord['id'] == $classId) {
//
//                        $entity = self::instantiateByARAndClassName($entity, null, $needSpecialRelation);
//
//                        if (!empty($needSpecialRelation)) {
//                            $this->checkSpecialRelations($needSpecialRelation, $entity);
//                        }
//
//                        return $entity;
//                    }
//                }
//
//                throw new HttpException(404, basename(__FILE__, '.php') . __LINE__);
//            }
//        }
//
//        if (!isset($entityInstance) && $entity instanceof ActiveRecord) {
//            $entityClassName = $this->getEntityClassByAR($entity);
//            $entityInstance = new $entityClassName;
//        }
//
//        if (method_exists(new $entityInstance, 'populate')) {
//            $entity = $entityClassName::populate($entity);
//        } else {
//            $entity = self::instantiateByARAndClassName($entity, null, $needSpecialRelation);
//        }
//
//        if (!empty($needSpecialRelation)) {
//            $this->checkSpecialRelations($needSpecialRelation, $entity);
//        }
//
//        return $entity;
//    }

    /**
     * @param $httpCondition
     * @return BaseCrudModel
     * @throws HttpException
     * @throws NotFoundHttpException
     */
    public function findModel($httpCondition)
    {
        $httpConditionModelClass = $httpCondition['modelClass'];

        $activeRecord = self::ENTITY_TO_RECORD[$httpConditionModelClass] ?? null;

        if (!class_exists($activeRecord)) {
            throw new HttpException(500, basename(__FILE__, '.php') . __LINE__);
        }
        /** @var BaseCrudModel $whereParamsModel */
        $whereParamsModel = new $activeRecord();
        $queryParams = Yii::$app->getRequest()->getQueryParams();
        $whereParamsModel->setAttributes(self::arrayKeysCamelCase2Underscore($queryParams), false);
        $condition = [];

        if (!empty($queryParams['with'])) {
            $condition['with'] = $queryParams['with'];
        }

        foreach ($whereParamsModel as $key => $value) {
            if (isset($value) && in_array(lcfirst(BaseInflector::camelize($key)), array_keys($queryParams))) {
                $condition['condition'][$key] = $value;
            }
        }

        if (!$whereParamsModel->validate($queryParams)) {
            return $whereParamsModel;
        }
        /** @var BaseCrudModel $modelClass */
        $modelClass = new $httpConditionModelClass();
        $keys = $this->primaryKey();

        if (count($keys) > 1) {
            $values = explode(',', $httpCondition['condition']['id']);

            if (count($keys) === count($values)) {
                $condition['condition']['id'] = array_combine($keys, $values);
            }
        }

        $model = $modelClass->findOne($condition);

        if (isset($model)) {
            return $model;
        } else {
            throw new NotFoundHttpException("Object not found: {$httpCondition['condition']['id']}");
        }
    }

    /**
     * Метод конвертирует названия ключей пришедшего массива из camelCase в underscore
     * @param array $attributes
     * @return array
     */
    public static function arrayKeysCamelCase2Underscore(array $attributes, $excludeKeys = []): array
    {
        $result = [];

        foreach ($attributes as $key => $attribute) {
            if (!in_array($key, $excludeKeys)) {
                $result[BaseInflector::underscore($key)] = $attribute;
            } else {
                $result[$key] = $attribute;
            }
        }

        return $result;
    }

    /**
     * @return \yii\db\Connection
     */
    public static function getBD()
    {
        return Yii::$app->getDb();
    }

    /**
     * @param string|array $value
     * @param int $propertyId
     * @return array
     * @throws NotFoundHttpException
     */
    protected function generateTranslationList($value, int $propertyId = null)
    {
        /** @var LocalizationService $localizationService */
        $localizationService = $this->getLocalizationService();
        //получаем переводы данного значения с учетом свойства
        return $localizationService->generateTranslationList($value, $propertyId);
    }

    /**
     * @param string $sourceString
     * @param array $translations
     * @param int $propertyValueIdInSearchDB
     * @param int|null $propertyId
     */
    protected function addTranslations(string $sourceString,
                                       array $translations,
                                       int $oldPropertyValueIdInSearchDB = null,
                                       int $propertyValueIdInSearchDB,
                                       int $propertyId = null)
    {
        /** @var LocalizationService $localizationService */
        $localizationService = $this->getLocalizationService();

        $source = $localizationService->getSourceBySourceString($sourceString);

        //если нашли источник, добавляем переводы
        if ($source) {
            if ($oldPropertyValueIdInSearchDB) {
                $localizationService->saveLocalizationVariants($source, $translations, $oldPropertyValueIdInSearchDB, $propertyValueIdInSearchDB, $propertyId);
            } else {
                $localizationService->addLocalizationVariants($source, $translations, $propertyValueIdInSearchDB, $propertyId);
            }
        }

    }

    /**
     * @return LocalizationService
     */
    protected function getLocalizationService()
    {
        return Yii::$app->localization;
    }

    /**
     * @param BaseCrudModel $entity
     * @param int|array $data
     * @throws NotFoundHttpException
     */
    public function populateRecord(BaseCrudModel $entity, $data)
    {
        if ($data instanceof ActiveRecord) {
            $entityRecord = $data;
        } else {
            $activeRecordClass = $this->activeRecordClass;
            $entityRecord = $activeRecordClass::findOne($data);
        }

        if (!$entityRecord) {
            throw new NotFoundHttpException("Entity id:$data not found.");
        }

        foreach ($entityRecord->getAttributes() as $field => $value) {
            $field = lcfirst(BaseInflector::camelize($field));

            $entity->setOldAttribute($field, $value);
            $entity->{$field} = $value;
        }

    }

    public static function translateRecord(BaseCrudModel $entity)
    {
        /** @var LocalizationService $localizationService */
        $localizationService = Yii::$app->localization;;
        $localizationService->translateEntity($entity);
    }

    /**
     * переводит сущность
     * @param BaseCrudModel $entity
     */
    protected function translate(BaseCrudModel $entity)
    {
        /** @var LocalizationService $localizationService */
        $localizationService = $this->getLocalizationService();
        $localizationService->translateEntity($entity);
    }

    /**
     * @param array|null $condition
     * @return int|string
     */
    public function count(array $condition = null)
    {
        return $this->activeRecordClass::find()->where($condition['condition'])->count();
    }

    private function getEntityClassByAR($record)
    {
        return array_keys(self::ENTITY_TO_RECORD, get_class($record))[0];
    }

    /**
     * @param $table
     * @param $elements
     * @param string $field
     * @return array
     */
    protected function buildTreeFromArray($table, $elements, $field = 'children')
    {
        $roots = [];
        foreach ($table as $item) {
            $element = $elements[$item['element']];

            if ($item['parent'] == 0) {
                $roots[$element->id] = $element;
            } else {
                $parent = $elements[$item['parent']];
                $parent->{$field} = array_merge($parent->{$field} ?? [], [$element]);
            }
        }

        return $roots;
    }

    /**
     * Возвращает список переводимых полей в формате $field => $data
     * @param BaseCrudModel $entity
     * @return null
     */
    public function getTranslatableFields(BaseCrudModel $entity)
    {
        return null;
    }

    /**
     * Возвращает source в зависимотси от переданный параметров
     * @param null $data
     * @return null
     */
    public function getTranslatableSource($data = null)
    {
        return null;
    }

    /**
     * возвращает ID сущности, которую необходимо перевести
     * @param BaseCrudModel $entity
     * @param null $data
     * @return mixed
     */
    public function getTranslatableRecordSetId(BaseCrudModel $entity, $data = null)
    {
        return $entity->id;
    }

    /**
     * Получает ID свойства из параметров, для не элементов должен быть null
     * @param null $data
     * @return null
     */
    public function getTranslatablePropertyId($data = null)
    {
        return null;
    }

    /**
     * Возвращает массив переводов
     * @param BaseCrudModel $entity
     * @return mixed
     */
    public function getTranslations(BaseCrudModel $entity)
    {
        $localizationService = $this->getLocalizationService();
        $translatableProperties = $this->getTranslatableFields($entity);

        if ($translatableProperties) {

            foreach ($translatableProperties as $key => $params) {
                $source = $this->getTranslatableSource($params);
                $recordSetId = $this->getTranslatableRecordSetId($entity, $params);

                if ($source) {
                    if ($recordSetId) {
                        $translations = $localizationService->getTranslatedVariants(
                            $source,
                            $recordSetId,
                            $this->getTranslatablePropertyId($params)
                        );

                    } else {
                        foreach ($localizationService->getExistingLangs() as $language) {
                            $translations[$language] = '';
                        }
                    }

                    $result[$key] = $translations ?? [];
                }
            }
        }

        return $result ?? [];
    }
}
