<?php

namespace commonprj\components\catalog\entities\materialCollection;

use commonprj\components\crm\entities\manufacturer\Manufacturer;

interface IMaterialCollectionRepository
{

    /**
     * @param MaterialCollection $materialCollection
     * @param int $manufacturerId
     * @return boolean
     */
    public function bindManufacturer(MaterialCollection $materialCollection, int $manufacturerId): bool;

    /**
     * @param MaterialCollection $materialCollection
     * @param int $manufacturerId
     * @return boolean
     */
    public function unbindManufacturer(MaterialCollection $materialCollection, int $manufacturerId): bool;

    /**
     * @param MaterialCollection $materialCollection
     * @return Manufacturer
     */
    public function getManufacturer(MaterialCollection $materialCollection): Manufacturer;
}
