<?php

namespace commonprj\components\core\models\i18n;

use Yii;

/**
 * This is the model class for table "source".
 *
 * @property integer $id
 * @property string $db_name
 * @property string $table_name
 * @property string $field_name
 * @property boolean $is_string
 *
 * @property StringEng[] $stringEngs
 * @property StringRus[] $stringRuses
 * @property TextEng[] $textEngs
 * @property TextRus[] $textRuses
 */
class SourceRecord extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'source';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbI18n');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['db_name', 'table_name', 'field_name'], 'required'],
            [['is_string'], 'boolean'],
            [['db_name', 'table_name', 'field_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'db_name' => 'Db Name',
            'table_name' => 'Table Name',
            'field_name' => 'Field Name',
            'is_string' => 'Is String',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getString()
    {
        return $this->hasMany(StringRecord::className(), ['source_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getText()
    {
        return $this->hasMany(TextRecord::className(), ['source_id' => 'id']);
    }

}
