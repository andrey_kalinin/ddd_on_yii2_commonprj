<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 09.02.2018
 * Time: 18:49
 */

namespace commonprj\components\core\entities\propertyGroup;

use commonprj\components\core\entities\property\Property;
use commonprj\components\core\models\PropertyGroupRecord;
use commonprj\extendedStdComponents\BaseDBRepository;
use commonprj\extendedStdComponents\IBaseCrudModel;
use yii\web\HttpException;

/**
 * Class PropertyGroupDBRepository
 * @package commonprj\components\core\entities\propertyGroup
 */
class PropertyGroupDBRepository extends BaseDBRepository implements IPropertyGroupRepository
{
    /**
     * @var string
     */
    public $activeRecordClass = PropertyGroupRecord::class;

    /**
     * @param PropertyGroup $propertyGroup
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function save(IBaseCrudModel $propertyGroup, $runValidation = true, $attributes = null): bool
    {
        if (!$propertyGroupRecord = PropertyGroupRecord::findOne($propertyGroup->id)) {
            $propertyGroupRecord = new PropertyGroupRecord();
        }
        $propertyGroupRecord->setAttributes(self::arrayKeysCamelCase2Underscore($propertyGroup->attributes));

        $result = $propertyGroupRecord->save();

        if ($result) {
            $propertyGroup->setAttributes(self::arrayKeysUnderscore2CamelCase($propertyGroupRecord->attributes), false);
        }

        return $result;
    }

    /**
     * @param PropertyGroup $propertyGroup
     * @return bool
     * @throws HttpException
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function delete(IBaseCrudModel $propertyGroup): bool
    {
        $propertyGroupRecord = PropertyGroupRecord::findOne($propertyGroup->id);

        if (!$propertyGroupRecord) {
            throw new HttpException(404, basename(__FILE__, '.php') . __LINE__);
        }

        if ($propertyGroupRecord->getProperties()->one()) {
            throw new \LogicException('Не могу удалить PropertyGroup который используется');
        }

        return $propertyGroupRecord->delete();
    }

    /**
     * @return string[]
     */
    public static function primaryKey()
    {
        return PropertyGroupRecord::primaryKey();
    }

    /**
     * @param PropertyGroup $propertyGroup
     * @return Property[]
     * @throws HttpException
     */
    public function getProperties(PropertyGroup $propertyGroup): array
    {
        /**
         * @var PropertyGroupRecord $propertyGroupRecord
         */
        $propertyGroupRecord = PropertyGroupRecord::findOne($propertyGroup->id);

        if (!$propertyGroupRecord) {
            throw new HttpException(404, basename(__FILE__, '.php') . __LINE__);
        }

        $propertyRecords = $propertyGroupRecord->getProperties()->all();
        $result = [];

        foreach ((array)$propertyRecords as $propertyRecord) {
            $result[] = Property::populate($propertyRecord);
        }

        return $result;
    }


}