<?php

namespace commonprj\components\core\models;

use Yii;

/**
 * This is the model class for table "property_value_list_item".
 *
 * @property int $id
 * @property int $property_id
 * @property int $value
 * @property int $element_id
 *
 * @property PropertyRecord $property
 * @property PropertyListItemRecord $value0
 */
class PropertyValueListItemRecord extends AbstractPropertyValueRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'property_value_list_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['property_id', 'value', 'element_id'], 'required'],
            [['property_id', 'value', 'element_id'], 'default', 'value' => null],
            [['property_id', 'value', 'element_id'], 'integer'],
            [['property_id'], 'exist', 'skipOnError' => true, 'targetClass' => PropertyRecord::className(), 'targetAttribute' => ['property_id' => 'id']],
            [['value'], 'exist', 'skipOnError' => true, 'targetClass' => PropertyListItemRecord::className(), 'targetAttribute' => ['value' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'property_id' => 'Property ID',
            'value'       => 'Value',
            'element_id'  => 'Element ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty()
    {
        return $this->hasOne(PropertyRecord::className(), ['id' => 'property_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getValue0()
    {
        return $this->hasOne(PropertyListItemRecord::className(), ['id' => 'value']);
    }

}
