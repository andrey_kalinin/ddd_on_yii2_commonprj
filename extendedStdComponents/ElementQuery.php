<?php

namespace commonprj\extendedStdComponents;

use commonprj\components\core\entities\element\Element;
use commonprj\components\core\helpers\ClassAndContextHelper;
use commonprj\services\LocalizationService;
use commonprj\services\search\SearchService;
use yii\db\ActiveQueryInterface;
use Yii;

class ElementQuery extends EntityQuery implements ActiveQueryInterface
{
    public $classId;

    public $withProperties = true;

    public $propertyToTableMap = [];


    public function __construct($modelClass, array $config = [])
    {
        parent::__construct($modelClass, $config);
    }

    /**
     * Initializes the object.
     * This method is called at the end of the constructor. The default implementation will trigger
     * an [[EVENT_INIT]] event. If you override this method, make sure you call the parent implementation at the end
     * to ensure triggering of the event.
     */
    public function init()
    {
        $this->classId = $this->getModelClassId();
        parent::init();
    }

    /**
     * @param array $filters
     * @param array $entityIds
     * @return array
     */
    public function getFilters(array $filters, array $entityIds)
    {
        /** @var SearchService $finder */
        $finder = Yii::$app->searchService;

        return $finder->getFilterData($filters, $this->classId, $entityIds);
    }

    /**
     * @return bool
     */
    public function isElementQuery()
    {
        return true;
    }

    /**
     * @return int
     */
    protected function getModelClassId()
    {
        return ClassAndContextHelper::getClassId($this->modelClass);
    }

    public function withProperties(bool $value)
    {
        $this->withProperties = $value;
        return $this;
    }

    protected function extendPopulatedModel($model, $modelClass)
    {
        if ($this->withProperties) {
            $modelClass::populateElementProperties($model);
        }
    }

    protected function isTransliterateFilter($condition)
    {
        $sysname = $condition[1] ?? null;

        if (!$sysname) {
            return false;
        }

        /** @var LocalizationService $localizationService */
        $localizationService = Yii::$app->localization;;
        return is_string($sysname) && $localizationService->isTranslatableProperty($sysname);
    }

    protected function tryFindTranslation($condition)
    {
        /** @var LocalizationService $localizationService */
        $localizationService = Yii::$app->localization;

        $result = $localizationService->findEntities($condition, 'element', $condition[1]);

        //Добавляем id найденных записей propertyValue
        if ($result !== null) {
            $condition[] = $result;
        }

        return $condition;

    }

}