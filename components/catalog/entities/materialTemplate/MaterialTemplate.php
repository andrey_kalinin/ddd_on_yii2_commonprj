<?php
/**
 * @package         FurniPrice
 * @subpackage      Catalog
 * @category        Entity
 * @created         2018.01.22
 * @author          Vasiliy Konakov
 * @updated         2018.01.23
 * @updatedBy       Vasiliy Konakov
 */

namespace commonprj\components\catalog\entities\materialTemplate;

use commonprj\components\core\entities\element\Element;
use Yii;

/**
 * Class MaterialTemplate
 * @property MaterialTemplateServiceRepository $repository
 * @package commonprj\components\catalog\entities\materialTemplate
 */
class MaterialTemplate extends Element
{
    /**
     * @var array
     */
    public $relatedProperties = [];

    /**
     * Присвоение свойству доменного слоя $repository - соответствующего компонента
     * @inheritdoc
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->repository = Yii::$app->materialTemplateRepository;
    }

    /**
     * @return array
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\HttpException
     */
    public function getMaterials(): array
    {
        return $this->repository->getMaterials($this);
    }

}
