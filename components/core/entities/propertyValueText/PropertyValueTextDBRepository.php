<?php
/**
 * Created by daSilva.Rodrigues
 * Date: 22.08.2016
 */

namespace commonprj\components\core\entities\propertyValueText;

use commonprj\components\core\entities\abstractTranslateValue\AbstractTranslateValueRepository;
use commonprj\extendedStdComponents\IBaseCrudModel;
use commonprj\components\core\entities\abstractPropertyValue\AbstractPropertyValue;
use commonprj\components\core\models\PropertyValueTextRecord;
use yii\db\Query;

/**
 * Class TextPropertyDBRepository
 * @package commonprj\components\core\entities\textProperty
 */
class PropertyValueTextDBRepository extends AbstractTranslateValueRepository
{
    public $activeRecordClass = PropertyValueTextRecord::class;

    /**
     * @param AbstractPropertyValue $propertyValue
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function save(IBaseCrudModel $propertyValue, $runValidation = true, $attributes = null): bool
    {
        return parent::save($propertyValue, $runValidation, $attributes);
    }

    /**
     * Возвращает строку для UNION запроса для получения свойств со значениями элемента
     * @param int $elementId
     * @return Query
     */
    public function getValueQueryBuilder(int $elementId)
    {
        $queryBuilder = parent::getValueQueryBuilder($elementId);
        $queryBuilder->addSelect(['searchdb_property_value_id' => 'searchdb_property_value_id']);

        return $queryBuilder;
    }

}