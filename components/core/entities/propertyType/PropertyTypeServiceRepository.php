<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 14.02.2018
 * Time: 12:51
 */

namespace commonprj\components\core\entities\propertyType;

use commonprj\extendedStdComponents\BaseServiceRepository;
use http\Exception\BadMethodCallException;
use Yii;
use yii\httpclient\Client;
use yii\web\NotFoundHttpException;

/**
 * Class PropertyServiceRepository
 * @package commonprj\components\core\entities\propertyType
 */
class PropertyTypeServiceRepository extends BaseServiceRepository implements IPropertyTypeRepository
{
    /**
     * @var Client
     */
    protected $restServer;

    /**
     * @var string
     */
    protected $baseUri = 'core/property-type/';

    /**
     * @var array
     */
    protected $forbiddenMethods = ['save', 'update', 'delete'];

    /**
     * PropertyServiceRepository constructor.
     */
    public function __construct()
    {
        $this->restServer = Yii::$app->catalogManager;
    }

    /**
     * @param PropertyType $propertyType
     * @return array
     * @throws \yii\web\HttpException
     */
    public function getProperties(PropertyType $propertyType): array
    {
        $this->requestUri = $this->getBaseUri($propertyType->id . '/properties');
        return $this->getApiData();
    }

}