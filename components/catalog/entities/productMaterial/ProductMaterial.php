<?php

namespace commonprj\components\catalog\entities\productMaterial;

use commonprj\components\catalog\entities\material\Material;
use commonprj\components\catalog\entities\materialCollection\MaterialCollection;
use commonprj\components\catalog\entities\materialGroup\MaterialGroup;
use commonprj\components\catalog\entities\priceCategory\PriceCategory;
use commonprj\components\core\entities\abstractPropertyValue\AbstractPropertyValue;
use commonprj\components\core\entities\element\Element;
use commonprj\components\crm\entities\manufacturer\Manufacturer;
use commonprj\extendedStdComponents\BaseCrudModel;
use Yii;

/**
 * Class ProductMaterial
 * @property IProductMaterialRepository $repository
 * @package commonprj\components\catalog\entities\productMaterial
 */
class ProductMaterial extends Element
{
    private const PROPERTY_IMAGE_SYSNAME = 'image';

    /**
     * @var int
     */
    public $priceCategoryId;

    /**
     * @var int
     */
    public $materialCollectionId;

    /**
     * @var int
     */
    public $materialId;

    /**
     * @var int
     */
    public $materialGroupId;

    /**
     * @var string
     */
    public $imageId;

    /**
     * @var array
     */
    protected $propertyMultiplicitySysname = [
        'colors'                     => AbstractPropertyValue::MULTIPLICITY_ARRAY,
        'colorResistance'            => AbstractPropertyValue::MULTIPLICITY_ARRAY,
        'wearResistance'             => AbstractPropertyValue::MULTIPLICITY_ARRAY,
        /* 'fireResistance' => AbstractPropertyValue::MULTIPLICITY_ARRAY, */
        /* 'fabricDensity'              => AbstractPropertyValue::MULTIPLICITY_ARRAY, */
        'fabricStructure'            => AbstractPropertyValue::MULTIPLICITY_ARRAY,
        'pvcFilmFinishing'           => AbstractPropertyValue::MULTIPLICITY_ARRAY,
        'pvcFilmType'                => AbstractPropertyValue::MULTIPLICITY_ARRAY,
        /* 'pvcFilmThickness' => AbstractPropertyValue::MULTIPLICITY_ARRAY, */
        'glassBrand'                 => AbstractPropertyValue::MULTIPLICITY_ARRAY,
        'glassProperty'              => AbstractPropertyValue::MULTIPLICITY_ARRAY,
        'glassDecoration'            => AbstractPropertyValue::MULTIPLICITY_ARRAY,
        'glassKind'                  => AbstractPropertyValue::MULTIPLICITY_ARRAY,
        'facadeDesign'               => AbstractPropertyValue::MULTIPLICITY_ARRAY,
        'additionalFinishingOptions' => AbstractPropertyValue::MULTIPLICITY_ARRAY,
        'hidingPower'                => AbstractPropertyValue::MULTIPLICITY_ARRAY,
        'glossDegree'                => AbstractPropertyValue::MULTIPLICITY_ARRAY,
    ];

    /**
     * Присвоение свойству доменного слоя $repository - соответствующего компонента
     * @inheritdoc
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->repository = Yii::$app->productMaterialRepository;
    }

    /**
     * @param int $manufacturerId
     * @return bool
     */
    public function bindManufacturer(int $manufacturerId): bool
    {
        return $this->repository->bindManufacturer($this, $manufacturerId);
    }

    /**
     * @param int $manufacturerId
     * @return bool
     */
    public function unbindManufacturer(int $manufacturerId): bool
    {
        return $this->repository->unbindManufacturer($this, $manufacturerId);
    }

    /**
     * @return Manufacturer
     */
    public function getManufacturer(): Manufacturer
    {
        return $this->repository->getManufacturer($this);
    }

    /**
     * @param int $materialId
     * @param array $materialGroupIds
     * @return bool
     */
    public function bindMaterial(int $materialId, array $materialGroupIds = []): bool
    {
        return $this->repository->bindMaterial($this, $materialId);
    }

    /**
     * @param int $materialId
     * @param array $materialGroupIds
     * @return bool
     */
    public function unbindMaterial(int $materialId, array $materialGroupIds = []): bool
    {
        return $this->repository->unbindMaterial($this, $materialId);
    }

    /**
     * @return Material
     */
    public function getMaterial(): Material
    {
        return $this->repository->getMaterial($this);
    }

    /**
     * @return boolean
     */
    public function bindMaterialCollection(int $materialCollectionId): bool
    {
        return $this->repository->bindMaterialCollection($this, $materialCollectionId);
    }

    /**
     * @param int $materialCollectionId
     * @return bool
     */
    public function unbindMaterialCollection(int $materialCollectionId): bool
    {
        return $this->repository->unbindMaterialCollection($this, $materialCollectionId);
    }

    /**
     * @return MaterialCollection
     */
    public function getMaterialCollection(): MaterialCollection
    {
        return $this->repository->getMaterialCollection($this);
    }

    /**
     * @return boolean
     */
    public function bindPriceCategory(int $priceCategoryId): bool
    {
        return $this->repository->bindPriceCategory($this, $priceCategoryId);
    }

    /**
     * @return boolean
     */
    public function unbindPriceCategory(int $priceCategoryId): bool
    {
        return $this->repository->unbindPriceCategory($this, $priceCategoryId);
    }

    /**
     *
     * @return PriceCategory
     */
    public function getPriceCategory(): PriceCategory
    {
        return $this->repository->getPriceCategory($this);
    }

    /**
     * @return MaterialGroup
     */
    public function getMaterialGroup(): MaterialGroup
    {
        return $this->repository->getMaterialGroup($this);
    }

    /**
     * @param int $materialGroupId
     * @return bool
     */
    public function bindMaterialGroup(int $materialGroupId)
    {
        return $this->repository->bindMaterialGroup($this, $materialGroupId);
    }

    /**
     * @param int $materialGroupId
     * @return bool
     */
    public function unbindMaterialGroup(int $materialGroupId)
    {
        return $this->repository->unbindMaterialGroup($this, $materialGroupId);
    }

    /**
     * Меняет property image value на реальные url
     * @param BaseCrudModel $entity
     * @param array|int $entitiesId
     */
    public static function populateRecord(BaseCrudModel $entity, $entitiesId)
    {
        parent::populateRecord($entity, $entitiesId);

        if (function_exists('cloudinary_url')) {
            foreach ($entity->properties as $property)
            {
                if ($property['sysname'] == self::PROPERTY_IMAGE_SYSNAME) {
                    $entity->imageId = cloudinary_url($property['value']);
                }
            }
        }
    }

    public function attributes()
    {
        return array_merge(parent::attributes(), ['imageId']);
    }


}
