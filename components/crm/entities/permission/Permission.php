<?php

namespace commonprj\components\crm\entities\permission;

use commonprj\components\crm\entities\permissionGroup\PermissionGroup;
use Yii;
use commonprj\components\core\entities\element\Element;

/**
 * Class Permission
 * @property IPermissionRepository $repository
 * @package commonprj\components\crm\entities\permission
 */
class Permission extends Element
{
    /**
     * Присвоение свойству доменного слоя $repository - соответствующего компонента
     * @inheritdoc
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->repository = Yii::$app->permissionRepository;
    }

    /**
     * @param int $permissionGroupId
     * @return bool
     */
    public function bindPermissionGroup($permissionGroupId): bool
    {
        return $this->repository->bindPermissionGroup($this, $permissionGroupId);
    }

    /**
     * @param int $permissionGroupId
     * @return bool
     */
    public function unbindPermissionGroup($permissionGroupId): bool
    {
        return $this->repository->unbindPermissionGroup($this, $permissionGroupId);
    }

    /**
     * @return PermissionGroup
     */
    public function getPermissionGroup(): PermissionGroup
    {
        return $this->repository->getPermissionGroup($this);
    }
}
