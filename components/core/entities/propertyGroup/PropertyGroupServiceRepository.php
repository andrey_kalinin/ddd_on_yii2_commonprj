<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 13.02.2018
 * Time: 11:54
 */

namespace commonprj\components\core\entities\propertyGroup;

use commonprj\extendedStdComponents\BaseServiceRepository;
use Yii;
use yii\httpclient\Client;

/**
 * Class PropertyGroupServiceRepository
 * @package commonprj\components\core\entities\propertyGroup
 */
class PropertyGroupServiceRepository extends BaseServiceRepository
{
    /**
     * @var Client
     */
    protected $restServer;

    /**
     * @var string
     */
    protected $baseUri = 'core/property-group/';

    /**
     * PropertyServiceRepository constructor.
     */
    public function __construct()
    {
        $this->restServer = Yii::$app->catalogManager;
    }

    /**
     * @param PropertyGroup $propertyGroup
     * @return array
     * @throws \yii\web\HttpException
     */
    public function getProperties(PropertyGroup $propertyGroup): array
    {
        $this->requestUri = $this->getBaseUri($propertyGroup->id . '/properties');
        return $this->getApiData();
    }
}